# vim: set fileencoding=utf-8 :
from django.contrib import admin

from . import models


class AcercaDeAdmin(admin.ModelAdmin):

    list_display = (
        'id_acerca',
        'id_gimnasio',
        'titulo_acerca',
        'desc_acerca',
        'sidea_acerca',
        'sidea1_acerca',
        'sideb_acerca',
        'sideb1_acerca',
        'img_uno',
        'img_dos',
    )
    list_filter = ('id_gimnasio',)


class AdministradorAdmin(admin.ModelAdmin):

    list_display = ('id_administrador', 'id')


class AuthGroupAdmin(admin.ModelAdmin):

    list_display = ('id', 'name')
    search_fields = ('name',)


class AuthGroupPermissionsAdmin(admin.ModelAdmin):

    list_display = ('id', 'group', 'permission')
    list_filter = ('group',)
    raw_id_fields = ('permission',)


class AuthPermissionAdmin(admin.ModelAdmin):

    list_display = ('id', 'name', 'content_type', 'codename')
    search_fields = ('name',)


class AuthUserAdmin(admin.ModelAdmin):

    list_display = (
        'id',
        'dni',
        'first_name',
        'last_name',
        'email',
        'genero',
        'edad',
        'direccion',
        'telefono',
        'celular',
        'img_perfil',
        'username',
        'password',
        'last_login',
        'is_superuser',
        'is_staff',
        'is_active',
        'date_joined',
        'es_administrador',
        'es_deportista',
        'es_instructor',
        'es_secretaria',
    )
    list_filter = (
        'last_login',
        'is_superuser',
        'is_staff',
        'is_active',
        'date_joined',
        'es_administrador',
        'es_deportista',
        'es_instructor',
        'es_secretaria',
    )


class AuthUserGroupsAdmin(admin.ModelAdmin):

    list_display = ('id', 'user', 'group')
    list_filter = ('group',)


class AuthUserUserPermissionsAdmin(admin.ModelAdmin):

    list_display = ('id', 'user', 'permission')
    raw_id_fields = ('permission',)


class ContactoAdmin(admin.ModelAdmin):

    list_display = (
        'id_contacto',
        'id_gimnasio',
        'titulo_contac',
        'desc_contact',
        'direccion_contact',
        'phone_uno',
        'phone_dos',
        'phone_tres',
    )
    list_filter = ('id_gimnasio',)


class DeportistaAdmin(admin.ModelAdmin):

    list_display = ('id_deportista', 'id')


class DisciplinaAdmin(admin.ModelAdmin):

    list_display = ('id_disciplina', 'nombre', 'img_disciplina')


class DjangoAdminLogAdmin(admin.ModelAdmin):

    list_display = (
        'id',
        'action_time',
        'object_id',
        'object_repr',
        'action_flag',
        'change_message',
        'content_type',
        'user',
    )
    list_filter = ('action_time',)


class DjangoContentTypeAdmin(admin.ModelAdmin):

    list_display = ('id', 'app_label', 'model')


class DjangoMigrationsAdmin(admin.ModelAdmin):

    list_display = ('id', 'app', 'name', 'applied')
    list_filter = ('applied',)
    search_fields = ('name',)


class DjangoSessionAdmin(admin.ModelAdmin):

    list_display = ('session_key', 'session_data', 'expire_date')
    list_filter = ('expire_date',)


class EmailAdmin(admin.ModelAdmin):

    list_display = (
        'id_email',
        'nombre',
        'direccion',
        'email',
        'telefono',
        'mensaje',
    )


class GaleriaAdmin(admin.ModelAdmin):

    list_display = (
        'id_galeria',
        'id_gimnasio',
        'titulo_galeria',
        'img_galeria',
        'desc_galeria',
    )
    list_filter = ('id_gimnasio',)


class GimnasioAdmin(admin.ModelAdmin):

    list_display = ('id_gimnasio', 'nombre_gym')


class HorarioAdmin(admin.ModelAdmin):

    list_display = (
        'id_horario',
        'hora_inicio',
        'hora_fin',
        'ubicacion',
        'sesion',
        'categoria',
        'cupos',
        'desc_horario',
    )


class InicioSlideAdmin(admin.ModelAdmin):

    list_display = (
        'id_inicioslide',
        'id_gimnasio',
        'img_slide',
        'imgbtn_slide',
        'titulo_slide',
        'subt_slide',
        'nombre_enlace',
        'direccion_enlace',
    )
    list_filter = ('id_gimnasio',)


class InstructorAdmin(admin.ModelAdmin):

    list_display = (
        'id_instructor',
        'id',
        'inst_profesion',
        'inst_estado',
        'inst_descripcion',
        'inst_imagen',
    )
    list_filter = ('inst_estado',)


class MatriculaAdmin(admin.ModelAdmin):

    list_display = (
        'id_matricula',
        'id_deportista',
        'id_secretaria',
        'estado',
        'fecha_matricula',
        'costo',
    )
    list_filter = (
        'id_deportista',
        'id_secretaria',
        'estado',
        'fecha_matricula',
    )


class DisciplinaHorarioAdmin(admin.ModelAdmin):

    list_display = (
        'id_disciplina_horario',
        'id_disciplina',
        'id_horario',
        'id_instructor',
        
    )
    list_filter = (
        'id_disciplina',
        'id_horario',
        'id_instructor',
        
    )


class MatriculaDisciplinaAdmin(admin.ModelAdmin):

    list_display = (
        'id_matriculadisciplina',
        'id_matricula',
        'id_disciplina_horario',
    )
    list_filter = ('id_matricula', 'id_disciplina_horario')


class MensualidadAdmin(admin.ModelAdmin):

    list_display = (
        'id_mensualidad',
        'id_matriculadisciplina',
        'concepto',
        'total',
        'pagado',
        'fecha_mensualidad',
        'fecha_pago',
    )
    list_filter = (
        'id_matriculadisciplina',
        'pagado',
        'fecha_mensualidad',
        'fecha_pago',
    )


class ParallaxAdmin(admin.ModelAdmin):

    list_display = (
        'id_parallax',
        'id_gimnasio',
        'img_parallax',
        'title_parallax',
        'desc_parallax',
        'imginfe_parallax',
        'enlace_video',
    )
    list_filter = ('id_gimnasio',)


class ParallaxDosAdmin(admin.ModelAdmin):

    list_display = (
        'id_parallaxdos',
        'id_gimnasio',
        'img_parallaxdos',
        'costo_parallaxdos',
        'dias_parallaxtres',
        'desc_parallaxdos',
    )
    list_filter = ('id_gimnasio',)


class ParallaxUnoAdmin(admin.ModelAdmin):

    list_display = (
        'id_parallaxuno',
        'id_gimnasio',
        'back_parallaxuno',
        'img_parallaxuno',
        'title_parallaxuno',
        'subt_parallaxuno',
    )
    list_filter = ('id_gimnasio',)


class ProductoAdmin(admin.ModelAdmin):

    list_display = (
        'id_producto',
        'id_gimnasio',
        'nombre_prod',
        'desc_prod',
        'img_prod',
        'precio_prod',
    )
    list_filter = ('id_gimnasio',)


class SecretariaAdmin(admin.ModelAdmin):

    list_display = ('id_secretaria', 'id', 'id_administrador')
    list_filter = ('id_administrador',)


class SeguimientoAdmin(admin.ModelAdmin):

    list_display = (
        'id_seguimiento',
        'diametro_brazo',
        'fecha',
        'id_matricula',
    )
    list_filter = ('fecha', 'id_matricula')
class DeportistaDestacadoAdmin(admin.ModelAdmin):

    list_display = (
        'id_deportista_des',
        'nombre',
        'apellidos',
        'disciplina',
    )
    list_filter = ('nombre', 'disciplina')


def _register(model, admin_class):
    admin.site.register(model, admin_class)


_register(models.AcercaDe, AcercaDeAdmin)
_register(models.Administrador, AdministradorAdmin)
_register(models.AuthGroup, AuthGroupAdmin)
_register(models.AuthGroupPermissions, AuthGroupPermissionsAdmin)
_register(models.AuthPermission, AuthPermissionAdmin)
_register(models.AuthUser, AuthUserAdmin)
_register(models.AuthUserGroups, AuthUserGroupsAdmin)
_register(models.AuthUserUserPermissions, AuthUserUserPermissionsAdmin)
_register(models.Contacto, ContactoAdmin)
_register(models.Deportista, DeportistaAdmin)
_register(models.Disciplina, DisciplinaAdmin)
_register(models.DjangoAdminLog, DjangoAdminLogAdmin)
_register(models.DjangoContentType, DjangoContentTypeAdmin)
_register(models.DjangoMigrations, DjangoMigrationsAdmin)
_register(models.DjangoSession, DjangoSessionAdmin)
_register(models.Email, EmailAdmin)
_register(models.Galeria, GaleriaAdmin)
_register(models.Gimnasio, GimnasioAdmin)
_register(models.Horario, HorarioAdmin)
_register(models.InicioSlide, InicioSlideAdmin)
_register(models.Instructor, InstructorAdmin)
_register(models.Matricula, MatriculaAdmin)
_register(models.DisciplinaHorario, DisciplinaHorarioAdmin)
_register(models.MatriculaDisciplina, MatriculaDisciplinaAdmin)
_register(models.Mensualidad, MensualidadAdmin)
_register(models.Parallax, ParallaxAdmin)
_register(models.ParallaxDos, ParallaxDosAdmin)
_register(models.ParallaxUno, ParallaxUnoAdmin)
_register(models.Producto, ProductoAdmin)
_register(models.Secretaria, SecretariaAdmin)
_register(models.Seguimiento, SeguimientoAdmin)
