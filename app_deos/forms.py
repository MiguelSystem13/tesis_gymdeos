# -- coding: utf-8
from django import forms
from django.forms import ModelForm
from ckeditor_uploader.widgets import CKEditorUploadingWidget
from ckeditor.widgets import CKEditorWidget
from .models import *
# Registration
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
import sys

class AuthUserGroupssForm(forms.ModelForm):
	class Meta:
		model = AuthUserGroups
		fields = '__all__'
		labels = {
			'user':'Usuario',
			'group':'Grupos',
		}
		widgets = {
			'user': forms.Select(attrs={'class':'form-control'}),
			'group': forms.Select(attrs={'class':'form-control'}),

		}
class AuthUserGroupssEditForm(forms.ModelForm):
	class Meta:
		model = AuthUserGroups
		fields = '__all__'
		labels = {
			'user':'Usuario',
			'group':'Grupos',
		}
		widgets = {
			'user': forms.Select(attrs={'class':'form-control'}),
			'group': forms.Select(attrs={'class':'form-control'}),
		}
class AuthUserGroupsNewForm(forms.ModelForm):
	class Meta:
		model = AuthUserGroups
		fields = '__all__'
class RegistrationForm(UserCreationForm):
	# email = forms.EmailField(required=True)
	class Meta:
		model = User
		fields = {
			'first_name',
			'last_name',
			'email',
			'username',
			'password1',
			'password2'
		}
	def save(self, commit=True):
		user = super(RegistrationForm, self).save(commit=False)
		user.first_name = self.cleaned_data['first_name']
		user.last_name = self.cleaned_data['last_name']
		user.email_name = self.cleaned_data['email']

		if commit:
			user.save()
		return user
class DeportistaEditForm(ModelForm):
	class Meta:
		model = AuthUser
		fields = '__all__'
		exclude = ['is_superuser', 'password','is_staff','es_secretaria','es_deportista','es_administrador','es_instructor']
		labels = {
			# 'buscar':'Buscar',
			'dni':'Cédula',
			'first_name':'Nombres',
			'last_name':'Apellidos',
			'email':'Email',
			'genero':'Género',
			'edad':'Edad',
			'direccion':'Dirección',
			'telefono':'Telefono',
			'celular':'Celular',
			'img_perfil':'Imágen',
			'username':'Usuario',
			# 'password':'Password',
			# 'is_superuser':'Administrador',
			'is_staff':'Ingresar a la aplicacion',
			'is_active':'Activo',
			'es_secretaria':'Es usuario',
			'es_deportista':'Es deportista',
		}
		widgets = {
			'dni': forms.TextInput(attrs={'class':'form-control','placeholder':'110****'}),
			'first_name': forms.TextInput(attrs={'class':'form-control','placeholder':'Jose Anibal'}),
			'last_name': forms.TextInput(attrs={'class':'form-control','placeholder':'Cueva Correa'}),
			'email': forms.EmailInput(attrs={'class':'form-control','placeholder':'example@gmail.com'}),
			'genero': forms.Select(attrs={'class':'form-control','placeholder':'MASCULINO'}),
			'edad': forms.NumberInput(attrs={'class':'form-control','placeholder':'25'}),
			'direccion': forms.TextInput(attrs={'class':'form-control','placeholder':'Loja'}),
			'telefono': forms.NumberInput(attrs={'class':'form-control','placeholder':'072***'}),
			'celular': forms.NumberInput(attrs={'class':'form-control','placeholder':'09****'}),
			'img_perfil': forms.FileInput(attrs={'class':'form-control'}),
			'username': forms.TextInput(attrs={'class':'form-control','placeholder':'Username'}),
			# 'password': forms.PasswordInput(attrs={'class':'form-control','placeholder':'********'}),
			# 'is_superuser': forms.CheckboxInput(attrs={'class':'radio'}),
			'is_staff': forms.CheckboxInput(attrs={'class':'radio'}),
			'is_active': forms.CheckboxInput(attrs={'class':'radio'}),
			'es_secretaria': forms.CheckboxInput(attrs={'class':'radio'}),
			'es_deportista': forms.CheckboxInput(attrs={'class':'radio'}),
		}
class DeportistaForm(ModelForm):
	class Meta:
		model = AuthUser
		fields = '__all__'
		exclude = ['is_superuser','es_instructor','es_deportista','is_staff','is_active','es_secretaria','es_administrador']
		labels = {
			# 'buscar':'Buscar',
			'dni':'Cédula',
			'first_name':'Nombres',
			'last_name':'Apellidos',
			'email':'Email',
			'genero':'Género',
			'edad':'Edad',
			'direccion':'Dirección',
			'telefono':'Telefono',
			'celular':'Celular',
			'img_perfil':'Imágen',
			'username':'Usuario',
			'password':'Password',
			# 'is_superuser':'Administrador',
			'is_staff':'Ingresar a la aplicacion',
			'is_active':'Activo',
			'es_secretaria':'Es usuario',
			'es_deportista':'Es deportista',
		}
		widgets = {
			'dni': forms.TextInput(attrs={'class':'form-control','placeholder':'110****'}),
			'first_name': forms.TextInput(attrs={'class':'form-control','placeholder':'Jose Anibal'}),
			'last_name': forms.TextInput(attrs={'class':'form-control','placeholder':'Cueva Correa'}),
			'email': forms.EmailInput(attrs={'class':'form-control','placeholder':'example@gmail.com'}),
			'genero': forms.Select(attrs={'class':'form-control','placeholder':'MASCULINO'}),
			'edad': forms.NumberInput(attrs={'class':'form-control','placeholder':'25'}),
			'direccion': forms.TextInput(attrs={'class':'form-control','placeholder':'Loja'}),
			'telefono': forms.NumberInput(attrs={'class':'form-control','placeholder':'072***'}),
			'celular': forms.NumberInput(attrs={'class':'form-control','placeholder':'09****'}),
			'img_perfil': forms.FileInput(attrs={'class':'form-control'}),
			'username': forms.TextInput(attrs={'class':'form-control','placeholder':'Username'}),
			'password': forms.PasswordInput(attrs={'class':'form-control','placeholder':'********'}),
			# 'is_superuser': forms.CheckboxInput(attrs={'class':'radio'}),
			'is_staff': forms.CheckboxInput(attrs={'class':'radio'}),
			'is_active': forms.CheckboxInput(attrs={'class':'radio'}),
			'es_secretaria': forms.CheckboxInput(attrs={'class':'radio'}),
			'es_deportista': forms.CheckboxInput(attrs={'class':'radio'}),
		}
	# desactivar is_active
	# def __init__(self, *args, **kwargs):
	# 		super(DeportistaForm, self).__init__(*args, **kwargs)
	# 		for field in iter(self.fields):
	# 			if field <> 'is_active':
	# 				self.fields[field].widget.attrs.update({'class':'form-control'})
class InstructorrForm(ModelForm):
	class Meta:
		model = AuthUser
		fields = '__all__'
		exclude = ['is_superuser','es_secretaria','es_deportista','es_instructor','es_administrador','is_active','is_staff']
		labels = {
			# 'buscar':'Buscar',
			'dni':'Cédula',
			'first_name':'Nombres',
			'last_name':'Apellidos',
			'email':'Email',
			'genero':'Género',
			'edad':'Edad',
			'direccion':'Dirección',
			'telefono':'Teléfono',
			'celular':'Celular',
			'img_perfil':'Imágen',
			'username':'Usuario',
			'password':'Password',
			# 'is_superuser':'Administrador',
			'is_staff':'Ingresar a la aplicación',
			'is_active':'Activo',
			'es_secretaria':'Es usuario',
			'es_deportista':'Es deportista',
		}
		widgets = {
			'dni': forms.TextInput(attrs={'class':'form-control','placeholder':'110****'}),
			'first_name': forms.TextInput(attrs={'class':'form-control','placeholder':'Jose Anibal'}),
			'last_name': forms.TextInput(attrs={'class':'form-control','placeholder':'Cueva Correa'}),
			'email': forms.EmailInput(attrs={'class':'form-control','placeholder':'example@gmail.com'}),
			'genero': forms.Select(attrs={'class':'form-control','placeholder':'MASCULINO'}),
			'edad': forms.NumberInput(attrs={'class':'form-control','placeholder':'25'}),
			'direccion': forms.TextInput(attrs={'class':'form-control','placeholder':'Loja'}),
			'telefono': forms.NumberInput(attrs={'class':'form-control','placeholder':'072***'}),
			'celular': forms.NumberInput(attrs={'class':'form-control','placeholder':'09****'}),
			'img_perfil': forms.FileInput(attrs={'class':'form-control'}),
			'username': forms.TextInput(attrs={'class':'form-control','placeholder':'Username'}),
			'password': forms.PasswordInput(attrs={'class':'form-control','placeholder':'********'}),
			# 'is_superuser': forms.CheckboxInput(attrs={'class':'radio'}),
			'is_staff': forms.CheckboxInput(attrs={'class':'radio'}),
			'is_active': forms.CheckboxInput(attrs={'class':'radio'}),
			'es_secretaria': forms.CheckboxInput(attrs={'class':'radio'}),
			'es_deportista': forms.CheckboxInput(attrs={'class':'radio'}),
		}
	# desactivar is_active
	# def __init__(self, *args, **kwargs):
	# 		super(DeportistaForm, self).__init__(*args, **kwargs)
	# 		for field in iter(self.fields):
	# 			if field <> 'is_active':
	# 				self.fields[field].widget.attrs.update({'class':'form-control'})
class InstructorEditForm(ModelForm):
	class Meta:
		model = AuthUser
		fields = '__all__'
		exclude = ['is_superuser', 'password','es_instructor','es_secretaria','es_administrador','es_deportista','is_staff']
		labels = {
			# 'buscar':'Buscar',
			'dni':'Cédula',
			'first_name':'Nombres',
			'last_name':'Apellidos',
			'email':'Email',
			'genero':'Género',
			'edad':'Edad',
			'direccion':'Direccion',
			'telefono':'Teléfono',
			'celular':'Celular',
			'img_perfil':'Imágen',
			'username':'Usuario',
			# 'password':'Password',
			# 'is_superuser':'Administrador',
			'is_staff':'Ingresar a la aplicación',
			'is_active':'Activo',
			'es_secretaria':'Es usuario',
			'es_deportista':'Es deportista',
		}
		widgets = {
			'dni': forms.TextInput(attrs={'class':'form-control','placeholder':'110****'}),
			'first_name': forms.TextInput(attrs={'class':'form-control','placeholder':'Jose Anibal'}),
			'last_name': forms.TextInput(attrs={'class':'form-control','placeholder':'Cueva Correa'}),
			'email': forms.EmailInput(attrs={'class':'form-control','placeholder':'example@gmail.com'}),
			'genero': forms.Select(attrs={'class':'form-control','placeholder':'MASCULINO'}),
			'edad': forms.NumberInput(attrs={'class':'form-control','placeholder':'25'}),
			'direccion': forms.TextInput(attrs={'class':'form-control','placeholder':'Loja'}),
			'telefono': forms.NumberInput(attrs={'class':'form-control','placeholder':'072***'}),
			'celular': forms.NumberInput(attrs={'class':'form-control','placeholder':'09****'}),
			'img_perfil': forms.FileInput(attrs={'class':'form-control'}),
			'username': forms.TextInput(attrs={'class':'form-control','placeholder':'Username'}),
			# 'password': forms.PasswordInput(attrs={'class':'form-control','placeholder':'********'}),
			# 'is_superuser': forms.CheckboxInput(attrs={'class':'radio'}),
			'is_staff': forms.CheckboxInput(attrs={'class':'radio'}),
			'is_active': forms.CheckboxInput(attrs={'class':'radio'}),
			'es_secretaria': forms.CheckboxInput(attrs={'class':'radio'}),
			'es_deportista': forms.CheckboxInput(attrs={'class':'radio'}),
		}
class UsuarioForm(forms.ModelForm):
	# def __init__(self, *args, **kwargs):
	#     super(UsuarioForm, self).__init__(*args, **kwargs)

	#     for key in self.fields:
	#         self.fields[key].required = False  
	class Meta:
		model = AuthUser
		fields = '__all__' 
		exclude = ['es_secretaria','is_superuser','is_staff','is_active','es_administrador','es_instructor','es_deportista']
		labels = {
			'dni':'Cédula',
			'first_name':'Nombres',
			'last_name':'Apellidos',
			'email':'Email',
			'genero':'Género',
			'edad':'Edad',
			'direccion':'Dirección',
			'telefono':'Teléfono',
			'celular':'Celular',
			'img_perfil':'Imágen',
			'username':'Usuario',
			'password':'Password',
			'es_secretaria':'Es usuario',
		}
		widgets = {
			'dni': forms.TextInput(attrs={'class':'form-control','placeholder':'110****'}),
			'first_name': forms.TextInput(attrs={'class':'form-control','placeholder':'Jose Anibal'}),
			'last_name': forms.TextInput(attrs={'class':'form-control','placeholder':'Cueva Correa'}),
			'email': forms.EmailInput(attrs={'class':'form-control','placeholder':'example@gmail.com'}),
			'genero': forms.Select(attrs={'class':'form-control','placeholder':'MASCULINO'}),
			'edad': forms.NumberInput(attrs={'class':'form-control','placeholder':'25'}),
			'direccion': forms.TextInput(attrs={'class':'form-control','placeholder':'Loja'}),
			'telefono': forms.NumberInput(attrs={'class':'form-control','placeholder':'072***'}),
			'celular': forms.NumberInput(attrs={'class':'form-control','placeholder':'09****'}),
			'img_perfil': forms.FileInput(attrs={'class':'form-control'}),
			'username': forms.TextInput(attrs={'class':'form-control','placeholder':'Username'}),
			'password': forms.PasswordInput(attrs={'class':'form-control','placeholder':'********'}),
			'es_secretaria': forms.CheckboxInput(attrs={'class':'radio'}),
		}
class UsuarioEditForm(forms.ModelForm):
	# def __init__(self, *args, **kwargs):
	#     super(UsuarioForm, self).__init__(*args, **kwargs)

	#     for key in self.fields:
	#         self.fields[key].required = False  
	class Meta:
		model = AuthUser
		fields = '__all__' 
		exclude = ['es_secretaria','password','is_superuser','is_staff','es_administrador','es_instructor','es_deportista']
		labels = {
			'dni':'Cédula',
			'first_name':'Nombres',
			'last_name':'Apellidos',
			'email':'Email',
			'genero':'Género',
			'edad':'Edad',
			'direccion':'Dirección',
			'telefono':'Teléfono',
			'celular':'Celular',
			'img_perfil':'Imágen',
			'username':'Usuario',
			'password':'Password',
			'is_active':'Activo',
		}
		widgets = {
			'dni': forms.TextInput(attrs={'class':'form-control','placeholder':'110****'}),
			'first_name': forms.TextInput(attrs={'class':'form-control','placeholder':'Jose Anibal'}),
			'last_name': forms.TextInput(attrs={'class':'form-control','placeholder':'Cueva Correa'}),
			'email': forms.EmailInput(attrs={'class':'form-control','placeholder':'example@gmail.com'}),
			'genero': forms.Select(attrs={'class':'form-control','placeholder':'MASCULINO'}),
			'edad': forms.NumberInput(attrs={'class':'form-control','placeholder':'25'}),
			'direccion': forms.TextInput(attrs={'class':'form-control','placeholder':'Loja'}),
			'telefono': forms.NumberInput(attrs={'class':'form-control','placeholder':'072***'}),
			'celular': forms.NumberInput(attrs={'class':'form-control','placeholder':'09****'}),
			'img_perfil': forms.FileInput(attrs={'class':'form-control'}),
			'username': forms.TextInput(attrs={'class':'form-control','placeholder':'Username'}),
			'password': forms.PasswordInput(attrs={'class':'form-control','placeholder':'********'}),
			'is_active': forms.CheckboxInput(attrs={'class':'radio'}),
		}

class SecretariaForm(forms.ModelForm):
	class Meta:
		model = Secretaria
		fields = '__all__'
		exclude = ['id']
		labels = {
			'id_administrador':'Administrador',
		}
		widgets = {
			'id_administrador':forms.Select(attrs={'class':'form-control'})
		}
class MensualidadForm(forms.ModelForm):
	class Meta:
		model = Mensualidad
		fields = '__all__'
		exclude = ['fecha_pago']
		labels = {
			'id_matriculadisciplina':'Matrícula',
			'concepto':'Concepto de pago',
			'total':'Total de pago',
			'pagado':'Pagado',
			'fecha_mensualidad':'Fecha de Mensualidad (Cuando se cumple)',
			'fecha_pago':'Fecha que realiza el pago',
		}
		widgets = {
			'id_matriculadisciplina': forms.Select(attrs={'type':'select2','class':'form-control'}),
			'concepto': forms.TextInput(attrs={'class':'form-control'}),
			'total': forms.TextInput(attrs={'class':'form-control'}),
			'pagado': forms.CheckboxInput(attrs={'class':'radio'}),
			'fecha_mensualidad': forms.TextInput(attrs={'type':'date','class':'form-control'}),
			'fecha_pago': forms.TextInput(attrs={'type':'date','class':'form-control'}),
		}
	# def __init__(self, *args, **kwargs):
	#     super().__init__(*args, **kwargs)
	#     self.fields['total'].disabled = True
	def __init__(self, *args, **kwargs):
			super(MensualidadForm, self).__init__(*args, **kwargs)
			for field in iter(self.fields):
				if field is not 'pagado':
				# if field <> 'pagado':
					self.fields[field].widget.attrs.update({'class':'form-control'})
# class InstructorForm(forms.ModelForm):

# 	class Meta:
# 		model = Instructor
# 		fields = {
# 			'inst_profesion',
# 			'inst_descripcion',
# 			'inst_imágen',
# 		}
# 		labels = {
# 			'inst_profesion':'Profesion',
# 			'inst_descripcion':'Descripcion',
# 			'inst_imagen':'Imágen',
# 		}
# 		widgets = {
# 			'inst_profesion': forms.TextInput(attrs={'class':'form-control','placeholder':''}),
# 			'inst_descripcion': forms.TextInput(attrs={'class':'form-control'}),
# 			'inst_imagen': forms.FileInput(attrs={'class':'form-control'}),
# 		}
		
class DisciplinaForm(forms.ModelForm):

	class Meta:
		model = Disciplina
		fields = '__all__'
		labels = {
			# 'buscar':'Buscar',
			'nombre':'Nombre',
			# 'categoria':'Categoria',
			'img_disciplina':'Imágen',
		}
		widgets = {
			# 'buscar': forms.SearhInput(attrs={'class':'form-control','placeholder':'Buscar...'}),
			'nombre': forms.TextInput(attrs={'class':'form-control'}),
			# 'categoria': forms.Select(attrs={'class':'form-control'}),
			'img_disciplina': forms.FileInput(attrs={'class':'form-control'}),
		}
class HorarioForm(forms.ModelForm):

	class Meta:
		model = Horario
		fields = '__all__'
		exclude = ['desc_horario']
		labels = {
			'hora_inicio':'Hora inicio',
			'hora_fin':'Hora fin',
			'ubicacion':'Ubicación',
			'sesion':'Sesión',
			'categoria':'Categoría',
			'cupos':'Cupos ',
		}
		widgets = {
			'hora_inicio': forms.TimeInput(attrs={'type':'time','class':'form-control'}),
			'hora_fin': forms.TimeInput(attrs={'type':'time','class':'form-control'}),
			'ubicacion': forms.TextInput(attrs={'class':'form-control','placeholder':'Sala 1'}),
			'sesion': forms.Select(attrs={'class':'form-control'}),
			'categoria': forms.Select(attrs={'class':'form-control'}),
			'cupos': forms.NumberInput(attrs={'class':'form-control '}),
			# 'disciplina': forms.SelectMultiple(attrs={'class':'form-control'}),
		}
class DisciplinaHorarioForm(forms.ModelForm):	
	class Meta:
		model = DisciplinaHorario
		fields = '__all__'
		labels = {
			'id_disciplina':'Disciplina',
			'id_instructor':'Instructor',
			'id_horario':'Horario',
		}
		widgets = {
			'id_disciplina':forms.Select(attrs={'class':'form-control'}),
			'id_instructor':forms.Select(attrs={'class':'form-control'}),
			'id_horario':forms.Select(attrs={'class':'form-control'}),
		}

class DisciplinaHorarioForm(forms.ModelForm):	
	class Meta:
		model = DisciplinaHorario
		fields = '__all__'
		labels = {
			'id_disciplina':'Disciplina',
			'id_instructor':'Instructor',
			'id_horario':'Horario',
		}
		widgets = {
			'id_disciplina':forms.Select(attrs={'class':'form-control'}),
			'id_instructor':forms.Select(attrs={'class':'form-control'}),
			'id_horario':forms.Select(attrs={'class':'form-control'}),
		}

# Inicio Pre Mat
class PreinscripcionForm(forms.ModelForm):
	id_disciplina_horario = forms.ModelMultipleChoiceField(widget=forms.CheckboxSelectMultiple(), queryset=DisciplinaHorario.objects.filter(id_horario__cupos__gte = 1))
	class Meta:
		# model = Deportista
		# fields = '__all__'
		model = Matricula
		fields = {
			'id_matricula',		    
		    'id_disciplina_horario',		    
		}
		widgets = {		    
		    'id_disciplina_horario': forms.Select(attrs={'class':'form-control'}),		    
		}

class PreinscripcionAdminForm(forms.ModelForm):
	id_disciplina_horario = forms.ModelMultipleChoiceField(widget=forms.CheckboxSelectMultiple(), queryset=DisciplinaHorario.objects.filter(id_horario__cupos__gte = 1))
	class Meta:
		# model = Deportista
		# fields = '__all__'
		model = Matricula
		fields = {
			'id_deportista',
			'estado',		    
		    'id_disciplina_horario',		    
		}
		labels = {
			'id_deportista':'Buscar deportista ',
			'id_disciplina_horario':'Seleccionar disciplina ',
		}
		widgets = {		    
		    'id_deportista': forms.Select(attrs={'class':'form-control'}),		    
		    'id_disciplina_horario': forms.CheckboxSelectMultiple(attrs={'class':'form-control'}),		    
		}
class PreinscripcionAdminEditForm(forms.ModelForm):
	id_disciplina_horario = forms.ModelMultipleChoiceField(widget=forms.CheckboxSelectMultiple(), queryset=DisciplinaHorario.objects.filter(id_horario__cupos__gte = 1))
	class Meta:
		# model = Deportista
		# fields = '__all__'
		model = Matricula
		fields = {
			'id_deportista',
			'estado',		    
		    'id_disciplina_horario',		    
		}
		labels = {
			'id_deportista':'Buscar deportista  ',
			'estado':'Matricula finalizada  ',
			'id_disciplina_horario':'Seleccionar disciplina ',
		}
		widgets = {		    
		    'id_deportista': forms.Select(attrs={'class':'form-control'}),		    
		    'estado': forms.CheckboxInput(attrs={'class':'radio'}),		    
		    'id_disciplina_horario': forms.CheckboxSelectMultiple(attrs={'class':'form-control'}),		    
		}

# Fin Pre mat
class MatriculaForm(forms.ModelForm):
	class Meta:
		# model = Deportista
		# fields = '__all__'
		model = Matricula
		fields = {
			'id_deportista',
			# 'id_disciplina_horario',
			'estado',
			'costo',
		}
		labels = {
			'id_deportista':'Elegir Deportista',
			# 'id_disciplina_horario':'Escoger Disciplina-Horario',
			'estado':'Matricula finalizada',
			'costo':'Costo',
		}
		widgets = {
			'id_deportista': forms.Select(attrs={'class':'form-control select2'}),
			# 'id_disciplina_horario': forms.Select(attrs={'class':'form-control select2'}),
			'estado': forms.CheckboxInput(),
			'costo': forms.NumberInput(attrs={'class':'form-control'}),	
		}
	def __init__(self, *args, **kwargs):
		super(MatriculaForm, self).__init__(*args, **kwargs)
		for field in iter(self.fields):
			if field is not 'estado':
				self.fields[field].widget.attrs.update({'class':'form-control'})

class PrematriculaForm(forms.ModelForm):

	class Meta:
		# model = Deportista
		# fields = '__all__'
		exclude = ['id_deportista']
		model = Matricula
		fields = {
			'id_deportista',
			# 'id_disciplina_horario',
			# 'estado',
			'costo',
		}
		labels = {
			'id_deportista':'Elegir Deportista',
			# 'id_disciplina_horario':'Escoger Disciplina-Horario',
			# 'estado':'Estado',
			'costo':'Costo',
		}
		widgets = {
			'id_deportista': forms.Select(attrs={'class':'form-control '}),
			# 'id_disciplina_horario': forms.Select(attrs={'class':'form-control'}),
			# 'estado': forms.CheckboxInput(),
			'costo': forms.NumberInput(attrs={'class':'form-control', 'placeholder':'25'}),	
			# 'costo': forms.NumberInput(attrs={'readonly':'True','class':'form-control', 'placeholder':'25'}),	
		}
	# def __init__(self, *args, **kwargs):
	# 	super(PrematriculaForm, self).__init__(*args, **kwargs)
	# 	self.fields['costo'].widget.attrs['disabled'] = 'disabled'
class DeportistaPreinscripcionForm(forms.ModelForm):

	class Meta:
		model = AuthUser
		fields = '__all__'
		exclude = ['is_superuser', 'is_staff', 'is_active', 'es_secretaria', 'es_deportista', 'es_instructor', 'es_administrador', 'costo', 'username', 'password', 'first_name', 'last_name', 'email']
		labels = {
			'dni':'Cédula',
			'first_name':'Nombres',
			'last_name':'Apellidos',
			'email':'Email',
			'genero':'Género',
			'edad':'Edad',
			'direccion':'Dirección',
			'telefono':'Teléfono',
			'celular':'Celular',
			'img_perfil':'Imágen perfil',
			# 'username':'Usuario',
			# 'password':'Password',
		}
		widgets = {
			'dni': forms.TextInput(attrs={'class':'form-control','placeholder':'110****'}),
			'first_name': forms.TextInput(attrs={'class':'form-control','placeholder':'Jose Anibal'}),
			'last_name': forms.TextInput(attrs={'class':'form-control','placeholder':'Cueva Correa'}),
			'email': forms.EmailInput(attrs={'class':'form-control','placeholder':'example@gmail.com'}),
			'genero': forms.Select(attrs={'class':'form-control','placeholder':'MASCULINO'}),
			'edad': forms.NumberInput(attrs={'class':'form-control','placeholder':'25'}),
			'direccion': forms.TextInput(attrs={'class':'form-control','placeholder':'Loja'}),
			'telefono': forms.NumberInput(attrs={'class':'form-control','placeholder':'072***'}),
			'celular': forms.NumberInput(attrs={'class':'form-control','placeholder':'09****'}),
			'img_perfil': forms.FileInput(attrs={'class':'form-control'}),
			# 'username': forms.TextInput(attrs={'class':'form-control','placeholder':'Username'}),
			# 'password': forms.PasswordInput(attrs={'class':'form-control','placeholder':'********'}),
		}
# Administrcion pagina
class InicioForm(forms.ModelForm):

	class Meta:
		model = InicioSlide
		fields = '__all__'
		exclude = ['nombre_enlace', 'direccion_enlace']
		labels = {
			'id_gimnasio':'Gimnasio',
			'img_slide':'Slider',
			'imgbtn_slide':'Botón Slider',
			'titulo_slide':'Título',
			'subt_slide':'Subtítulo',
			'nombre_enlace':'Enlace',
			'direccion_enlace':'Direccin de enlace',
		}
		widgets = {
			'id_gimnasio': forms.Select(attrs={'class':'form-control'}),
			'img_slide': forms.FileInput(attrs={'class':'form-control'}),
			'imgbtn_slide': forms.FileInput(attrs={'class':'form-control'}),
			'titulo_slide': forms.TextInput(attrs={'class':'form-control','placeholder':'Club Deportivo Deos'}),
			'subt_slide': forms.TextInput(attrs={'class':'form-control'}),
			'nombre_enlace': forms.TextInput(attrs={'class':'form-control','placeholder':'Facebook'}),
			'direccion_enlace': forms.TextInput(attrs={'class':'form-control','placeholder':'www.facebook.com'}),
		}
class ParallaxForm(forms.ModelForm):

	class Meta:
		model = Parallax
		fields = '__all__'
		exclude = ['enlace_video']
		labels = {
			'id_gimnasio':'Gimnasio',
			'img_parallax':'Imágen',
			'title_parallax':'Título',
			'desc_parallax':'Descripción',
			'imginfe_parallax':'Imágen Inferior',
		}
		widgets = {
			'id_gimnasio': forms.Select(attrs={'class':'form-control'}),
			'img_parallax': forms.FileInput(attrs={'class':'form-control'}),
			'title_parallax': forms.TextInput(attrs={'class':'form-control'}),
			'desc_parallax': forms.TextInput(attrs={'class':'form-control'}),
			'imginfe_parallax': forms.FileInput(attrs={'class':'form-control'}),
		}
class ParallaxUnoForm(forms.ModelForm):

	class Meta:
		model = ParallaxUno
		fields = '__all__'
		exclude = ['back_parallaxuno']
		labels = {
			'id_gimnasio':'Gimnasio',
			'back_parallaxuno':'Imágen Fondo',
			'img_parallaxuno':'Imágen',
			'title_parallaxuno':'Título',
			'subt_parallaxuno':'Dirección',
		}
		widgets = {
			'id_gimnasio': forms.Select(attrs={'class':'form-control'}),
			'back_parallaxuno': forms.FileInput(attrs={'class':'form-control'}),
			'img_parallaxuno': forms.FileInput(attrs={'class':'form-control'}),
			'title_parallaxuno': forms.TextInput(attrs={'class':'form-control'}),
			'subt_parallaxuno': forms.TextInput(attrs={'class':'form-control'}),
		}
class ParallaxDosForm(forms.ModelForm):

	class Meta:
		model = ParallaxDos
		fields = '__all__'
		exclude = ['img_parallaxdos']
		labels = {
			'id_gimnasio':'Gimnasio',
			'img_parallaxdos':'Imágen Fondo',
			'costo_parallaxdos':'Valor Inscripcián',
			'dias_parallaxtres':'Días',
			'desc_parallaxdos':'Descripción',
		}
		widgets = {
			'id_gimnasio': forms.Select(attrs={'class':'form-control'}),
			'img_parallaxdos': forms.FileInput(attrs={'class':'form-control'}),
			'costo_parallaxdos': forms.TextInput(attrs={'class':'form-control'}),
			'dias_parallaxtres': forms.TextInput(attrs={'class':'form-control'}),
			'desc_parallaxdos': forms.TextInput(attrs={'class':'form-control'}),
		}
class AcercaForm(forms.ModelForm):

	class Meta:
		model = AcercaDe
		fields = '__all__'
		exclude = ['img_uno','img_dos']
		labels = {
			'id_gimnasio':'Gimnasio',
			'titulo_acerca':'Título',
			'desc_acerca':'Descripción',
			'sidea_acerca':'Misión',
			'sidea1_acerca':'Visión',
			'sideb_acerca':'Valores',
			'sideb1_acerca':'Participaciones',
			# 'img_uno':'Imagen uno',
			# 'img_dos':'Imagen dos',
		}
		widgets = {
			'id_gimnasio': forms.Select(attrs={'class':'form-control'}),
			'titulo_acerca': forms.TextInput(attrs={'class':'form-control'}),
			'desc_acerca': forms.TextInput(attrs={'class':'richtexteditor'}),
			'sidea_acerca': forms.TextInput(attrs={'class':'form-control'}),
			'sidea1_acerca': forms.TextInput(attrs={'class':'form-control'}),
			'sideb_acerca': forms.TextInput(attrs={'class':'form-control'}),
			'sideb1_acerca': forms.TextInput(attrs={'class':'form-control'}),
			# 'img_uno': forms.FileInput(attrs={'class':'form-control'}),
			# 'img_dos': forms.FileInput(attrs={'class':'form-control'}),
		}
class ProductoForm(forms.ModelForm):

	class Meta:
		model = Producto
		fields = '__all__'
		labels = {
			'id_gimnasio':'Gimnasio',
			'nombre_prod':'Nombre',
			'desc_prod':'Descripción',
			'img_prod':'Imágen',
			'precio_prod':'Precio',
		}
		widgets = {
			'id_gimnasio': forms.Select(attrs={'class':'form-control'}),
			'nombre_prod': forms.TextInput(attrs={'class':'form-control'}),
			'desc_prod': forms.TextInput(attrs={'class':'form-control'}),
			'img_prod': forms.FileInput(attrs={'class':'form-control'}),
			'precio_prod': forms.NumberInput(attrs={'class':'form-control'}),
		}
class GaleriaForm(forms.ModelForm):

	class Meta:
		model = Galeria
		fields = '__all__'
		labels = {
			'id_gimnasio':'Gimnasio',
			'titulo_galeria':'Título',
			'img_galeria':'Imágen',
			'desc_galeria':'Descripción',
		}
		widgets = {
			'id_gimnasio': forms.Select(attrs={'class':'form-control'}),
			'titulo_galeria': forms.TextInput(attrs={'class':'form-control'}),
			'img_galeria': forms.FileInput(attrs={'class':'form-control'}),
			'desc_galeria': forms.TextInput(attrs={'class':'form-control'}),
		}
class ContactoForm(forms.ModelForm):

	class Meta:
		model = Contacto
		fields = '__all__'
		exclude = ['phone_tres']
		labels = {
			'id_gimnasio':'Gimnasio',
			'titulo_contac':'Título',
			'desc_contact':'Descripción',
			'direccion_contact':'Dirección',
			'phone_uno':'Teléfono',
			'phone_dos':'Celular',
		}
		widgets = {
			'id_gimnasio': forms.Select(attrs={'class':'form-control'}),
			'titulo_contac': forms.TextInput(attrs={'class':'form-control'}),
			'desc_contact': forms.TextInput(attrs={'class':'form-control'}),
			'direccion_contact': forms.TextInput(attrs={'class':'form-control'}),
			'phone_uno': forms.TextInput(attrs={'class':'form-control','placeholder':'072****'}),
			'phone_dos': forms.TextInput(attrs={'class':'form-control','placeholder':'+593 98278**'}),
		}
class DeportistaDestacadoForm(forms.ModelForm):
	class Meta:
		model = DeportistaDestacado
		fields = '__all__'
		labels = {
			'nombre':'Nombres',
			'apellidos':'Apellidos',
			'disciplina':'Disciplina',
			'nivel':'Nivel',
			'descripcion':'Descripción',
		}
		widgets = {
			'nombre': forms.TextInput(attrs={'class':'form-control'}),
			'apellidos': forms.TextInput(attrs={'class':'form-control'}),
			'disciplina': forms.TextInput(attrs={'class':'form-control'}),
			'nivel': forms.TextInput(attrs={'class':'form-control'}),
			'descripcion': forms.Textarea(attrs={'class':'form-control'}),

		}
class DeportistaDestacadoEditForm(forms.ModelForm):
	class Meta:
		model = DeportistaDestacado
		fields = '__all__'
		labels = {
			'nombre':'Nombres',
			'apellidos':'Apellidos',
			'disciplina':'Disciplina',
			'nivel':'Nivel',
			'descripcion':'Descripción',
			'imagen':'Imágen',
		}
		widgets = {
			'nombre': forms.TextInput(attrs={'class':'form-control'}),
			'apellidos': forms.TextInput(attrs={'class':'form-control'}),
			'disciplina': forms.TextInput(attrs={'class':'form-control'}),
			'nivel': forms.TextInput(attrs={'class':'form-control'}),
			'descripcion': forms.Textarea(attrs={'class':'form-control'}),
			'imagen': forms.FileInput(attrs={'class':'form-control'}),

		}
