# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-05-05 01:19
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app_deos', '0004_auto_20180504_1918'),
    ]

    operations = [
        migrations.AlterModelTable(
            name='deportistadestacado',
            table='deportista_destacado',
        ),
    ]
