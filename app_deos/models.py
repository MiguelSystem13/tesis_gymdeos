# -*- coding: utf-8 -*- 
# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField
from django.contrib.auth.models import User

class AcercaDe(models.Model):
    id_acerca = models.AutoField(primary_key=True)
    id_gimnasio = models.ForeignKey('Gimnasio', models.DO_NOTHING, db_column='id_gimnasio')
    titulo_acerca = models.CharField(max_length=50)
    desc_acerca = RichTextUploadingField()
    sidea_acerca = models.CharField(max_length=300)
    sidea1_acerca = models.CharField(max_length=300)
    sideb_acerca = RichTextUploadingField()
    sideb1_acerca = RichTextUploadingField()    
    img_uno = models.ImageField(max_length=150, blank=True, null=True, upload_to="acercade/%Y/%m/%d")
    img_dos = models.ImageField(max_length=150, blank=True, null=True, upload_to="acercade/%Y/%m/%d")

    class Meta:
        managed = False
        db_table = 'acerca_de'


class Administrador(models.Model):
    id_administrador = models.AutoField(primary_key=True)
    id = models.ForeignKey('AuthUser', db_column='id', on_delete=models.CASCADE)

    class Meta:
        managed = False
        db_table = 'administrador'
    def __str__(self):
        return self.id.first_name

class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=80)

    class Meta:
        managed = False
        db_table = 'auth_group'
    def __str__(self):
        return self.name

class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)
    # def __str__(self):
    #     return self.group

class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)
    def __str__(self):
        return self.name

class AuthUser(models.Model):
    dni= models.CharField(unique=True,max_length=10)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.CharField(max_length=254)
    genero = models.CharField(max_length=10, choices=(('Masculino', 'Masculino'),('Femenino','Femenino')))
    edad = models.CharField(max_length=3)
    direccion = models.CharField(max_length=300)
    telefono = models.CharField(max_length=10, blank=True, null=True)
    celular = models.CharField(max_length=10)
    img_perfil = models.ImageField(max_length=500,null=True,blank=True, upload_to="usuario/%Y/%m/%d")
    username = models.CharField(unique=True, max_length=150)
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(auto_now_add=True)
    is_superuser = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=True)
    is_active = models.BooleanField(default=True)
    date_joined = models.DateTimeField(auto_now_add=True)
    es_administrador = models.BooleanField()
    es_deportista = models.BooleanField()
    es_instructor = models.BooleanField()
    es_secretaria = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'auth_user'
    def __str__(self):
        return self.first_name + ' ' + self.last_name

class AuthUserGroups(models.Model):
    user = models.ForeignKey(AuthUser, on_delete=models.CASCADE)
    group = models.ForeignKey(AuthGroup, on_delete=models.CASCADE)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class Contacto(models.Model):
    id_contacto = models.AutoField(primary_key=True)
    id_gimnasio = models.ForeignKey('Gimnasio', models.DO_NOTHING, db_column='id_gimnasio')
    titulo_contac = models.CharField(max_length=50)
    desc_contact = RichTextUploadingField()
    direccion_contact = models.CharField(max_length=200)
    phone_uno = models.IntegerField()
    phone_dos = models.IntegerField()
    phone_tres = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'contacto'


class Deportista(models.Model):
    id_deportista = models.AutoField(primary_key=True)
    id = models.ForeignKey(AuthUser, db_column='id', on_delete=models.CASCADE)

    class Meta:
        managed = False
        db_table = 'deportista'
    def __str__(self):
        return self.id.first_name + ' ' + self.id.last_name

class Disciplina(models.Model):
    id_disciplina = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=50)
    # categoria = models.CharField(max_length=30, choices=(('Ninos', 'Ninos'),('Adultos','Adultos'), ('Todos','Todos')))
    img_disciplina = models.ImageField(max_length=150, upload_to="disciplina/%Y/%m/%d")

    class Meta:
        managed = False
        db_table = 'disciplina'
    def __str__(self):
        return self.nombre

class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class Email(models.Model):
    id_email = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=50)
    direccion = models.CharField(max_length=100, blank=True, null=True)
    email = models.CharField(max_length=80)
    telefono = models.CharField(max_length=10, blank=True, null=True)
    mensaje = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'email'


class Galeria(models.Model):
    id_galeria = models.AutoField(primary_key=True)
    id_gimnasio = models.ForeignKey('Gimnasio', models.DO_NOTHING, db_column='id_gimnasio')
    titulo_galeria = models.CharField(max_length=50)
    img_galeria = models.ImageField(max_length=500, upload_to="galeria/%Y/%m/%d")
    desc_galeria = models.CharField(max_length=500)

    class Meta:
        managed = False
        db_table = 'galeria'


class Gimnasio(models.Model):
    id_gimnasio = models.AutoField(primary_key=True)
    nombre_gym = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'gimnasio'
    def __str__(self):
        return self.nombre_gym

class Horario(models.Model):
    id_horario = models.AutoField(primary_key=True)
    hora_inicio = models.TimeField()
    hora_fin = models.TimeField()
    ubicacion = models.CharField(max_length=20, blank=True, null=True)
    sesion = models.CharField(max_length=10, choices=(('Mañana', 'Mañana'), ('Tarde','Tarde'), ('Noche','Noche')))
    categoria = models.CharField(max_length=20, choices=(('4 a 6 años', '4 a 6 años'), ('7 a 13 años','7 a 13 años'), ('9 a 17 años','9 a 17 años'),('Avanzados','Avanzados')))
    cupos = models.IntegerField(blank=True, null=True)
    desc_horario = models.CharField(max_length=500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'horario'
    def __str__(self):#__str__ para python 3
            String = "%s %s %s" %(self.hora_inicio, self.hora_fin, self.categoria)
            return String  

class InicioSlide(models.Model):
    id_inicioslide = models.AutoField(primary_key=True)
    id_gimnasio = models.ForeignKey(Gimnasio, db_column='id_gimnasio')
    img_slide = models.ImageField(max_length=150, upload_to="inicio/%Y/%m/%d")
    imgbtn_slide = models.ImageField(max_length=150, upload_to="inicio/%Y/%m/%d")
    titulo_slide = models.CharField(max_length=50)
    subt_slide = models.CharField(max_length=50)
    nombre_enlace = models.CharField(max_length=50, blank=True, null=True)
    direccion_enlace = models.CharField(max_length=300, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'inicio_slide'


class Instructor(models.Model):
    id_instructor = models.AutoField(primary_key=True)
    id = models.ForeignKey(AuthUser, db_column='id', on_delete=models.CASCADE)
    inst_profesion = models.CharField(max_length=50, blank=True, null=True)
    inst_estado = models.BooleanField()
    inst_descripcion = models.CharField(max_length=500, blank=True, null=True)
    inst_imagen = models.CharField(max_length=150, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'instructor'
    def __str__(self):
        return '%s' %(self.id)

class Matricula(models.Model):
    id_matricula = models.AutoField(primary_key=True)
    id_deportista = models.ForeignKey(Deportista, db_column='id_deportista', on_delete=models.CASCADE)    
    id_secretaria = models.ForeignKey('Secretaria', null=True, blank=True, db_column='id_secretaria', on_delete=models.CASCADE)    
    estado = models.BooleanField(default=False)
    fecha_matricula = models.DateTimeField(auto_now_add=True)
    costo = models.FloatField()

    class Meta:
        managed = False
        db_table = 'matricula'
    def __str__(self):
        return '%s' % (self.id_deportista)

class DisciplinaHorario(models.Model):
    id_disciplina_horario = models.AutoField(primary_key=True)
    id_disciplina = models.ForeignKey(Disciplina, db_column='id_disciplina', on_delete=models.CASCADE)
    id_horario = models.ForeignKey('Horario', db_column='id_horario', on_delete=models.CASCADE)
    id_instructor = models.ForeignKey('Instructor', db_column='id_instructor', on_delete=models.CASCADE)    
    class Meta:
        managed = False
        db_table = 'disciplina_instructor'
    def __str__(self):
        return '%s - %s' %(self.id_disciplina,self.id_horario)

class MatriculaDisciplina(models.Model):
    id_matriculadisciplina = models.AutoField(primary_key=True)
    id_matricula = models.ForeignKey(Matricula, db_column='id_matricula', on_delete=models.CASCADE)
    id_disciplina_horario = models.ForeignKey(DisciplinaHorario, db_column='id_disciplina_horario', on_delete=models.CASCADE)
    estado = models.BooleanField(default=False)
    fecha_matricula = models.DateTimeField(auto_now_add=True)
    costo = models.FloatField(default=0)
    class Meta:
        managed = False
        db_table = 'matricula_disciplina'
    def __str__(self):
        return '%s - %s' % (self.id_matricula.id_deportista, self.id_disciplina_horario)

class Mensualidad(models.Model):
    id_mensualidad = models.AutoField(primary_key=True)
    id_matriculadisciplina = models.ForeignKey(MatriculaDisciplina, db_column='id_matriculadisciplina', on_delete=models.CASCADE)
    concepto = models.CharField(max_length=100, default="PAGO MENSUAL")
    total = models.FloatField(default=25)
    pagado = models.BooleanField(default=False)
    fecha_mensualidad = models.DateField()
    fecha_pago = models.DateField(auto_now_add=True)

    class Meta:
        managed = False
        db_table = 'mensualidad'


class Parallax(models.Model):
    id_parallax = models.AutoField(primary_key=True)
    id_gimnasio = models.ForeignKey(Gimnasio, db_column='id_gimnasio', on_delete=models.CASCADE)
    img_parallax = models.ImageField(max_length=150, upload_to="parallax/%Y/%m/%d")
    title_parallax = models.CharField(max_length=50)
    desc_parallax = models.CharField(max_length=700)
    imginfe_parallax = models.ImageField(max_length=150, upload_to="parallax/%Y/%m/%d")
    enlace_video = models.CharField(max_length=300, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'parallax'


class ParallaxDos(models.Model):
    id_parallaxdos = models.AutoField(primary_key=True)
    id_gimnasio = models.ForeignKey(Gimnasio, db_column='id_gimnasio')
    img_parallaxdos = models.ImageField(max_length=150, upload_to="parallaxdos/%Y/%m/%d")
    costo_parallaxdos = models.FloatField()
    dias_parallaxtres = models.CharField(max_length=20)
    desc_parallaxdos = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'parallax_dos'


class ParallaxUno(models.Model):
    id_parallaxuno = models.AutoField(primary_key=True)
    id_gimnasio = models.ForeignKey(Gimnasio, db_column='id_gimnasio')
    back_parallaxuno = models.ImageField(max_length=150, upload_to="parallaxuno/%Y/%m/%d")
    img_parallaxuno = models.ImageField(max_length=150, upload_to="parallaxuno/%Y/%m/%d")
    title_parallaxuno = models.CharField(max_length=50)
    subt_parallaxuno = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'parallax_uno'


class Producto(models.Model):
    id_producto = models.AutoField(primary_key=True)
    id_gimnasio = models.ForeignKey(Gimnasio, db_column='id_gimnasio')
    nombre_prod = models.CharField(max_length=30)
    desc_prod = models.CharField(max_length=500)
    img_prod = models.ImageField(max_length=500, upload_to="producto/%Y/%m/%d")
    precio_prod = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'producto'


class Secretaria(models.Model):
    id_secretaria = models.AutoField(primary_key=True)
    id = models.ForeignKey(AuthUser, db_column='id', on_delete=models.CASCADE)
    id_administrador = models.ForeignKey(Administrador, db_column='id_administrador', blank=True, null=True, on_delete=models.CASCADE)

    class Meta:
        managed = False
        db_table = 'secretaria'
    def __str__(self):
        return self.id.first_name

class Seguimiento(models.Model):
    id_seguimiento = models.AutoField(primary_key=True)
    diametro_brazo = models.FloatField(blank=True, null=True)
    fecha = models.DateField()
    id_matricula = models.ForeignKey(Matricula, db_column='id_matricula', on_delete=models.CASCADE)

    class Meta:
        managed = False
        db_table = 'seguimiento'
class DeportistaDestacado(models.Model):
    id_deportista_des = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=50)
    apellidos = models.CharField(max_length=50)
    descripcion = models.CharField(max_length=50)
    nivel = models.CharField(max_length=50, blank=True, null=True)
    disciplina = models.CharField(max_length=50)
    imagen = models.ImageField(max_length=1000, blank=True,null=True,upload_to="destacados/%Y/%m/%d")

    class Meta:
        managed = False
        db_table = 'deportista_destacado'