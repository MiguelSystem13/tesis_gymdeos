# -*- coding: utf-8 -*-
# from django.conf.urls import url, include
from django.conf.urls import *
from django.conf import settings
from django.contrib import admin
from django.conf.urls.static import static
from .import views 
from  .views import *
from django.contrib.auth import views as auth_views
from django.contrib.auth.views import *
from django.conf.urls import handler404
# permisos
from django.contrib.auth.decorators import *
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^ver_inicio/$', views.ver_inicio, name='ver_inicio' ),    
    url(r'^nuevo_inicio/$', login_required(views.nuevo_inicio), name='nuevo_inicio' ),
    url(r'^editar_inicio/(?P<id_inicioslide>\d+)/$', login_required(views.editar_inicio), name='editar_inicio' ),
    url(r'^eliminar_inicio/(?P<id_inicioslide>\d+)/$', login_required(views.eliminar_inicio), name='eliminar_inicio' ),
    url(r'^ver_parallax/$', login_required(views.ver_parallax), name='ver_parallax' ),
    url(r'^nuevo_parallax/$', login_required(views.nuevo_parallax), name='nuevo_parallax' ),
    url(r'^editar_parallax/(?P<id_parallax>\d+)/$', login_required(views.editar_parallax), name='editar_parallax' ),
    url(r'^eliminar_parallax/(?P<id_parallax>\d+)/$', login_required(views.eliminar_parallax), name='eliminar_parallax' ),
    url(r'^ver_parallaxuno/$', login_required(views.ver_parallaxuno), name='ver_parallaxuno' ),
    url(r'^nuevo_parallaxuno/$', login_required(views.nuevo_parallaxuno), name='nuevo_parallaxuno' ),
    url(r'^editar_parallaxuno/(?P<id_parallaxuno>\d+)/$', login_required(views.editar_parallaxuno), name='editar_parallaxuno' ),
    url(r'^eliminar_parallaxuno/(?P<id_parallaxuno>\d+)/$', login_required(views.eliminar_parallaxuno), name='eliminar_parallaxuno' ),
    url(r'^ver_parallaxdos/$', login_required(views.ver_parallaxdos), name='ver_parallaxdos' ),
    url(r'^nuevo_parallaxdos/$', login_required(views.nuevo_parallaxdos), name='nuevo_parallaxdos' ),
    url(r'^editar_parallaxdos/(?P<id_parallaxdos>\d+)/$', login_required(views.editar_parallaxdos), name='editar_parallaxdos' ),
    url(r'^eliminar_parallaxdos/(?P<id_parallaxdos>\d+)/$', login_required(views.eliminar_parallaxdos), name='eliminar_parallaxdos' ),
    # acerca  
    url(r'^about/$', views.about, name='acerca' ),
    url(r'^reporte/(?P<ide>\S+)/$', views.reporte, name='reporte' ),
    url(r'^ver_acerca/$', views.ver_acerca, name='ver_acerca' ),
    url(r'^nuevo_acerca/$', views.nuevo_acerca, name='nuevo_acerca' ),
    url(r'^editar_acerca/(?P<id_acerca>\d+)/$', views.editar_acerca, name='editar_acerca' ),
    url(r'^eliminar_acerca/(?P<id_acerca>\d+)/$', views.eliminar_acerca, name='eliminar_acerca' ),

    url(r'^schedule/$', views.schedule, name='horario' ),
    url(r'^classes/$', views.classes, name='disciplina' ),
    url(r'^taekwondo/$', views.taekwondo, name='taekwondo' ),
    url(r'^read_disciplina/$', views.read_disciplina, name='leer_disciplina' ),
    url(r'^trainer/$', views.trainer, name='entrenador' ),
    # producto
    url(r'^store/$', views.store, name='producto' ),
    url(r'^ver_producto/$', views.ver_producto, name='ver_producto' ),
    url(r'^nuevo_producto/$', views.nuevo_producto, name='nuevo_producto' ),
    url(r'^editar_producto/(?P<id_producto>\d+)/$', views.editar_producto, name='editar_producto' ),
    url(r'^eliminar_producto/(?P<id_producto>\d+)/$', views.eliminar_producto, name='eliminar_producto' ),
    # galeria
    url(r'^gallery/$', views.gallery, name='galeria' ),
    url(r'^ver_galeria/$', views.ver_galeria, name='ver_galeria' ),
    url(r'^nueva_galeria/$', views.nueva_galeria, name='nueva_galeria' ),
    url(r'^editar_galeria/(?P<id_galeria>\d+)/$', views.editar_galeria, name='editar_galeria' ),
    url(r'^eliminar_galeria/(?P<id_galeria>\d+)/$', views.eliminar_galeria, name='eliminar_galeria' ),
    # contacto
    url(r'^location/$', views.location, name='ubicacion' ),
    url(r'^ver_contacto/$', views.ver_contacto, name='ver_contacto' ),
    url(r'^nuevo_contacto/$', views.nuevo_contacto, name='nuevo_contacto' ),
    url(r'^editar_contacto/(?P<id_contacto>\d+)/$', views.editar_contacto, name='editar_contacto' ),
    url(r'^eliminar_contacto/(?P<id_contacto>\d+)/$', views.eliminar_contacto, name='eliminar_contacto' ),
    # deportistas destacado
    url(r'^ver_destacado/$', views.ver_destacados, name='ver_destacados' ),
    url(r'^nuevo_destacado/$', views.nuevo_destacado, name='nuevo_destacado' ),
    url(r'^editar_destacado/(?P<id>\d+)/$', views.editar_destacado, name='editar_destacado' ),

    # administracion app
    url(r'^index_admin/$', views.index_admin, name='index_admin' ),
    url(r'^admindeos/ver_usuario/$', views.ver_usuario, name='ver_usuario' ),
    url(r'^admindeos/nuevo_usuario/$', views.nuevo_usuario, name='nuevo_usuario' ),
    url(r'^admindeos/newuser_prueba/$', views.newuser_prueba, name='newuser_prueba' ),
    url(r'^admindeos/editar_usuario/(?P<id>\d+)/$', views.editar_usuario, name='editar_usuario' ),
    # url(r'^admindeos/editar_usuario/(?P<id>\d+)/$', views.editar_usuario, name='editar_usuario' ),
    # url(r'^admindeos/editar_usuario/(?P<pk>\d+)/$', update_usuario.as_view(), name='editar_usuario' ),
    url(r'^admindeos/eliminar_usuario/(?P<id>\d+)/$', views.eliminar_usuario, name='eliminar_usuario' ),
    url(r'^admindeos/ver_perfil/(?P<id>\d+)/$', views.ver_perfil, name='ver_perfil' ),
    # Instrcutor
    url(r'^admindeos/ver_instructor/$', ver_instructor.as_view(), name='ver_instructor' ),
    url(r'^admindeos/nuevo_instructor/$', views.nuevo_instructor, name='nuevo_instructor' ),
    url(r'^admindeos/editar_instructor/(?P<id>\d+)/$', views.editar_instructor, name='editar_instructor' ),
    url(r'^admindeos/eliminar_instructor/(?P<id>\d+)/$', views.eliminar_instructor, name='eliminar_instructor' ),
    url(r'^admindeos/perfil_instructor/(?P<id>\d+)/$', views.perfil_instructor, name='perfil_instructor' ),
    # horario
    url(r'^admindeos/ver_horario/$', ver_horario.as_view(), name='ver_horario' ),
    url(r'^admindeos/editar_horario/(?P<id_horario>\d+)/$', views.editar_horario, name='editar_horario' ),
    url(r'^admindeos/eliminar_horario/(?P<id>\d+)/$', views.eliminar_horario, name='eliminar_horario' ),
    url(r'^admindeos/nuevo_horario/$', views.nuevo_horario, name='nuevo_horario' ),
    # url(r'^nuevo_horario/$', nuevo_horario.as_view(), name='nuevo_horario' ),
    # DisciplinaHorario
    url(r'^admindeos/ver_disciplinahorario/$', views.ver_disciplinahorario, name='ver_disciplinahorario' ),
    url(r'^admindeos/nueva_disciplinahorario/$', views.nueva_disciplinahorario, name='nueva_disciplinahorario' ),
    url(r'^admindeos/editar_disciplinahorario/(?P<id_disciplinahorario>\d+)/$', views.editar_disciplinahorario, name='editar_disciplinah' ),
    url(r'^admindeos/eliminar_disciplinah/(?P<id>\d+)/$', views.eliminar_disciplinah, name='eliminar_disciplinah' ),

    # startadmmatricula
    url(r'^admindeos/ver_matricula/$', views.ver_matricula, name='ver_matricula' ),
    url(r'^admindeos/nueva_matricula/$', views.nueva_matricula, name='nueva_matricula' ),
    url(r'^admindeos/validar_matricula/(?P<id_matricula>\d+)/$', views.validar_matricula, name='validar_matricula' ),
    url(r'^admindeos/editar_matricula/(?P<id_matricula>\d+)/$', views.editar_matricula, name='editar_matricula' ),
    url(r'^admindeos/buscar_matricula/$', views.buscar_usuario, name='buscar_usuario' ),
    url(r'^admindeos/buscar_deportista$', views.buscar_deportista),
    url(r'^admindeos/info_matricula/(?P<id>\d+)/$', views.info_matricula, name='info_matricula' ),
    url(r'^admindeos/eliminar_matricula/(?P<id_matricula>\d+)/$', views.eliminar_matricula, name='eliminar_matricula' ),
    # url(r'^nueva_matricula/$', nueva_matricula.as_view(), name='nueva_matricula' ),
    # startadisciplina - 
    url(r'^admindeos/ver_disciplina/$', views.ver_disciplina, name='ver_disciplina' ),
    url(r'^admindeos/nueva_disciplina/$', login_required(views.nueva_disciplina), name='nueva_disciplina' ),
    url(r'^admindeos/editar_disciplina/(?P<id_disciplina>\d+)/$', login_required(views.editar_disciplina), name='editar_disciplina' ),
    url(r'^admindeos/eliminar_disciplina/(?P<id>\d+)/$', views.eliminar_disciplina, name='eliminar_disciplina' ),
    # url(r'^search/$', views.search, name='search' ),
    # url(r'^search1/$', views.search1, name='search1' ),
    # url(r'^editar_disciplina/(?P<id_disciplina>\d+)/$', views.editar_disciplina, name='editar_disciplina' ),
    # url(r'^eliminar_disciplina/(?P<id_disciplina>\d+)/$', views.eliminar_disciplina, name='eliminar_disciplina' ),
    # startdeportista
    url(r'^admindeos/ver_deportista/$', ver_deportista.as_view(), name='ver_deportista' ),
    # url(r'^admindeos/nuevo_deportista/$', views.register, name='nuevo_deportista' ),
    url(r'^admindeos/nuevo_deportista1/$', views.nuevo_deportista1, name='nuevo_deportista1' ),
    url(r'^admindeos/editar_deportista1/(?P<id>\d+)/$', views.editar_deportista1, name='editar_deportista1' ),
    url(r'^admindeos/eliminar_deportista/(?P<id>\d+)/$', views.eliminar_deportista1, name='eliminar_deportista1' ),
    url(r'^admindeos/perfil_deportista/(?P<id>\d+)/$', views.perfil_deportista, name='perfil_deportista' ),
    # mensualidades
    url(r'^admindeos/ver_mensualidad/$',ver_mensualidad.as_view(), name='ver_mensualidad' ),
    url(r'^admindeos/nueva_mensualidad/$', views.nueva_mensualidad, name='nueva_mensualidad' ),
    url(r'^admindeos/editar_mensualidad/(?P<id_mensualidad>\d+)/$', views.editar_mensualidad, name='editar_mensualidad' ),
    url(r'^admindeos/eliminar_mensualidad/(?P<id_mensualidad>\d+)/$', views.eliminar_mensualidad, name='eliminar_mensualidad' ),
    
    # grupos
    url(r'^admindeos/ver_grupos/$', views.ver_grupos, name='ver_grupos' ),
    url(r'^admindeos/nuevo_grupo/$', views.nuevo_grupo, name='nuevo_grupo' ),
    url(r'^admindeos/editar_grupo/(?P<id>\d+)/$', views.editar_grupo, name='editar_grupo' ),
    url(r'^admindeos/eliminar_grupo/(?P<id>\d+)/$', views.eliminar_grupo, name='eliminar_grupo' ),
    
    # Pre-inscripciones
    # url(r'^admindeos/ver_preinscripciones/$', login_required(ver_preinscripciones.as_view()), name='ver_preinscripciones' ),
    # url(r'^preinscripcion/$', login_required(views.nueva_preinscripcion), name='preinscripcion1'),
    url(r'^preinscripcion/$', views.pre_matricula, name='preinscripcion'),
    url(r'^preinscripcion_realizada/$', login_required(views.preinscripcion_realizada), name='preinscripcion_realizada'),
    # url(r'^nueva_preinscripcion/$', login_required(views.nueva_preinscripcion), name='nueva_preinscripcion' ),
    # LOGIN-LOGOUT-RESET_PASSWORD
    url(r'^account/login/$', login,{'template_name': 'accounts/loginv1.html'},name='login' ),
    url(r'^account/logout/$', auth_views.logout, {'template_name':'index.html'}, name='logout'),
    url(r'^account/register/$', views.register_deportista, name='register' ),
    url(r'^reset/password_reset/', password_reset,{'template_name':'accounts/password_reset_form.html','email_template_name':'accounts/password_reset_email.html'}, name='password_reset'), 
    url(r'^reset/password_reset_done/$', password_reset_done,{'template_name': 'accounts/password_reset_done.html'}, name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>.+)/$', password_reset_confirm,{'template_name':'accounts/password_reset_confirm.html'}, name='password_reset_confirm'),
    url(r'^reset/complete_reset/$', password_reset_complete,{'template_name':'accounts/password_reset_complete.html'}, name='password_reset_complete' ),
    url(r'^error404/$', views.error_404, name='error404' ),
    url(r'^email/$', views.sendEmail, name='email'),
    # reportes
    # url(r'^reporte_personas_pdf/$',ReportePersonasPDF.as_view(), name="reporte_personas_pdf"),
 ]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
 