# -*- coding: utf-8 -*-	
from __future__ import unicode_literals
from django.http import HttpResponseRedirect
from django.http import *
from django.views.generic import CreateView, ListView, UpdateView, DeleteView
from django.shortcuts import get_object_or_404
from django.shortcuts import render
from django.views.generic import *
from .models import *
from .forms import *
from django.shortcuts import redirect
from django.core.urlresolvers import reverse_lazy
from django.views.generic import *
from django.db.models import Q
from django.shortcuts import *
from datetime import datetime, timedelta
from django.contrib.auth import *
# reportes - incluye vistas genericas
from django.conf import settings
from io import BytesIO
from reportlab.pdfgen import canvas
# permisos
from django.contrib.auth.decorators import *
# from django.contrib.auth.decorators import login_required, user_passes_test
# register
from django.contrib.auth.forms import UserCreationForm
# grupos
from django.contrib.auth.models import *
from django import template
# error_404
# mesajes
from django.contrib import messages
# ajax
import json
from django.template import RequestContext
from django.core import serializers
# inline form formset
from django.forms.models import inlineformset_factory
# filtros
from .filters import *
#create user
from django.contrib.auth.hashers import make_password
# envio de correo
from django.core.mail import EmailMultiAlternatives
# reportes
from django.http import HttpResponse
from django.template import Context
from django.template.loader import get_template
from xhtml2pdf import pisa
from datetime import datetime
import json
# Fin de librerias para pdf
from django.utils.decorators import *
# adicipnal python3
import sys  
reload(sys)  
sys.setdefaultencoding('utf-8')
# grupos
# register = template.Library() 

# @register.filter(name='es') 
# def is_group(user, group_name):
#     group =  Group.objects.get(name=group_name) 
#     return group in user.groups.all() 
# Pagina
def link_callback(uri, rel):
    """
    Convert HTML URIs to absolute system paths so xhtml2pdf can access those
    resources
    """
    # use short variable names
    sUrl = settings.STATIC_URL      # Typically /static/
    sRoot = settings.STATIC_ROOT    # Typically /home/userX/project_static/
    mUrl = settings.MEDIA_URL       # Typically /static/media/
    mRoot = settings.MEDIA_ROOT     # Typically /home/userX/project_static/media/

    # convert URIs to absolute system paths
    if uri.startswith(mUrl):
        path = os.path.join(mRoot, uri.replace(mUrl, ""))
    elif uri.startswith(sUrl):
        path = os.path.join(sRoot, uri.replace(sUrl, ""))
    else:
        return uri  # handle absolute uri (ie: http://some.tld/foo.png)

    # make sure that file exists
    if not os.path.isfile(path):
            raise Exception(
                'media URI must start with %s or %s' % (sUrl, mUrl)
            )
    return path

def reporte(request,ide):
    template_path = 'reporte.html'
    horario = ide
    matricula = MatriculaDisciplina.objects.filter(id_disciplina_horario = horario)
    contador = MatriculaDisciplina.objects.filter(id_disciplina_horario = horario).count()
    mat = DisciplinaHorario.objects.get(id_disciplina_horario = horario)
    context = {'matricula':matricula,'contador':contador,'mat':mat}
    # Create a Django response object, and specify content_type as pdf
    response = HttpResponse(content_type='application/pdf')
    # response['Content-Disposition'] = 'attachment; filename="report.pdf"'
    # find the template and render it.
    template = get_template(template_path)
    html = template.render(context)
     # create a pdf
    pisaStatus = pisa.CreatePDF(
       html, dest=response, link_callback=link_callback)
    # if error then show some funy view
    if pisaStatus.err:
       return HttpResponse('We had some errors <pre>' + html + '</pre>')
    return response

def gym(request):
	# if request.method == 'POST'
	if request.method == "POST" and 'btn_gym' in request.POST:
		nombre_gym = request.POST['nombre_gym']
	gym = Gimnasio.objects.all()
	ctx = {'gym':gym}
	return render(request, 'nuevo_inicio.html', ctx )

@login_required
def ver_inicio(request):
		inicio = InicioSlide.objects.all()
		ctx = {'inicio':inicio}
		return render(request, 'adm_pagina/inicio/ver_inicio.html', ctx)

def nuevo_inicio(request):
    if request.method == 'POST':
        form_ini = InicioForm(request.POST or None, request.FILES or None)
        if form_ini.is_valid():
            inicio = form_ini.save()
            return redirect('ver_inicio')
    else:
        form_ini = InicioForm()
    return render(request, 'adm_pagina/inicio/nuevo_inicio.html', {'form_ini': form_ini})
def editar_inicio(request, id_inicioslide):
	a = get_object_or_404(InicioSlide, pk=id_inicioslide)
	if request.method == 'POST':
		form_ini = InicioForm(request.POST, request.FILES, instance=a)
		if form_ini.is_valid():
			form_ini.save()
			return redirect('ver_inicio')	
	else:
		form_ini = InicioForm(instance=a)
	return render(request, 'adm_pagina/inicio/nuevo_inicio.html', {'form_ini':form_ini})
def eliminar_inicio(request, id_inicioslide):
	a = get_object_or_404(InicioSlide, pk=id_inicioslide)
	if request.method == 'POST':
		a.delete()
		return redirect('ver_inicio')
	return render(request, 'adm_pagina/inicio/eliminar.html', {'a':a})
def ver_parallax(request):
	parallax = Parallax.objects.all()
	ctx = {'parallax':parallax}
	return render(request, 'adm_pagina/parallax/ver.html', ctx)

def nuevo_parallax(request):
    if request.method == 'POST':
        form_par = ParallaxForm(request.POST or None, request.FILES or None)
        if form_par.is_valid():
            inicio = form_par.save()
            return redirect('ver_parallax')
    else:
        form_par = ParallaxForm()
    return render(request, 'adm_pagina/parallax/nuevo.html', {'form_par': form_par})

def editar_parallax(request, id_parallax):
	a = get_object_or_404(Parallax, pk=id_parallax)
	if request.method == 'POST':
		form_par = ParallaxForm(request.POST, request.FILES, instance=a)
		if form_par.is_valid():
			form_par.save()
			return redirect('ver_parallax')	
	else:
		form_par = ParallaxForm(instance=a)
	return render(request, 'adm_pagina/parallax/nuevo.html', {'form_par':form_par})
def eliminar_parallax(request, id_parallax):
	a = get_object_or_404(Parallax, pk=id_parallax)
	if request.method == 'POST':
		a.delete()
		return redirect('ver_parallax')
	return render(request, 'adm_pagina/parallax/eliminar.html', {'a':a})

def ver_parallaxuno(request):
	parallaxuno = ParallaxUno.objects.all()
	ctx = {'parallaxuno':parallaxuno}
	return render(request, 'adm_pagina/parallaxuno/ver.html', ctx)

def nuevo_parallaxuno(request):
    if request.method == 'POST':
        form_paruno = ParallaxUnoForm(request.POST or None, request.FILES or None)
        if form_paruno.is_valid():
            inicio = form_paruno.save()
            return redirect('ver_parallaxuno')
    else:
        form_paruno = ParallaxUnoForm()
    return render(request, 'adm_pagina/parallaxUno/nuevo.html', {'form_paruno': form_paruno})

def editar_parallaxuno(request, id_parallaxuno):
	a = get_object_or_404(ParallaxUno, pk=id_parallaxuno)
	if request.method == 'POST':
		form_paruno = ParallaxUnoForm(request.POST, request.FILES, instance=a)
		if form_paruno.is_valid():
			form_paruno.save()
			return redirect('ver_parallaxuno')	
	else:
		form_paruno = ParallaxUnoForm(instance=a)
	return render(request, 'adm_pagina/parallaxuno/nuevo.html', {'form_paruno':form_paruno})
def eliminar_parallaxuno(request, id_parallaxuno):
	a = get_object_or_404(ParallaxUno, pk=id_parallaxuno)
	if request.method == 'POST':
		a.delete()
		return redirect('ver_parallaxuno')
	return render(request, 'adm_pagina/parallaxuno/eliminar.html', {'a':a})

def ver_parallaxdos(request):
	parallaxdos = ParallaxDos.objects.all()
	ctx = {'parallaxdos':parallaxdos}
	return render(request, 'adm_pagina/parallaxdos/ver.html', ctx)

def nuevo_parallaxdos(request):
    if request.method == 'POST':
        form_pardos = ParallaxDosForm(request.POST or None, request.FILES or None)
        if form_pardos.is_valid():
            parallax = form_pardos.save()
            return redirect('ver_parallaxdos')
    else:
        form_pardos = ParallaxDosForm()
    return render(request, 'adm_pagina/parallaxdos/nuevo.html', {'form_pardos': form_pardos})

def editar_parallaxdos(request, id_parallaxdos):
	a = get_object_or_404(ParallaxDos, pk=id_parallaxdos)
	if request.method == 'POST':
		form_pardos = ParallaxDosForm(request.POST, request.FILES, instance=a)
		if form_pardos.is_valid():
			form_pardos.save()
			return redirect('ver_parallaxdos')	
	else:
		form_pardos = ParallaxDosForm(instance=a)
	return render(request, 'adm_pagina/parallaxdos/nuevo.html', {'form_pardos':form_pardos})
def eliminar_parallaxdos(request, id_parallaxdos):
	a = get_object_or_404(ParallaxDos, pk=id_parallaxdos)
	if request.method == 'POST':
		a.delete()
		return redirect('ver_parallaxdos')
	return render(request, 'adm_pagina/parallaxdos/eliminar.html', {'a':a})
def index (request):
	slide = InicioSlide.objects.all()
	listaSlide=[]
	for a in slide:
		img_slide = str(a.img_slide)
		imgbtn_slide = str(a.imgbtn_slide)
		titulo_slide = a.titulo_slide
		subt_slide = a.subt_slide
		listaSlide.append({'slide':img_slide, 'btn_img':imgbtn_slide, 'titulo':titulo_slide,'subtitulo':subt_slide})

	parallax = Parallax.objects.all()
	listPar=[]
	for a in parallax:
		img_parallax = str(a.img_parallax)
		title_parallax = a.title_parallax
		desc_parallax = a.desc_parallax
		imginfe_parallax = str(a.imginfe_parallax)
		listPar.append({'imagen':img_parallax,'titulo':title_parallax,'descripcion':desc_parallax,'imagen_inferior':imginfe_parallax})

	parallaxuno = ParallaxUno.objects.all()
	listParuno=[]
	for a in parallaxuno:
		img_parallaxuno = str(a.img_parallaxuno)
		title_parallaxuno = a.title_parallaxuno
		subt_parallaxuno = a.subt_parallaxuno
		listParuno.append({'imagen':img_parallaxuno,'titulo':title_parallaxuno,'direccion':subt_parallaxuno})

	parallaxdos = ParallaxDos.objects.all()
	listPardos = []
	for a in parallaxdos:
		costo_parallaxdos = a.costo_parallaxdos
		dias_parallaxtres = a.dias_parallaxtres
		desc_parallaxdos = a.desc_parallaxdos
		listPardos.append({'valor':costo_parallaxdos,'dias':dias_parallaxtres,'descripcion':desc_parallaxdos,})

	producto = Producto.objects.all()
	listPro = []
	for a in producto:
		nombre_prod = a.nombre_prod
		img_prod = str(a.img_prod)
		precio_prod = a.precio_prod
		listPro.append({'nombre':nombre_prod,'imagen':img_prod,'precio':precio_prod})

	disciplina = Disciplina.objects.all()
	listDisc = []
	for a in disciplina:
		id_disciplina = a.id_disciplina
		nombre = a.nombre
		listDisc.append({'nombre':nombre})
	ctx = {'disciplina':listDisc}

	instructor = Instructor.objects.all()
	listInstructor = []
	for a in instructor:
		nombre = a.id.first_name
		apellido = a.id.last_name
		imagen = a.id.img_perfil
		listInstructor.append({'nombre':nombre, 'apellido':apellido,'imagen':imagen})
	ctx = {'instructor':listInstructor}

	destacado = DeportistaDestacado.objects.all()
	listdestacado = []
	for a in destacado:
		nombre = a.nombre
		apellidos = a.apellidos
		disciplina = a.disciplina
		nivel = a.nivel
		descripcion = a.descripcion
		imagen = str(a.imagen)
		listdestacado.append({'nombre':nombre, 'apellidos':apellidos,'imagen':imagen, 'descripcion':descripcion, 'disciplina':disciplina, 'nivel':nivel})
	ctx = {'destacado':listdestacado}


	ctx = {'slide':listaSlide, 'parallax':listPar, 'parallaxuno':listParuno,'parallaxdos':listPardos,'producto':listPro, 'disciplina':listDisc, 'instructor':listInstructor, 'destacado':listdestacado}
	return render(request, 'index.html', ctx)
# Acerca
@login_required
def ver_acerca(request):
	user = request.user
	if user.has_perm('can_add_acerca_de'):
		acerca = AcercaDe.objects.all()
		ctx = {'acerca':acerca}
		return render(request, 'adm_pagina/acerca/ver.html', ctx)
	else:
		return render(request, 'index.html', {})

def nuevo_acerca(request):
    if request.method == 'POST':
        form_acer = AcercaForm(request.POST or None, request.FILES or None)
        if form_acer.is_valid():
            acerca = form_acer.save()
            return redirect('ver_acerca')
    else:
        form_acer = AcercaForm()
    return render(request, 'adm_pagina/acerca/nuevo.html', {'form_acer': form_acer})

def editar_acerca(request, id_acerca):
	a = get_object_or_404(AcercaDe, pk=id_acerca)
	if request.method == 'POST':
		form_acer = AcercaForm(request.POST, request.FILES, instance=a)
		if form_acer.is_valid():
			form_acer.save()
			return redirect('ver_acerca')	
	else:
		form_acer = AcercaForm(instance=a)
	return render(request, 'adm_pagina/acerca/nuevo.html', {'form_acer':form_acer})
def eliminar_acerca(request, id_acerca):
	a = get_object_or_404(AcercaDe, pk=id_acerca)
	if request.method == 'POST':
		a.delete()
		return redirect('ver_acerca')
	return render(request, 'adm_pagina/acerca/eliminar.html', {'a':a})
def about (request):
	acerca = AcercaDe.objects.all()
	listAcer = []
	for a in acerca:
		titulo_acerca = a.titulo_acerca
		desc_acerca = a.desc_acerca
		sidea_acerca = a.sidea_acerca
		sidea1_acerca = a.sidea1_acerca
		sideb_acerca = a.sideb_acerca
		sideb1_acerca = a.sideb1_acerca
		img_uno = str(a.img_uno)
		img_dos = str(a.img_dos)
		listAcer.append({'titulo':titulo_acerca,'descripcion':desc_acerca,'mision':sidea_acerca,'vision':sidea1_acerca,'valores':sideb_acerca,'logros':sideb1_acerca,'imagenuno':img_uno,'imagendos':img_dos})
	ctx = {'acerca':listAcer}
	return render(request, 'about.html', ctx)
def schedule(request):
	horario = DisciplinaHorario.objects.all()
	listHorario = []
	for a in horario:
		id_instructor = a.id_instructor
		id_disciplina = a.id_disciplina
		id_horario = a.id_horario
		listHorario.append({'instructor':id_instructor, 'disciplina':id_disciplina, 'horario':id_horario})
	ctx = {'horario':listHorario}
	return render(request, 'schedule.html', ctx)
# Disciplinas
def classes (request):
	disciplina = Disciplina.objects.all()
	listDisc = []
	for a in disciplina:
		id_disciplina = a.id_disciplina
		nombre = a.nombre
		img_disciplina = str(a.img_disciplina)
		listDisc.append({'nombre':nombre,'imagen':img_disciplina, 'id_disciplina':id_disciplina})
	ctx = {'disciplina':listDisc}
	return render(request, 'classes.html', ctx)
def taekwondo(request):
	return render(request, 'taekwondo.html', {})
def read_disciplina(request):
	return render(request, 'read_disciplina.html', {})
def trainer(request):
	instructor = Instructor.objects.all()
	listInstructor = []
	for a in instructor:
		nombre = a.id.first_name
		apellido = a.id.last_name
		imagen = str(a.id.img_perfil)
		listInstructor.append({'nombre':nombre,'apellido':apellido,'imagen':imagen})
	ctx = {'instructor':listInstructor}
	return render(request, 'trainers.html', ctx)
# productos
def ver_producto(request):
	producto = Producto.objects.all()
	ctx = {'producto':producto}
	return render(request, 'adm_pagina/producto/ver.html', ctx)
def nuevo_producto(request):
    if request.method == 'POST':
        form_pro = ProductoForm(request.POST or None, request.FILES or None)
        if form_pro.is_valid():
            producto = form_pro.save()
            return redirect('ver_producto')
    else:
        form_pro = ProductoForm()
    return render(request, 'adm_pagina/producto/nuevo.html', {'form_pro': form_pro})
def editar_producto(request, id_producto):
	a = get_object_or_404(Producto, pk=id_producto)
	if request.method == 'POST':
		form_pro = ProductoForm(request.POST, request.FILES, instance=a)
		if form_pro.is_valid():
			form_pro.save()
			return redirect('ver_producto')	
	else:
		form_pro = ProductoForm(instance=a)
	return render(request, 'adm_pagina/producto/nuevo.html', {'form_pro':form_pro})
def eliminar_producto(request, id_producto):
	a = get_object_or_404(Producto, pk=id_producto)
	if request.method == 'POST':
		a.delete()
		return redirect('ver_producto')
	return render(request, 'adm_pagina/producto/eliminar.html', {'a':a})
def store(request):
	producto = Producto.objects.all()
	listPro = []
	for a in producto:
		nombre_prod = a.nombre_prod
		desc_prod = a.desc_prod
		img_prod = str(a.img_prod)
		precio_prod = a.precio_prod
		listPro.append({'nombre':nombre_prod,'descripcion':desc_prod,'imagen':img_prod,'precio':precio_prod})
	ctx = {'producto':listPro}
	return render(request, 'store.html', ctx)
# destacados
def ver_destacados(request):
	destacado = DeportistaDestacado.objects.all()
	ctx = {'destacado':destacado}
	return render(request, 'adm_pagina/destacados/ver.html', ctx)
def nuevo_destacado(request):
    if request.method == 'POST':
        form_destacado = DeportistaDestacadoForm(request.POST or None, request.FILES or None)
        if form_destacado.is_valid():
            destacado = form_destacado.save()
            return redirect('ver_destacados')
    else:
        form_destacado = DeportistaDestacadoForm()
    return render(request, 'adm_pagina/destacados/nuevo.html', {'form_destacado': form_destacado})
def editar_destacado(request, id):
	a = get_object_or_404(DeportistaDestacado, pk=id)
	if request.method == 'POST':
		form_destacado = DeportistaDestacadoEditForm(request.POST, request.FILES, instance=a)
		if form_destacado.is_valid():
			form_destacado.save()
			return redirect('ver_destacados')	
	else:
		form_destacado = DeportistaDestacadoEditForm(instance=a)
	return render(request, 'adm_pagina/destacados/editar.html', {'form_destacado':form_destacado})
# galeria
def ver_galeria(request):
	galeria = Galeria.objects.all()
	ctx = {'galeria':galeria}
	return render(request, 'adm_pagina/galeria/ver.html', ctx)
def nueva_galeria(request):
    if request.method == 'POST':
        form_gl = GaleriaForm(request.POST or None, request.FILES or None)
        if form_gl.is_valid():
            galeria = form_gl.save()
            return redirect('ver_galeria')
    else:
        form_gl = GaleriaForm()
    return render(request, 'adm_pagina/galeria/nuevo.html', {'form_gl': form_gl})
def editar_galeria(request, id_galeria):
	a = get_object_or_404(Galeria, pk=id_galeria)
	if request.method == 'POST':
		form_gl = GaleriaForm(request.POST, request.FILES, instance=a)
		if form_gl.is_valid():
			form_gl.save()
			return redirect('ver_galeria')	
	else:
		form_gl = GaleriaForm(instance=a)
	return render(request, 'adm_pagina/galeria/nuevo.html', {'form_gl':form_gl})
def eliminar_galeria(request, id_galeria):
	a = get_object_or_404(Galeria, pk=id_galeria)
	if request.method == 'POST':
		a.delete()
		return redirect('ver_galeria')
	return render(request, 'adm_pagina/galeria/eliminar.html', {'a':a})
def gallery(request):
	galeria = Galeria.objects.all()
	listGal = []
	for a in galeria:
		titulo_galeria = a.titulo_galeria
		img_galeria = str(a.img_galeria)
		desc_galeria = a.desc_galeria
		listGal.append({'titulo':titulo_galeria,'imagen':img_galeria,'descripcion':desc_galeria})
	ctx = {'galeria':listGal}
	return render(request, 'gallery.html', ctx)
# contactos
def ver_contacto(request):
	contacto = Contacto.objects.all()
	ctx = {'contacto':contacto}
	return render(request, 'adm_pagina/contacto/ver.html', ctx)
def nuevo_contacto(request):
    if request.method == 'POST':
        form_cont = ContactoForm(request.POST or None, request.FILES or None)
        if form_cont.is_valid():
            contacto = form_cont.save()
            return redirect('ver_contacto')
    else:
        form_cont = ContactoForm()
    return render(request, 'adm_pagina/contacto/nuevo.html', {'form_cont': form_cont})
def editar_contacto(request, id_contacto):
	a = get_object_or_404(Contacto, pk=id_contacto)
	if request.method == 'POST':
		form_cont = ContactoForm(request.POST, instance=a)
		if form_cont.is_valid():
			form_cont.save()
			return redirect('ver_contacto')	
	else:
		form_cont = ContactoForm(instance=a)
	return render(request, 'adm_pagina/contacto/nuevo.html', {'form_cont':form_cont})
def eliminar_contacto(request, id_contacto):
	a = get_object_or_404(Contacto, pk=id_contacto)
	if request.method == 'POST':
		a.delete()
		return redirect('ver_contacto')
	return render(request, 'adm_pagina/contacto/eliminar.html', {'a':a})
def location(request):
	contacto = Contacto.objects.all()
	listCont=[]
	for a in contacto:
		titulo_contac = a.titulo_contac
		desc_contact = a.desc_contact
		direccion_contact = a.direccion_contact
		phone_uno = a.phone_uno
		phone_dos = a.phone_dos
		listCont.append({'titulo':titulo_contac,'descripcion':desc_contact,'direccion':direccion_contact,'telefono':phone_uno,'celular':phone_dos})
	ctx = {'contacto':listCont}
	return render(request, 'location.html', ctx)
def sendEmail(request):
    if request.method == 'POST' :
    	nombre = request.POST['nombre']
    	telefono = request.POST['telefono']
    	email = request.POST['email']
    	mensaje = request.POST['mensaje']
    	subject, from_email, to = 'La siguiente persona desea contactarse con ud.', 'carlosjervesg@yahoo.com', 'carlosjervesg@yahoo.com'
        text_content = 'Contactarse'
        html_content = '<p><strong>La siguiente persona desea contactarse con gym deos.:</strong><br> Nombre: '+nombre+ '<br> Telefono: '+telefono+ '<br> Email: '+email+' <br> Mensaje: '+mensaje

        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
        msg.attach_alternative(html_content, "text/html")
        msg.send()

        return redirect('ubicacion')
	contact = Email.objects.all()
	ctx = {'contact':contact}
	return render(request, 'location.html', ctx)
# Deportista
@method_decorator(login_required, name='dispatch')
class ver_deportista(ListView):
    # queryset = Deportista.objects.order_by('first_name')
    model = Deportista
    template_name = 'adm_deportista/ver_deportista.html'
    @method_decorator(user_passes_test(lambda u: u.groups.filter(name='Usuarios') or u.groups.filter(name='Administrador') or u.groups.filter(name='SuperUsuarios')))
    def dispatch(self, *args, **kwargs):
        return super(ver_deportista, self).dispatch(*args, **kwargs)
def register_deportista(request):
	registrar = {'form_register':'form_register'}
	if request.method == 'POST':
		form_register = RegistrationForm(request.POST)
		if form_register.is_valid():
			users = form_register.save()
			print (users.pk)
			# Cambia de estado user a deportista
			AuthUser.objects.filter(pk=users.pk).update(es_deportista=True)
			forms = Deportista()
			# Guardar en tabla deportista
			forms.id=AuthUser.objects.get(pk=users.pk)
			forms.save()
			return redirect('login')
	else:
		form_register = RegistrationForm()
		registrar = {'form_register':'form_register'}
	return render(request, 'accounts/registerv1.html', registrar)
@login_required
@user_passes_test(lambda u: u.groups.filter(name='Usuarios').count() == 1 or u.groups.filter(name='Administrador').count() == 1 or u.groups.filter(name='SuperUsuarios').count() == 1)
def nuevo_deportista1(request):
	if request.method == 'POST':
		form_deportista1 = DeportistaForm(request.POST or None)
		if form_deportista1.is_valid():
			nuevo_deportista = form_deportista1.save(commit = False)
			nuevo_deportista.password = make_password(form_deportista1.cleaned_data['password'])
			nuevo_deportista.es_deportista = 1
			nuevo_deportista = form_deportista1.save()
			AuthUser.objects.filter(pk=nuevo_deportista.pk).update(es_deportista=True)
			forms = Deportista()
			forms.id=AuthUser.objects.get(pk=nuevo_deportista.pk)
			forms.save()
			return redirect('ver_deportista')
	else:
		form_deportista1 = DeportistaForm()
	return render(request, 'adm_deportista/nuevo_deportista1.html', {'form_deportista1':form_deportista1})
def editar_deportista1(request, id=None):
	auth = AuthUser.objects.get(id=id)
	form_deportista1 = DeportistaEditForm(request.POST or None, request.FILES or None, instance=auth)
	if request.method == 'POST':
		if form_deportista1.is_valid():
			users = form_deportista1.save()
			return redirect('ver_deportista')
	return render(request, 'adm_deportista/editar_deportista.html', {'form_deportista1':form_deportista1})
# class editar_deportista1(UpdateView):
# 	model = Deportista
# 	form_class = DeportistaEditForm
# 	template_name = 'adm_deportista/editar_deportista1.html'
# 	success_url = reverse_lazy('ver_deportista')
def eliminar_deportista1(request, id=None):
	a = AuthUser.objects.get(id=id)
	if request.method == 'POST':
		a.delete()
		return redirect('ver_deportista')
	return render(request, 'adm_deportista/eliminar_deportista.html', {'a':a})
def perfil_deportista(request, id=None):
	deportista = AuthUser.objects.filter(id=id)
	get_object_or_404(AuthUser, id=id)
	# deportista = MatriculaDisciplina.objects.filter(id=id)
	# get_object_or_404(MatriculaDisciplina, id=id)
	ctx ={'deportista':deportista,}
	return render(request, 'adm_deportista/perfil_deportista.html', ctx)
def error_404(request):
	return render(request, '404.html', {})
# RegistrarInstructor
	
# Administracion app
# def index_admin(user):
#     return user.groups.filter(name__in=['Administrador', 'Usuarios']).exists()

# @user_passes_test(lambda u: u.groups.filter(name='Usuarios').exists() or u.groups.filter(name='Administrador').exists() or u.groups.filter(name='SuperUsuarios').exists())
# def index_admin(request):
# 	try:
# 		return render(request, 'index_admin.html', {})
# 	except:
# 		return redirect('index')
# def index_admin(request):
#     """
#     Redirects users based on whether they are in the admins group
#     """
#     if request.user.groups.filter(name="Administrador").exists():
#         # user is an admin
#         return redirect("index_admin")
#     else:
#         return redirect("index")

def index_admin(request):
	if request.user.groups.filter(name='Usuarios').exists() or request.user.groups.filter(name='Administrador').exists() or request.user.groups.filter(name='SuperUsuarios').exists():
		return render(request, 'index_admin.html', {})
	else:
		return redirect('index')

# @user_passes_test(lambda u: u.groups.filter(name='Usuarios').exists() or u.groups.filter(name='Administrador').exists() or u.groups.filter(name='SuperUsuarios').exists())
# def index_admin(request):
# 	# users_in_group = Group.objects.get(name="Administrador").user_set.all()
# 	# if user in users_in_group:
# 	if request.users_in_group:
# 		return render(request, 'index_admin.html', {})
# 	else:
# 		return redirect('index')
# class ProfileView(LoginRequiredMixin, TemplateView):
#     template_name = 'index_admin.html'
#     def get_context_data(self, **kwargs):
#         self.request.session['Hi'] = True
#         context = super(ProfileView, self).get_context_data(**kwargs)
#         is_auth = False
#         name = None
#         # Check if in the request goes the user
#         user = self.request.user

#         # Check about of possible cases (For now is one profile)
#         if user.es_secretaria:
#         #if self.request.user.is_authenticated():
#             print (user.is_medical)
#         else:

# grupos
def ver_grupos(request):
	grupo = AuthUserGroups.objects.all()
	ctx = {'grupo':grupo}
	return render(request, 'grupos/ver_grupo.html', ctx)
def nuevo_grupo(request):
	if request.method == 'POST':
		form_group = AuthUserGroupssForm(request.POST or None)
		if form_group.is_valid():
			group = form_group.save()
			return redirect('ver_grupos')
	else:
		form_group = AuthUserGroupssForm()
	return render(request, 'grupos/nuevo_grupo.html', {'form_group':form_group})
def editar_grupo(request, id=None):
	g = AuthUserGroups.objects.get(id=id)
	form_group = AuthUserGroupssEditForm(request.POST or None, request.FILES or None, instance=g)
	if request.method == 'POST':
		if form_group.is_valid():
			group = form_group.save()
			return redirect('ver_grupos')	
	
	return render(request, 'grupos/editar_grupo.html', {'form_group':form_group})
def eliminar_grupo(request, id=None):
	a = AuthUserGroups.objects.get(id=id)
	if request.method == 'POST':
		a.delete()
		return redirect('ver_grupos')
	return render(request, 'grupos/eliminar_grupo.html', {'a':a})
# Usuario
@login_required
@user_passes_test(lambda u: u.groups.filter(name='Usuarios').count() == 1 or u.groups.filter(name='Administrador').count() == 1 or u.groups.filter(name='SuperUsuarios').count() == 1)
def ver_usuario(request):
	usuario = Secretaria.objects.all()
	ctx = {'usuario':usuario}
	return render(request, 'adm_usuario/ver_usuario.html', ctx)
@login_required
@user_passes_test(lambda u: u.groups.filter(name='Usuarios').count() == 1 or u.groups.filter(name='Administrador').count() == 1 or u.groups.filter(name='SuperUsuarios').count() == 1)
def nuevo_usuario(request):
    if request.method == 'POST':
	# POST, generar formulario vinculado con los datos del form
        form_user = UsuarioForm(request.POST or None, request.FILES or None)
        # verificar si es valido: 
        if form_user.is_valid():
        	 # Insertar en DB
        	nuevo_usuario = form_user.save(commit = False)
        	nuevo_usuario.password = make_password(form_user.cleaned_data['password'])
        	nuevo_usuario.es_secretaria = 1
        	nuevo_usuario = User.objects.get(id=1)
        	nuevo_usuario.username = 'Administrador'
        	nuevo_usuario = form_user.save()
        	nuevo_usuario.save()
        	AuthUser.objects.filter(pk=nuevo_usuario.pk).update(es_secretaria=True)
        	forms = Secretaria()
        	forms.id = AuthUser.objects.get(pk=nuevo_usuario.pk)
        	forms.save()
        	# my_group = Group.objects.get(name='Usuarios')
        	# my_group.user_set.add(nuevo_usuario)
        	return redirect('ver_usuario')
    else:
    	# GET, generar formulario (blank) 
        form_user = UsuarioForm()
    return render(request, 'adm_usuario/nuevo_usuario.html', {'form_user': form_user})
def newuser_prueba(request):
    if request.method == 'POST':
	# POST, generar formulario vinculado con los datos del form
        form_user = AuthUserGroupsNewForm(request.POST or None, request.FILES or None)
        # verificar si es valido: 
        if form_user.is_valid():
        	 # Insertar en DB
        	nuevo_usuario = form_user.save(commit = False)
        	nuevo_usuario.password = make_password(form_user.cleaned_data['password'])
        	nuevo_usuario.es_secretaria = 1
        	nuevo_usuario = form_user.save()
        	nuevo_usuario.save()
        	AuthUser.objects.filter(pk=nuevo_usuario.pk).update(es_secretaria=True)
        	forms = Secretaria()
        	forms.id = AuthUser.objects.get(pk=nuevo_usuario.pk)
        	forms.save()
        	# my_group = Group.objects.get(name='Usuarios')
        	# my_group.user_set.add(nuevo_usuario)
        	return redirect('ver_usuario')
    else:
    	# GET, generar formulario (blank) 
        form_user = AuthUserGroupsNewForm()
    return render(request, 'adm_usuario/prueba_user.html', {'form_user': form_user})
	# @method_decorator(permission_required('personas.add_persona',reverse_lazy('personas:personas')))
	#     def dispatch(self, *args, **kwargs):
	#             return super(CrearPersona, self).dispatch(*args, **kwargs)
# class update_usuario(UpdateView):
# 	model = Secretaria
# 	second_model = AuthUser
# 	form_class = UsuarioForm
# 	template_name = 'adm_usuario/nuevo_usuario.html'

# 	def get_context_data(self, **kwargs):
# 		context = super(update_usuario, self).get_context_data(**kwargs)
# 		pk = self.kwargs.get('pk', 0)
# 		usuario = self.model.objects.get(id=pk)
# 		secretaria = self.second_model.objects.get(id=usuario.id)
# 		if 'form' not in context:
# 			context['form'] = self.form_class(self.request.GET, instance=secretaria)
# 		context['id'] = pk
#  		return context
@login_required
@user_passes_test(lambda u: u.groups.filter(name='Usuarios').count() == 1 or u.groups.filter(name='Administrador').count() == 1 or u.groups.filter(name='SuperUsuarios').count() == 1)	
def editar_usuario(request, id=None):
	auth = AuthUser.objects.get(id=id)
	form_user = UsuarioEditForm(request.POST or None, request.FILES or None, instance=auth)
	if request.method == 'POST':
		if form_user.is_valid():
			users = form_user.save()
			return redirect('ver_usuario')	
	
	return render(request, 'adm_usuario/editar_usuario.html', {'form_user':form_user})
@login_required
@user_passes_test(lambda u: u.groups.filter(name='Usuarios').count() == 1 or u.groups.filter(name='Administrador').count() == 1 or u.groups.filter(name='SuperUsuarios').count() == 1)
def eliminar_usuario(request, id=None):
	a = AuthUser.objects.get(id=id)
	if request.method == 'POST':
		a.delete()
		return redirect('ver_usuario')
	return render(request, 'adm_usuario/eliminar_usuario.html', {'a':a})
@login_required
@user_passes_test(lambda u: u.groups.filter(name='Usuarios').count() == 1 or u.groups.filter(name='Administrador').count() == 1 or u.groups.filter(name='SuperUsuarios').count() == 1)
def ver_perfil(request, id=None):
	usuario = AuthUser.objects.filter(pk=id)
	get_object_or_404(AuthUser, pk=id)
	ctx ={'usuario':usuario}
	return render(request, 'adm_usuario/ver_perfil.html', ctx)
# Instructor
@method_decorator(login_required, name='dispatch')
class ver_instructor(ListView):
    model = Instructor
    template_name = 'adm_instructor/ver_instructor.html'
    @method_decorator(user_passes_test(lambda u: u.groups.filter(name='Administrador') or u.groups.filter(name='SuperUsuarios')))
    def dispatch(self, *args, **kwargs):
        return super(ver_instructor, self).dispatch(*args, **kwargs)
@login_required
@user_passes_test(lambda u: u.groups.filter(name='Administrador').count() == 1 or u.groups.filter(name='SuperUsuarios').count() == 1)
def nuevo_instructor(request):
	if request.method == 'POST':
		# form_instructor = InstructorForm(request.POST)
		form_instructor = InstructorrForm(request.POST)
		if form_instructor.is_valid() :
			nuevo_instructor = form_instructor.save(commit = False)
			nuevo_instructor.password = make_password(form_instructor.cleaned_data['password'])
			nuevo_instructor.es_instructor = 1
			nuevo_instructor = form_instructor.save() 
			nuevo_instructor.save()
			AuthUser.objects.filter(pk=nuevo_instructor.pk).update(es_instructor=True)
			forms = Instructor()
			# Guardar en tabla instructor
			forms.id=AuthUser.objects.get(pk=nuevo_instructor.pk)
			forms.save()
			return redirect('ver_instructor')
	else:
		form_instructor = InstructorrForm()
		# form_usuario = InstructorForm()
	return render(request, 'adm_instructor/nuevo_instructor.html', {'form_instructor':form_instructor})
@login_required
@user_passes_test(lambda u: u.groups.filter(name='Administrador').count() == 1 or u.groups.filter(name='SuperUsuarios').count() == 1)
def editar_instructor(request, id=None):
	inst = AuthUser.objects.get(id=id)
	# form_instructor = InstructorForm(request.POST or None, request.FILES or None, instance=inst)
	form_usuario = InstructorEditForm(request.POST or None, request.FILES or None, instance=inst)
	if request.method == 'POST':
		# if form_instructor.is_valid() and form_usuario.is_valid():
		if form_usuario.is_valid():
			# instructor = form_instructor.save()
			instructor = form_usuario.save()
			# instructor.save()
			return redirect('ver_instructor')	
	
	return render(request, 'adm_instructor/editar_instructor.html', {'form_usuario':form_usuario})
@login_required
@user_passes_test(lambda u: u.groups.filter(name='Administrador').count() == 1 or u.groups.filter(name='SuperUsuarios').count() == 1)
def eliminar_instructor(request, id=None):
	a = Instructor.objects.get(id=id)
	if request.method == 'POST':
		a.delete()
		return redirect('ver_instructor')
	return render(request, 'adm_instructor/eliminar_instructor.html', {'a':a})
@login_required
@user_passes_test(lambda u: u.groups.filter(name='Administrador').count() == 1 or u.groups.filter(name='SuperUsuarios').count() == 1)
def perfil_instructor(request, id=None):
	instructor = AuthUser.objects.filter(pk=id)
	get_object_or_404(AuthUser, pk=id)
	ctx ={'instructor':instructor}
	return render(request, 'adm_instructor/perfil_instructor.html', ctx)
# Disciplina 
@login_required
@user_passes_test(lambda u: u.groups.filter(name='Administrador').count() == 1 or u.groups.filter(name='SuperUsuarios').count() == 1)
def ver_disciplina(request):
	disciplina = Disciplina.objects.all()
	ctx = {'disciplina':disciplina}
	return render(request, 'adm_disciplina/ver_disciplina.html', ctx)
@login_required
@user_passes_test(lambda u: u.groups.filter(name='Administrador').count() == 1 or u.groups.filter(name='SuperUsuarios').count() == 1)
def nueva_disciplina(request):
    if request.method == 'POST':
        form_disciplina = DisciplinaForm(request.POST or None, request.FILES or None)
        if form_disciplina.is_valid():
            disciplina = form_disciplina.save()
            return redirect('ver_disciplina')
    else:
        form_disciplina = DisciplinaForm()
    return render(request, 'adm_disciplina/nueva_disciplina.html', {'form_disciplina': form_disciplina})
@login_required
@user_passes_test(lambda u: u.groups.filter(name='Administrador').count() == 1 or u.groups.filter(name='SuperUsuarios').count() == 1)
def editar_disciplina(request, id_disciplina):
	a = get_object_or_404(Disciplina,pk=id_disciplina)
	if request.method == 'POST':
		form_disciplina = DisciplinaForm(request.POST or None, request.FILES or None, instance=a)
		if form_disciplina.is_valid():
			form_disciplina.save()
			return redirect('ver_disciplina')	
	else:
		form_disciplina = DisciplinaForm(instance=a)
	return render(request, 'adm_disciplina/editar_disciplina.html', {'form_disciplina':form_disciplina})
@login_required
@user_passes_test(lambda u: u.groups.filter(name='Administrador').count() == 1 or u.groups.filter(name='SuperUsuarios').count() == 1)
def eliminar_disciplina(request, id):
	a = get_object_or_404(Disciplina, pk=id)
	if request.method == 'POST':
		a.delete()
		return redirect('ver_disciplina')
	return render(request, 'adm_disciplina/eliminar_disciplina.html', {'a':a})

# horario
@method_decorator(login_required, name='dispatch')
class ver_horario(ListView):
    model = Horario
    template_name = 'adm_horario/ver_horario.html'
    @method_decorator(user_passes_test(lambda u: u.groups.filter(name='Administrador') or u.groups.filter(name='SuperUsuarios')))
    def dispatch(self, *args, **kwargs):
        return super(ver_horario, self).dispatch(*args, **kwargs)
@login_required
@user_passes_test(lambda u: u.groups.filter(name='Administrador').count() == 1 or u.groups.filter(name='SuperUsuarios').count() == 1)
def nuevo_horario(request):
    if request.method == 'POST':
        form_horario = HorarioForm(request.POST)
        if form_horario.is_valid():
            horario = form_horario.save()
            return redirect('ver_horario')
    else:
        form_horario = HorarioForm()
    return render(request, 'adm_horario/nuevo_horario.html', {'form_horario': form_horario})
@login_required
@user_passes_test(lambda u: u.groups.filter(name='Administrador').count() == 1 or u.groups.filter(name='SuperUsuarios').count() == 1)
def editar_horario(request, id_horario):
	a = get_object_or_404(Horario,pk=id_horario)
	if request.method == 'POST':
		form_horario = HorarioForm(request.POST, instance=a)
		if form_horario.is_valid():
			form_horario.save()
			return redirect('ver_horario')	
	else:
		form_horario = HorarioForm(instance=a)
	return render(request, 'adm_horario/editar_horario.html', {'form_horario':form_horario})
@login_required
@user_passes_test(lambda u: u.groups.filter(name='Administrador').count() == 1 or u.groups.filter(name='SuperUsuarios').count() == 1)
def eliminar_horario(request, id):
	a = get_object_or_404(Horario, pk=id)
	if request.method == 'POST':
		a.delete()
		return redirect('ver_horario')
	return render(request, 'adm_horario/eliminar_horario.html', {'a':a})
# Matricula
# @user_passes_test(lambda u: u.groups.filter(name='Usuarios').exists() or u.groups.filter(name='Administrador').exists() or u.groups.filter(name='SuperUsuarios').exists())
@login_required
@user_passes_test(lambda u: u.groups.filter(name='Usuarios').count() == 1 or u.groups.filter(name='Administrador').count() == 1 or u.groups.filter(name='SuperUsuarios').count() == 1)
def ver_matricula(request):
	matricula = MatriculaDisciplina.objects.all()
	ctx = {'matricula':matricula}
	return render(request, 'adm_matricula/ver_matricula.html', ctx)
@login_required
# @user_passes_test(lambda u: u.groups.filter(name='Administrador').count() == 1)
def nueva_matricula(request):
	# deportista=Deportista.objects.all()
	# disciplinahorario=DisciplinaHorario.objects.all()	
	if request.method == 'POST':
		form_matricula = PreinscripcionAdminForm(request.POST or None, request.FILES or None)
		if form_matricula.is_valid():			
			costo = 0
			contador = 1
			for c in request.POST.getlist('id_disciplina_horario'):
				costo = contador * 5
				form_matricula = Matricula()
				form_matricula.id_deportista = Deportista.objects.get(id_deportista = request.POST.get("id_deportista"))
				form_matricula.estado = False
				form_matricula.costo = costo
				form_matricula.save()
				contador = contador + 1
			
			for c in request.POST.getlist('id_disciplina_horario'):
				form_MatDis = MatriculaDisciplina()
				ide_matricula = Matricula.objects.latest('id_matricula')
				form_MatDis.id_matricula = ide_matricula
				Matricula.objects.filter(id_matricula = ide_matricula.id_matricula).update(estado=True)
				form_MatDis.id_disciplina_horario = DisciplinaHorario.objects.get(id_disciplina_horario = c)
				form_MatDis.save()
				form_mensusalidad = Mensualidad()
				form_mensusalidad.id_matriculadisciplina = MatriculaDisciplina.objects.latest("id_matriculadisciplina")
				form_mensusalidad.fecha_mensualidad = datetime.now()
				form_mensusalidad.fecha_pago = datetime.now()
				form_mensusalidad.save()
				
				horario = DisciplinaHorario.objects.get(id_disciplina_horario = c).id_horario.pk
				cupos_inicio = int(Horario.objects.get(id_horario = horario).cupos)
				Horario.objects.filter(id_horario=horario).update(cupos = ( cupos_inicio - 1 ))					
			return redirect('ver_matricula')
		
	else:
		form_matricula = PreinscripcionAdminForm()
	return render(request, 'adm_matricula/nueva_matricula.html', {'form_matricula':form_matricula})
# @login_required
# @user_passes_test(lambda u: u.groups.filter(name='SuperUsuarios' or 'Administrador').count() == 1)
def validar_matricula(request, id_matricula=None):
	a = get_object_or_404(Matricula, pk=id_matricula)
	if request.method == 'POST':
		form_matriculav = MatriculaForm(request.POST or None, instance=a)
		if form_matriculav.is_valid():
			form_matriculav.save()
			md = MatriculaDisciplina.objects.filter(id_matricula = id_matricula)
			for c in md:
				form_mensusalidad = Mensualidad()
				mens = datetime.now()
				form_mensusalidad.fecha_mensualidad = datetime(mens.year,mens.month,mens.day)
				form_mensusalidad.fecha_pago = mens
				form_mensusalidad.id_matriculadisciplina = MatriculaDisciplina.objects.get(id_matriculadisciplina = c.id_matriculadisciplina)
				print(request.POST.get("estado"))
				if (Mensualidad.objects.filter(id_matriculadisciplina = c.id_matriculadisciplina) and request.POST.get("estado") is None):
					Mensualidad.objects.filter(id_matriculadisciplina = c.id_matriculadisciplina).delete()
				else:
					form_mensusalidad.save()
			return redirect('ver_matricula')	
	else:
		form_matriculav = MatriculaForm(instance=a)
	return render(request, 'adm_matricula/validar_matricula.html', {'form_matriculav':form_matriculav})
def editar_matricula(request, id_matricula=None):
	a = get_object_or_404(Matricula, pk=id_matricula)
	if request.method == 'POST':
		form_matricula = PreinscripcionAdminEditForm(request.POST or None, instance=a)
		if form_matricula.is_valid():
			form_matricula.save()
			return redirect('ver_matricula')	
	else:
		form_matricula = PreinscripcionAdminEditForm(instance=a)
	return render(request, 'adm_matricula/editar_matricula.html', {'form_matricula':form_matricula})

def eliminar_matricula(request, id_matricula):
	a = get_object_or_404(Matricula, pk=id_matricula)
	if request.method == 'POST':
		a.delete()
		return redirect('ver_matricula')
	return render(request, 'adm_matricula/eliminar_matricula.html', {'a':a})
def info_matricula(request, id=	None):
	matricula = Matricula.objects.filter(pk=id)
	get_object_or_404(Matricula, pk=id)
	ctx ={'matricula':matricula}
	return render(request, 'adm_matricula/info_matricula.html', ctx)
def pre_matricula(request):
	form_deportistapre = PreinscripcionForm()
	costo = 0
	user = ""
	deportista_id = ""
	if request.method == 'POST':
		if request.user.is_authenticated():
			user = request.user
			deportista_id = Deportista.objects.get(id = request.user.id)
		else:		
			try:
				user = User.objects.get(username = request.POST.get('username'))
				mensaje = "El usuario <strong>"+"</strong> que esta intentanto crear ya existe"
				return render(request, 'adm_preinscripciones/preinscripcion.html', {'form_deportistapre':form_deportistapre,'mensaje':mensaje})
			except User.DoesNotExist:
				user = User.objects.create_user(password=request.POST.get('pass'),
	                                            username=request.POST.get('username'),
	                                            first_name=request.POST.get('nombres'),
	                                            last_name=request.POST.get('apellidos'),
	                                            email=request.POST.get('email'),
	                                            is_active=True)
				user = authenticate(username=request.POST.get('username'), password=request.POST.get('pass'))
				ide = AuthUser.objects.latest('id')
				AuthUser.objects.filter(id = ide.id).update(dni= request.POST.get('cedula'),direccion = request.POST.get('direccion'),telefono = request.POST.get('telefono'),celular = request.POST.get('celular'),edad = request.POST.get('edad'),genero = request.POST.get('genero'))				
				if user is not None:
					if user.is_active:
						login(request, user)
						# return redirect('')
					else:
						print ("Error")

				form_deportista = Deportista()
				form_deportista.id = AuthUser.objects.get(id = request.user.id)
				form_deportista.save()
				deportista_id = Deportista.objects.latest('id_deportista') 
		# form_deportistapre = DeportistaPreinscripcionForm(request.POST or None, request.FILES or None, instance=usuario)
		contador = 1
		for c in request.POST.getlist('id_disciplina_horario'):
			costo = contador * 5
			contador = contador + 1
		form_matricula = Matricula()
		form_matricula.id_deportista = deportista_id
		form_matricula.estado = False
		form_matricula.costo = costo
		form_matricula.save()		
		
		
		for c in request.POST.getlist('id_disciplina_horario'):
			form_MatDis = MatriculaDisciplina()
			form_MatDis.id_matricula = Matricula.objects.latest('id_matricula')
			form_MatDis.id_disciplina_horario = DisciplinaHorario.objects.get(id_disciplina_horario = c)
			form_MatDis.save()
			
			horario = DisciplinaHorario.objects.get(id_disciplina_horario = c).id_horario.pk
			cupos_inicio = int(Horario.objects.get(id_horario = horario).cupos)
			Horario.objects.filter(id_horario=horario).update(cupos = ( cupos_inicio - 1 ))
		return redirect('preinscripcion_realizada')		
	else:
		form_deportistapre = PreinscripcionForm()
	return render(request, 'adm_preinscripciones/preinscripcion.html', {'form_deportistapre':form_deportistapre})

def search_deportista(request):
	if request.is_ajax:
		search=request.GET.get('start', '')
		user=Gimnasio.objects.filter(nombre_gym__icontains=search)
		results=[]
		for user in deportistas:
			user_json={}
			user_json['label']=user.nombre_gym
			user_json['value']=user.nombre_gym
			results.append(user_json)
		data_json=json.dumps(results)
	else:
		data_json='fail'
	mimetype="application/json"
	return HttpResponse(data_json,mimetype)

# discipina_horario
@login_required
@user_passes_test(lambda u: u.groups.filter(name='Administrador').count() == 1 or u.groups.filter(name='SuperUsuarios').count() == 1)
def ver_disciplinahorario(request):
    dis = Disciplina.objects.all()
    # object_list = DisciplinaHorario.objects.all()
    dishor = DisciplinaHorario.objects.all()
    object_list = []
    for c in dishor:
    	id_disciplina_horario = c.id_disciplina_horario
        id_disciplina = c.id_disciplina
        id_instructor = c.id_instructor
        id_horario = c.id_horario
    	horario = Horario.objects.filter(~Q(disciplinahorario__id_instructor=c.id_instructor))
    	object_list.append({"id_disciplina_horario":id_disciplina_horario,"id_disciplina":id_disciplina,"id_instructor":id_instructor,"id_horario":id_horario,"horario":horario})
    if request.method == 'POST' and 'actualizar' in request.POST:    	
    	id_disciplina = Disciplina.objects.get(id_disciplina = request.POST.get('id_disciplina'))
    	id_horario = Horario.objects.get(id_horario = request.POST.get('id_horario'))
    	DisciplinaHorario.objects.filter(id_disciplina_horario = request.POST.get("id_disciplina_horario")).update(id_disciplina = id_disciplina,id_horario=id_horario)
    ctx = {"object_list":object_list, "dis":dis}    
    return render(request, 'adm_disciplinahorario/ver_disciplinahorario.html', ctx)
@login_required
@user_passes_test(lambda u: u.groups.filter(name='Administrador').count() == 1 or u.groups.filter(name='SuperUsuarios').count() == 1)
def nueva_disciplinahorario(request):
    instructor = Instructor.objects.all()
    bandera = False
    ctx = {'instructor': instructor, 'bandera':bandera}
    if request.method == 'POST' and 'instructor' in request.POST:
        dis = Disciplina.objects.all()
        horario = Horario.objects.filter(~Q(disciplinahorario__id_instructor = request.POST.get('instructor')))
        bandera = True
        valor = Instructor.objects.get(id_instructor = int(request.POST.get('instructor')))
        ctx = {'instructor': instructor, 'dis':dis,'horario':horario, 'bandera': bandera, 'valor':valor}
    if request.method == 'POST' and 'guardar' in request.POST:
    	formDH = DisciplinaHorario()
    	formDH.id_disciplina = Disciplina.objects.get(id_disciplina = request.POST.get('id_disciplina'))
    	formDH.id_horario = Horario.objects.get(id_horario = request.POST.get('id_horario'))
    	formDH.id_instructor = Instructor.objects.get(id_instructor = request.POST.get('id_instructor'))
    	formDH.save()
    	return redirect('ver_disciplinahorario')
    return render(request, 'adm_disciplinahorario/nueva_disciplinahorario.html', ctx)
@login_required
@user_passes_test(lambda u: u.groups.filter(name='Administrador').count() == 1 or u.groups.filter(name='SuperUsuarios').count() == 1)
def editar_disciplinahorario(request, id_disciplinahorario):
	a = get_object_or_404(DisciplinaHorario,pk=id_disciplinahorario)
	if request.method == 'POST':
		form_disciplinah = DisciplinaHorarioForm(request.POST or None, request.FILES or None, instance=a)
		if form_disciplinah.is_valid():
			form_disciplinah.save()
			return redirect('ver_disciplinahorario')	
	else:
		form_disciplinah = DisciplinaHorarioForm(instance=a,initial={'id_instructor': id_disciplinahorario})
	return render(request, 'adm_disciplinahorario/nueva_disciplinahorario.html', {'form_disciplinah':form_disciplinah})
@login_required
@user_passes_test(lambda u: u.groups.filter(name='Administrador').count() == 1 or u.groups.filter(name='SuperUsuarios').count() == 1)
def eliminar_disciplinah(request, id):
	a = get_object_or_404(DisciplinaHorario, pk=id)
	if request.method == 'POST':
		a.delete()
		return redirect('ver_disciplinahorario')
	return render(request, 'adm_disciplinahorario/eliminar_disciplinahorario.html', {'a':a})
# Mensualidades}
def preinscripcion_realizada(request):
	return render(request, 'adm_preinscripciones/preinscripcion_realizada.html', {})
class ver_mensualidad(ListView):
	model = Mensualidad
	query = Mensualidad.objects.all()
	template_name = 'adm_mensualidad/ver_mensualidad.html'
def nueva_mensualidad(request):
	if request.method == 'POST':
		form_mensualidad = MensualidadForm(request.POST or None, request.FILES)
		if form_mensualidad.is_valid():
			mensualidad = form_mensualidad.save()
			mens = Mensualidad.objects.latest("id_mensualidad").fecha_mensualidad
			idmatdis = Mensualidad.objects.latest("id_mensualidad").id_matriculadisciplina.pk
			form_mensusalidad = Mensualidad()
			form_mensusalidad.fecha_mensualidad = datetime(mens.year,mens.month + 1,mens.day)
			# form_mensusalidad.fecha_mensualidad = mens + timedelta(month=1)
			form_mensusalidad.fecha_pago = datetime.now()
			form_mensusalidad.id_matriculadisciplina = MatriculaDisciplina.objects.get(id_matriculadisciplina = idmatdis)
			form_mensusalidad.pagado = False
			form_mensusalidad.save()
			return redirect('ver_mensualidad')
	else:
		form_mensualidad = MensualidadForm()
	return render(request, 'adm_mensualidad/nueva_mensualidad.html', {'form_mensualidad':form_mensualidad})
def editar_mensualidad(request, id_mensualidad=None):
	a = get_object_or_404(Mensualidad, pk=id_mensualidad)
	if request.method == 'POST':
		form_mensualidad = MensualidadForm(request.POST or None, instance=a)
		if form_mensualidad.is_valid():
			form_mensualidad.save()
			mensualidad = form_mensualidad.save()
			mens = Mensualidad.objects.latest("id_mensualidad").fecha_mensualidad

			idmatdis = Mensualidad.objects.latest("id_mensualidad").id_matriculadisciplina.pk
			form_mensusalidad = Mensualidad()			
			form_mensusalidad.fecha_mensualidad = datetime(mens.year,mens.month + 1,mens.day)
			form_mensusalidad.fecha_pago = datetime.now()
			form_mensusalidad.id_matriculadisciplina = MatriculaDisciplina.objects.get(id_matriculadisciplina = idmatdis)
			# form_mensusalidad.pagado = False
			form_mensusalidad.save()
			return redirect('ver_mensualidad')	
	else:
		form_mensualidad = MensualidadForm(instance=a)	
	return render(request, 'adm_mensualidad/nueva_mensualidad.html', {'form_mensualidad':form_mensualidad})
def eliminar_mensualidad(request, id_mensualidad):
	a = get_object_or_404(Mensualidad, pk=id_mensualidad)
	if request.method == 'POST':
		a.delete()
		return redirect('ver_mensualidad')
	return render(request, 'adm_mensualidad/eliminar_mensualidad.html', {'a':a})
# filtros
def buscar_usuario(request):
    discipina_horario = DisciplinaHorario.objects.all()
    matricula = ""
    mat = ""
    contador = 0
    horario = 0
    if request.method == 'POST':
    	horario = int((request.POST.get('horario')))    	
    	matricula = MatriculaDisciplina.objects.filter(id_disciplina_horario = horario)
    	contador = MatriculaDisciplina.objects.filter(id_disciplina_horario = horario).count()
    	mat = DisciplinaHorario.objects.get(id_disciplina_horario = horario)
    ctx = {'discipina_horario':discipina_horario,'matricula':matricula,'contador':contador,'mat':mat,'horario':horario}
    return render(request, 'adm_matricula/buscar.html', ctx)

def buscar_deportista(request):
    id_deportista = request.GET['id']
    deportista = AuthUser.objects.filter(dni__contains=id_deportista)
    data = serializers.serialize(
        'json', deportista, fields=('dni', 'first_name', 'last_name', 'telefono'))
    return HttpResponse(data, content_type='application/json')


