--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.3
-- Dumped by pg_dump version 9.5.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: acerca_de; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE acerca_de (
    id_acerca integer NOT NULL,
    id_gimnasio integer NOT NULL,
    titulo_acerca character varying(50) NOT NULL,
    desc_acerca character varying(10000) NOT NULL,
    sidea_acerca character varying(300) NOT NULL,
    sidea1_acerca character varying(300) NOT NULL,
    sideb_acerca character varying(300) NOT NULL,
    sideb1_acerca character varying(300) NOT NULL,
    img_uno character varying(150),
    img_dos character varying(150)
);


ALTER TABLE acerca_de OWNER TO postgres;

--
-- Name: acerca_de_id_acerca_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE acerca_de_id_acerca_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE acerca_de_id_acerca_seq OWNER TO postgres;

--
-- Name: acerca_de_id_acerca_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE acerca_de_id_acerca_seq OWNED BY acerca_de.id_acerca;


--
-- Name: administrador; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE administrador (
    id_administrador integer NOT NULL,
    id integer NOT NULL
);


ALTER TABLE administrador OWNER TO postgres;

--
-- Name: administrador_id_administrador_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE administrador_id_administrador_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE administrador_id_administrador_seq OWNER TO postgres;

--
-- Name: administrador_id_administrador_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE administrador_id_administrador_seq OWNED BY administrador.id_administrador;


--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE auth_group OWNER TO postgres;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_group_id_seq OWNER TO postgres;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_group_id_seq OWNED BY auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE auth_group_permissions OWNER TO postgres;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_group_permissions_id_seq OWNER TO postgres;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_group_permissions_id_seq OWNED BY auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE auth_permission OWNER TO postgres;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_permission_id_seq OWNER TO postgres;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_permission_id_seq OWNED BY auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL,
    dni character(10),
    genero character varying(20),
    celular character(10),
    img_perfil character varying(8000),
    edad character(2),
    telefono character(10),
    es_administrador boolean,
    es_deportista boolean,
    es_instructor boolean,
    es_secretaria boolean,
    direccion character varying(300)
);


ALTER TABLE auth_user OWNER TO postgres;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE auth_user_groups OWNER TO postgres;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_groups_id_seq OWNER TO postgres;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_user_groups_id_seq OWNED BY auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_id_seq OWNER TO postgres;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_user_id_seq OWNED BY auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE auth_user_user_permissions OWNER TO postgres;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_user_permissions_id_seq OWNER TO postgres;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_user_user_permissions_id_seq OWNED BY auth_user_user_permissions.id;


--
-- Name: contacto; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE contacto (
    id_contacto integer NOT NULL,
    id_gimnasio integer NOT NULL,
    titulo_contac character varying(50) NOT NULL,
    desc_contact character varying(80000) NOT NULL,
    direccion_contact character varying(200) NOT NULL,
    phone_uno integer NOT NULL,
    phone_dos integer NOT NULL,
    phone_tres integer
);


ALTER TABLE contacto OWNER TO postgres;

--
-- Name: contacto_id_contacto_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE contacto_id_contacto_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE contacto_id_contacto_seq OWNER TO postgres;

--
-- Name: contacto_id_contacto_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE contacto_id_contacto_seq OWNED BY contacto.id_contacto;


--
-- Name: deportista; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE deportista (
    id_deportista integer NOT NULL,
    id integer NOT NULL
);


ALTER TABLE deportista OWNER TO postgres;

--
-- Name: deportista_id_deportista_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE deportista_id_deportista_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE deportista_id_deportista_seq OWNER TO postgres;

--
-- Name: deportista_id_deportista_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE deportista_id_deportista_seq OWNED BY deportista.id_deportista;


--
-- Name: disciplina; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE disciplina (
    id_disciplina integer NOT NULL,
    nombre character varying(50) NOT NULL,
    img_disciplina character varying(150) NOT NULL
);


ALTER TABLE disciplina OWNER TO postgres;

--
-- Name: disciplina_id_disciplina_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE disciplina_id_disciplina_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE disciplina_id_disciplina_seq OWNER TO postgres;

--
-- Name: disciplina_id_disciplina_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE disciplina_id_disciplina_seq OWNED BY disciplina.id_disciplina;


--
-- Name: disciplina_instructor; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE disciplina_instructor (
    id_disciplina_horario integer NOT NULL,
    id_disciplina integer NOT NULL,
    id_horario integer NOT NULL,
    id_instructor integer NOT NULL
);


ALTER TABLE disciplina_instructor OWNER TO postgres;

--
-- Name: disciplina_instructor_id_disciplina_horario_seq_1; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE disciplina_instructor_id_disciplina_horario_seq_1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE disciplina_instructor_id_disciplina_horario_seq_1 OWNER TO postgres;

--
-- Name: disciplina_instructor_id_disciplina_horario_seq_1; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE disciplina_instructor_id_disciplina_horario_seq_1 OWNED BY disciplina_instructor.id_disciplina_horario;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE django_admin_log OWNER TO postgres;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_admin_log_id_seq OWNER TO postgres;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE django_admin_log_id_seq OWNED BY django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE django_content_type OWNER TO postgres;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_content_type_id_seq OWNER TO postgres;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE django_content_type_id_seq OWNED BY django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE django_migrations OWNER TO postgres;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_migrations_id_seq OWNER TO postgres;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE django_migrations_id_seq OWNED BY django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE django_session OWNER TO postgres;

--
-- Name: email; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE email (
    id_email integer NOT NULL,
    nombre character varying(50) NOT NULL,
    direccion character varying(100),
    email character varying(80) NOT NULL,
    telefono character(10),
    mensaje character varying(100) NOT NULL
);


ALTER TABLE email OWNER TO postgres;

--
-- Name: email_id_email_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE email_id_email_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE email_id_email_seq OWNER TO postgres;

--
-- Name: email_id_email_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE email_id_email_seq OWNED BY email.id_email;


--
-- Name: galeria; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE galeria (
    id_galeria integer NOT NULL,
    id_gimnasio integer NOT NULL,
    titulo_galeria character varying(50) NOT NULL,
    img_galeria character varying(500) NOT NULL,
    desc_galeria character varying(500) NOT NULL
);


ALTER TABLE galeria OWNER TO postgres;

--
-- Name: galeria_id_galeria_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE galeria_id_galeria_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE galeria_id_galeria_seq OWNER TO postgres;

--
-- Name: galeria_id_galeria_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE galeria_id_galeria_seq OWNED BY galeria.id_galeria;


--
-- Name: gimnasio; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE gimnasio (
    id_gimnasio integer NOT NULL,
    nombre_gym character varying(100) NOT NULL
);


ALTER TABLE gimnasio OWNER TO postgres;

--
-- Name: gimnasio_id_gimnasio_seq_5; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE gimnasio_id_gimnasio_seq_5
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE gimnasio_id_gimnasio_seq_5 OWNER TO postgres;

--
-- Name: gimnasio_id_gimnasio_seq_5; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE gimnasio_id_gimnasio_seq_5 OWNED BY gimnasio.id_gimnasio;


--
-- Name: horario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE horario (
    id_horario integer NOT NULL,
    hora_inicio time without time zone NOT NULL,
    hora_fin time without time zone NOT NULL,
    ubicacion character varying(20),
    sesion character varying(10) NOT NULL,
    desc_horario character varying(500),
    categoria character varying(20),
    cupos integer
);


ALTER TABLE horario OWNER TO postgres;

--
-- Name: horario_id_horario_seq_1_1; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE horario_id_horario_seq_1_1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE horario_id_horario_seq_1_1 OWNER TO postgres;

--
-- Name: horario_id_horario_seq_1_1; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE horario_id_horario_seq_1_1 OWNED BY horario.id_horario;


--
-- Name: inicio_slide; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE inicio_slide (
    id_inicioslide integer NOT NULL,
    id_gimnasio integer NOT NULL,
    img_slide character varying(150) NOT NULL,
    imgbtn_slide character varying(150) NOT NULL,
    titulo_slide character varying(50) NOT NULL,
    subt_slide character varying(50) NOT NULL,
    nombre_enlace character varying(50),
    direccion_enlace character varying(300)
);


ALTER TABLE inicio_slide OWNER TO postgres;

--
-- Name: inicio_slide_id_inicioslide_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE inicio_slide_id_inicioslide_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE inicio_slide_id_inicioslide_seq OWNER TO postgres;

--
-- Name: inicio_slide_id_inicioslide_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE inicio_slide_id_inicioslide_seq OWNED BY inicio_slide.id_inicioslide;


--
-- Name: instructor; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE instructor (
    id_instructor integer NOT NULL,
    id integer NOT NULL,
    inst_profesion character varying(50),
    inst_estado boolean,
    inst_descripcion character varying(500),
    inst_imagen character varying(150)
);


ALTER TABLE instructor OWNER TO postgres;

--
-- Name: instructor_id_instructor_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE instructor_id_instructor_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE instructor_id_instructor_seq OWNER TO postgres;

--
-- Name: instructor_id_instructor_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE instructor_id_instructor_seq OWNED BY instructor.id_instructor;


--
-- Name: matricula; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE matricula (
    id_matricula integer NOT NULL,
    id_deportista integer NOT NULL,
    id_secretaria integer,
    estado boolean NOT NULL,
    fecha_matricula timestamp without time zone NOT NULL,
    costo real NOT NULL
);


ALTER TABLE matricula OWNER TO postgres;

--
-- Name: matricula_disciplina; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE matricula_disciplina (
    id_matriculadisciplina integer NOT NULL,
    id_matricula integer NOT NULL,
    id_disciplina_horario integer
);


ALTER TABLE matricula_disciplina OWNER TO postgres;

--
-- Name: matricula_disciplina_id_matriculadisciplina_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE matricula_disciplina_id_matriculadisciplina_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE matricula_disciplina_id_matriculadisciplina_seq OWNER TO postgres;

--
-- Name: matricula_disciplina_id_matriculadisciplina_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE matricula_disciplina_id_matriculadisciplina_seq OWNED BY matricula_disciplina.id_matriculadisciplina;


--
-- Name: matricula_id_matricula_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE matricula_id_matricula_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE matricula_id_matricula_seq OWNER TO postgres;

--
-- Name: matricula_id_matricula_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE matricula_id_matricula_seq OWNED BY matricula.id_matricula;


--
-- Name: mensualidad; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE mensualidad (
    id_mensualidad integer NOT NULL,
    id_matricula integer NOT NULL,
    concepto character varying(100) NOT NULL,
    total real NOT NULL,
    pagado boolean NOT NULL,
    fecha_mensualidad date NOT NULL,
    fecha_pago date NOT NULL
);


ALTER TABLE mensualidad OWNER TO postgres;

--
-- Name: mensualidad_id_mensualidad_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE mensualidad_id_mensualidad_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE mensualidad_id_mensualidad_seq OWNER TO postgres;

--
-- Name: mensualidad_id_mensualidad_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE mensualidad_id_mensualidad_seq OWNED BY mensualidad.id_mensualidad;


--
-- Name: parallax; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE parallax (
    id_parallax integer NOT NULL,
    id_gimnasio integer NOT NULL,
    img_parallax character varying(150) NOT NULL,
    title_parallax character varying(50) NOT NULL,
    desc_parallax character varying(700) NOT NULL,
    imginfe_parallax character varying(150) NOT NULL,
    enlace_video character varying(300)
);


ALTER TABLE parallax OWNER TO postgres;

--
-- Name: parallax_dos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE parallax_dos (
    id_parallaxdos integer NOT NULL,
    id_gimnasio integer NOT NULL,
    img_parallaxdos character varying(150) NOT NULL,
    costo_parallaxdos real NOT NULL,
    dias_parallaxtres character varying(10) NOT NULL,
    desc_parallaxdos character varying(50) NOT NULL
);


ALTER TABLE parallax_dos OWNER TO postgres;

--
-- Name: parallax_dos_id_parallaxdos_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE parallax_dos_id_parallaxdos_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE parallax_dos_id_parallaxdos_seq OWNER TO postgres;

--
-- Name: parallax_dos_id_parallaxdos_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE parallax_dos_id_parallaxdos_seq OWNED BY parallax_dos.id_parallaxdos;


--
-- Name: parallax_id_parallax_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE parallax_id_parallax_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE parallax_id_parallax_seq OWNER TO postgres;

--
-- Name: parallax_id_parallax_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE parallax_id_parallax_seq OWNED BY parallax.id_parallax;


--
-- Name: parallax_uno; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE parallax_uno (
    id_parallaxuno integer NOT NULL,
    id_gimnasio integer NOT NULL,
    back_parallaxuno character varying(150) NOT NULL,
    img_parallaxuno character varying(150) NOT NULL,
    title_parallaxuno character varying(50) NOT NULL,
    subt_parallaxuno character varying(50) NOT NULL
);


ALTER TABLE parallax_uno OWNER TO postgres;

--
-- Name: parallax_uno_id_parallaxuno_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE parallax_uno_id_parallaxuno_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE parallax_uno_id_parallaxuno_seq OWNER TO postgres;

--
-- Name: parallax_uno_id_parallaxuno_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE parallax_uno_id_parallaxuno_seq OWNED BY parallax_uno.id_parallaxuno;


--
-- Name: producto; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE producto (
    id_producto integer NOT NULL,
    id_gimnasio integer NOT NULL,
    nombre_prod character varying(30) NOT NULL,
    desc_prod character varying(500) NOT NULL,
    img_prod character varying(500) NOT NULL,
    precio_prod real
);


ALTER TABLE producto OWNER TO postgres;

--
-- Name: producto_id_producto_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE producto_id_producto_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE producto_id_producto_seq OWNER TO postgres;

--
-- Name: producto_id_producto_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE producto_id_producto_seq OWNED BY producto.id_producto;


--
-- Name: secretaria; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE secretaria (
    id_secretaria integer NOT NULL,
    id integer NOT NULL,
    id_administrador integer
);


ALTER TABLE secretaria OWNER TO postgres;

--
-- Name: secretaria_id_secretaria_seq_1_1; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE secretaria_id_secretaria_seq_1_1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE secretaria_id_secretaria_seq_1_1 OWNER TO postgres;

--
-- Name: secretaria_id_secretaria_seq_1_1; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE secretaria_id_secretaria_seq_1_1 OWNED BY secretaria.id_secretaria;


--
-- Name: seguimiento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE seguimiento (
    id_seguimiento integer NOT NULL,
    diametro_brazo real,
    fecha date NOT NULL,
    id_matricula integer NOT NULL
);


ALTER TABLE seguimiento OWNER TO postgres;

--
-- Name: seguimiento_id_seguimiento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE seguimiento_id_seguimiento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE seguimiento_id_seguimiento_seq OWNER TO postgres;

--
-- Name: seguimiento_id_seguimiento_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE seguimiento_id_seguimiento_seq OWNED BY seguimiento.id_seguimiento;


--
-- Name: id_acerca; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY acerca_de ALTER COLUMN id_acerca SET DEFAULT nextval('acerca_de_id_acerca_seq'::regclass);


--
-- Name: id_administrador; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY administrador ALTER COLUMN id_administrador SET DEFAULT nextval('administrador_id_administrador_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group ALTER COLUMN id SET DEFAULT nextval('auth_group_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('auth_group_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_permission ALTER COLUMN id SET DEFAULT nextval('auth_permission_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user ALTER COLUMN id SET DEFAULT nextval('auth_user_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_groups ALTER COLUMN id SET DEFAULT nextval('auth_user_groups_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('auth_user_user_permissions_id_seq'::regclass);


--
-- Name: id_contacto; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY contacto ALTER COLUMN id_contacto SET DEFAULT nextval('contacto_id_contacto_seq'::regclass);


--
-- Name: id_deportista; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY deportista ALTER COLUMN id_deportista SET DEFAULT nextval('deportista_id_deportista_seq'::regclass);


--
-- Name: id_disciplina; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY disciplina ALTER COLUMN id_disciplina SET DEFAULT nextval('disciplina_id_disciplina_seq'::regclass);


--
-- Name: id_disciplina_horario; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY disciplina_instructor ALTER COLUMN id_disciplina_horario SET DEFAULT nextval('disciplina_instructor_id_disciplina_horario_seq_1'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_admin_log ALTER COLUMN id SET DEFAULT nextval('django_admin_log_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_content_type ALTER COLUMN id SET DEFAULT nextval('django_content_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_migrations ALTER COLUMN id SET DEFAULT nextval('django_migrations_id_seq'::regclass);


--
-- Name: id_email; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY email ALTER COLUMN id_email SET DEFAULT nextval('email_id_email_seq'::regclass);


--
-- Name: id_galeria; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY galeria ALTER COLUMN id_galeria SET DEFAULT nextval('galeria_id_galeria_seq'::regclass);


--
-- Name: id_gimnasio; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY gimnasio ALTER COLUMN id_gimnasio SET DEFAULT nextval('gimnasio_id_gimnasio_seq_5'::regclass);


--
-- Name: id_horario; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY horario ALTER COLUMN id_horario SET DEFAULT nextval('horario_id_horario_seq_1_1'::regclass);


--
-- Name: id_inicioslide; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY inicio_slide ALTER COLUMN id_inicioslide SET DEFAULT nextval('inicio_slide_id_inicioslide_seq'::regclass);


--
-- Name: id_instructor; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY instructor ALTER COLUMN id_instructor SET DEFAULT nextval('instructor_id_instructor_seq'::regclass);


--
-- Name: id_matricula; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY matricula ALTER COLUMN id_matricula SET DEFAULT nextval('matricula_id_matricula_seq'::regclass);


--
-- Name: id_matriculadisciplina; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY matricula_disciplina ALTER COLUMN id_matriculadisciplina SET DEFAULT nextval('matricula_disciplina_id_matriculadisciplina_seq'::regclass);


--
-- Name: id_mensualidad; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY mensualidad ALTER COLUMN id_mensualidad SET DEFAULT nextval('mensualidad_id_mensualidad_seq'::regclass);


--
-- Name: id_parallax; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY parallax ALTER COLUMN id_parallax SET DEFAULT nextval('parallax_id_parallax_seq'::regclass);


--
-- Name: id_parallaxdos; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY parallax_dos ALTER COLUMN id_parallaxdos SET DEFAULT nextval('parallax_dos_id_parallaxdos_seq'::regclass);


--
-- Name: id_parallaxuno; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY parallax_uno ALTER COLUMN id_parallaxuno SET DEFAULT nextval('parallax_uno_id_parallaxuno_seq'::regclass);


--
-- Name: id_producto; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY producto ALTER COLUMN id_producto SET DEFAULT nextval('producto_id_producto_seq'::regclass);


--
-- Name: id_secretaria; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY secretaria ALTER COLUMN id_secretaria SET DEFAULT nextval('secretaria_id_secretaria_seq_1_1'::regclass);


--
-- Name: id_seguimiento; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY seguimiento ALTER COLUMN id_seguimiento SET DEFAULT nextval('seguimiento_id_seguimiento_seq'::regclass);


--
-- Data for Name: acerca_de; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY acerca_de (id_acerca, id_gimnasio, titulo_acerca, desc_acerca, sidea_acerca, sidea1_acerca, sideb_acerca, sideb1_acerca, img_uno, img_dos) FROM stdin;
1	1	ACERCA DE NOSOTROS	<p style="margin-left:0cm; margin-right:0cm; text-align:justify">&nbsp;</p>\r\n\r\n<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Desde que se abri&oacute; las puertas se ha venido contratando y actualizando conocimientos con el personal que labora en el club deportivo, en base a esta eficiente capacidad de entrenamiento ha logrado t&iacute;tulos de entrenador internacional IFBB de f&iacute;sico culturismo, Instructor Nacional de Aer&oacute;bicos y Bailo terapia, entrenador nacional de TAE KWONDO adem&aacute;s de ser juez Nacional en F&iacute;sico Culturismo, y &aacute;rbitro nacional en TAE KWON DO. </span></span></p>\r\n\r\n<p style="margin-left:0cm; margin-right:0cm; text-align:justify">&nbsp;</p>\r\n\r\n<p style="margin-left:0cm; margin-right:0cm; text-align:justify">&nbsp;</p>\r\n\r\n<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt"><span style="font-family:&quot;Times New Roman&quot;,serif">En &aacute;reas como TAE KWON DO y F&iacute;sico Culturismo en varias ocasiones y a nivel nacional ha venido trabajando con distintos clientes siendo as&iacute; uno de los mejores clubes deportivos logrando los siguientes resultados. </span></span></p>\r\n\r\n<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Tae kwon Do:</span></span></p>\r\n\r\n<ul>\r\n\t<li style="text-align:justify"><span style="font-size:12pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Campeones nacionales oficiales de Tae Kwon Do.</span></span></li>\r\n\t<li style="text-align:justify"><span style="font-size:12pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Campeones juegos nacionales de Tae Kwon Do.</span></span></li>\r\n\t<li style="text-align:justify"><span style="font-size:12pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Campeones por equipos en varios torneos de car&aacute;cter abierto.</span></span></li>\r\n\t<li style="text-align:justify"><span style="font-size:12pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Medallistas provinciales y nacionales en las categor&iacute;as infantil, menores, pre juvenil, juvenil y adultos. </span></span></li>\r\n</ul>\r\n\r\n<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:12pt"><span style="font-family:&quot;Times New Roman&quot;,serif">F&iacute;sico Culturismo:</span></span></p>\r\n\r\n<ul>\r\n\t<li style="text-align:justify"><span style="font-size:12pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Campeones por equipos en la categor&iacute;a novatos y clasificados de f&iacute;sico culturismo por m&aacute;s de 4 ocasiones.</span></span></li>\r\n\t<li style="text-align:justify"><span style="font-size:12pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Seleccionados nacionales en f&iacute;sico culturismo.</span></span></li>\r\n\t<li style="text-align:justify"><span style="font-size:12pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Medallistas de plata en eventos internacionales oficiales como lo es el sudamericano de f&iacute;sico culturismo.</span></span></li>\r\n</ul>	Formas deportistas con el mas alto rendimiento capaces de superar...	Ser una de los mejores clubes deportivos de la ciudad contando con ...	<p><strong>-Responsabilidad </strong></p>\r\n\r\n<p><strong>-Disciplina </strong></p>\r\n\r\n<p><strong>-Respeto</strong></p>\r\n\r\n<p><strong>-Perseverancia</strong></p>	<h2>-Quito</h2>\r\n\r\n<h2>-Cuenca</h2>		
\.


--
-- Name: acerca_de_id_acerca_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('acerca_de_id_acerca_seq', 1, true);


--
-- Data for Name: administrador; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY administrador (id_administrador, id) FROM stdin;
1	3
\.


--
-- Name: administrador_id_administrador_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('administrador_id_administrador_seq', 1, true);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_group (id, name) FROM stdin;
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_group_id_seq', 1, false);


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_group_permissions_id_seq', 1, false);


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can add permission	2	add_permission
5	Can change permission	2	change_permission
6	Can delete permission	2	delete_permission
7	Can add group	3	add_group
8	Can change group	3	change_group
9	Can delete group	3	delete_group
10	Can add user	4	add_user
11	Can change user	4	change_user
12	Can delete user	4	delete_user
13	Can add content type	5	add_contenttype
14	Can change content type	5	change_contenttype
15	Can delete content type	5	delete_contenttype
16	Can add session	6	add_session
17	Can change session	6	change_session
18	Can delete session	6	delete_session
19	Can add auth group	7	add_authgroup
20	Can change auth group	7	change_authgroup
21	Can delete auth group	7	delete_authgroup
22	Can add email	8	add_email
23	Can change email	8	change_email
24	Can delete email	8	delete_email
25	Can add django session	9	add_djangosession
26	Can change django session	9	change_djangosession
27	Can delete django session	9	delete_djangosession
28	Can add horario	10	add_horario
29	Can change horario	10	change_horario
30	Can delete horario	10	delete_horario
31	Can add django migrations	11	add_djangomigrations
32	Can change django migrations	11	change_djangomigrations
33	Can delete django migrations	11	delete_djangomigrations
34	Can add parallax uno	12	add_parallaxuno
35	Can change parallax uno	12	change_parallaxuno
36	Can delete parallax uno	12	delete_parallaxuno
37	Can add acerca de	13	add_acercade
38	Can change acerca de	13	change_acercade
39	Can delete acerca de	13	delete_acercade
40	Can add disciplina instructor	14	add_disciplinainstructor
41	Can change disciplina instructor	14	change_disciplinainstructor
42	Can delete disciplina instructor	14	delete_disciplinainstructor
43	Can add auth permission	15	add_authpermission
44	Can change auth permission	15	change_authpermission
45	Can delete auth permission	15	delete_authpermission
46	Can add preinscripcion	16	add_preinscripcion
47	Can change preinscripcion	16	change_preinscripcion
48	Can delete preinscripcion	16	delete_preinscripcion
49	Can add secretaria	17	add_secretaria
50	Can change secretaria	17	change_secretaria
51	Can delete secretaria	17	delete_secretaria
52	Can add deportista	18	add_deportista
53	Can change deportista	18	change_deportista
54	Can delete deportista	18	delete_deportista
55	Can add parallax	19	add_parallax
56	Can change parallax	19	change_parallax
57	Can delete parallax	19	delete_parallax
58	Can add auth user user permissions	20	add_authuseruserpermissions
59	Can change auth user user permissions	20	change_authuseruserpermissions
60	Can delete auth user user permissions	20	delete_authuseruserpermissions
61	Can add producto	21	add_producto
62	Can change producto	21	change_producto
63	Can delete producto	21	delete_producto
64	Can add disciplina horario	22	add_disciplinahorario
65	Can change disciplina horario	22	change_disciplinahorario
66	Can delete disciplina horario	22	delete_disciplinahorario
67	Can add disciplina preinscripcion	23	add_disciplinapreinscripcion
68	Can change disciplina preinscripcion	23	change_disciplinapreinscripcion
69	Can delete disciplina preinscripcion	23	delete_disciplinapreinscripcion
70	Can add inicio slide	24	add_inicioslide
71	Can change inicio slide	24	change_inicioslide
72	Can delete inicio slide	24	delete_inicioslide
73	Can add contacto	25	add_contacto
74	Can change contacto	25	change_contacto
75	Can delete contacto	25	delete_contacto
76	Can add auth group permissions	26	add_authgrouppermissions
77	Can change auth group permissions	26	change_authgrouppermissions
78	Can delete auth group permissions	26	delete_authgrouppermissions
79	Can add auth user	27	add_authuser
80	Can change auth user	27	change_authuser
81	Can delete auth user	27	delete_authuser
82	Can add gimnasio	28	add_gimnasio
83	Can change gimnasio	28	change_gimnasio
84	Can delete gimnasio	28	delete_gimnasio
85	Can add auth user groups	29	add_authusergroups
86	Can change auth user groups	29	change_authusergroups
87	Can delete auth user groups	29	delete_authusergroups
88	Can add administrador	30	add_administrador
89	Can change administrador	30	change_administrador
90	Can delete administrador	30	delete_administrador
91	Can add django content type	31	add_djangocontenttype
92	Can change django content type	31	change_djangocontenttype
93	Can delete django content type	31	delete_djangocontenttype
94	Can add parallax dos	32	add_parallaxdos
95	Can change parallax dos	32	change_parallaxdos
96	Can delete parallax dos	32	delete_parallaxdos
97	Can add disciplina	33	add_disciplina
98	Can change disciplina	33	change_disciplina
99	Can delete disciplina	33	delete_disciplina
100	Can add instructor	34	add_instructor
101	Can change instructor	34	change_instructor
102	Can delete instructor	34	delete_instructor
103	Can add django admin log	35	add_djangoadminlog
104	Can change django admin log	35	change_djangoadminlog
105	Can delete django admin log	35	delete_djangoadminlog
106	Can add matricula	36	add_matricula
107	Can change matricula	36	change_matricula
108	Can delete matricula	36	delete_matricula
109	Can add galeria	37	add_galeria
110	Can change galeria	37	change_galeria
111	Can delete galeria	37	delete_galeria
112	Can add matricula disciplina	38	add_matriculadisciplina
113	Can change matricula disciplina	38	change_matriculadisciplina
114	Can delete matricula disciplina	38	delete_matriculadisciplina
115	Can add mensualidad	39	add_mensualidad
116	Can change mensualidad	39	change_mensualidad
117	Can delete mensualidad	39	delete_mensualidad
118	Can add seguimiento	40	add_seguimiento
119	Can change seguimiento	40	change_seguimiento
120	Can delete seguimiento	40	delete_seguimiento
\.


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_permission_id_seq', 120, true);


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined, dni, genero, celular, img_perfil, edad, telefono, es_administrador, es_deportista, es_instructor, es_secretaria, direccion) FROM stdin;
2	Angelc1997	2018-04-14 13:44:51.472-05	f	secretaria1	secretaria1	Lalangui	lore25@yahoo.es	t	t	2018-04-14 13:44:51.472-05	1106782340	Femenino	0982787093		56	0725693243	f	f	f	t	\N
3	Angelc1997	2018-04-14 14:00:47.803-05	f	administrador1	administrador1	Lalangui	miguel12@yahoo.es	f	f	2018-04-14 14:00:47.803-05	1111111111	Masculino	0982787093		3 	\N	t	f	f	f	\N
4	Angelc1997	2018-04-14 14:04:48.405-05	f	secretaria2	secretaria2	Lalangui	lore25@yahoo.es	f	f	2018-04-14 14:04:48.406-05	1106782333	Femenino	0982787093		4 	\N	f	f	f	t	\N
5	wsddw	2018-04-14 14:05:37.502-05	f	secretaria3	secretaria2	Lalangui	lore25@yahoo.es	f	f	2018-04-14 14:05:37.502-05	1106782335	Femenino	0982787093		4 	\N	f	f	f	t	\N
23	Angelc1997	2018-04-16 08:54:52.057-05	f	rodrigo	Rodrigo	Lievanas	miguelcapa20@gmail.com	t	t	2018-04-16 08:54:52.057-05	1111111122	Masculino	0983625478		2 	0725693243	\N	\N	\N	\N	Quito
24		2018-04-16 08:54:52.124-05	f					t	t	2018-04-16 08:54:52.124-05	          		          		  	\N	\N	\N	\N	\N	
7	Angelc1997	2018-04-14 14:31:32.455-05	f	instructor1	instructor01	Lalangui	lore25@yahoo.es	f	t	2018-04-14 14:31:32.455-05	1106782454	Masculino	0982344566		2 	0725693243	f	f	t	f	\N
8	123456	2018-04-14 14:32:53.643-05	f	instructor02	instructor02	Lalangui	lore25@yahoo.es	f	t	2018-04-14 14:32:53.643-05	1106782457	Masculino	0982344566		2 	0725693243	f	f	t	f	\N
9	Angelc1997|	2018-04-14 14:48:30.251-05	f	instructor03	instructor03	Lalangui	miguel12@yahoo.es	f	t	2018-04-14 14:48:30.251-05	1109876899	Masculino	0983625478		2 	0725693243	f	f	t	f	\N
27	Angelc1997	2018-04-16 09:34:52.064-05	f	tesis	Miguel	Ruiz	miguel12@yahoo.es	t	t	2018-04-16 09:34:52.064-05	1106782343	Masculino	0987632523		34	0725693243	\N	\N	\N	\N	Loja - San Sebastian
29	Angelc1997	2018-04-16 09:45:09.839-05	f	deportista22	Rodrigo	Lalangui	miguel12@yahoo.es	t	t	2018-04-16 09:45:09.839-05	1109876542	Masculino	0982344566		13	0725693243	\N	\N	\N	\N	PItas
16	Angelc1997	2018-04-14 21:58:32.426-05	f	prematricula1	Prematricula1	Lalangui	miguelcapa20@gmail.com	t	t	2018-04-14 21:58:32.426-05	1106782366	Masculino	0982344566		11	0725693243	\N	\N	\N	\N	\N
17	Angelc1997	2018-04-14 21:59:35.062-05	f	prematricula2	Prematricula2	Lalangui	miguelcapa20@gmail.com	t	t	2018-04-14 21:59:35.062-05	1106782344	Masculino	0982344566		11	0725693243	\N	\N	\N	\N	\N
32	Angelc1997	2018-04-16 11:09:55.454-05	f	secretaria6	Ale	Lievanas	miguel12@yahoo.es	t	t	2018-04-16 11:09:55.454-05	1106782322	Femenino	0983625478		34	09876543  	f	f	f	t	Guayaquil
6	Angelc1997	2018-04-14 14:09:16.298-05	f	secretaria4	secretaria2	Lalangui	lore25@yahoo.es	f	f	2018-04-14 14:09:16.299-05	1106782354	Femenino	0982787093		1 	\N	f	f	f	t	Loja
14	123456789	2018-04-14 19:07:01.566-05	f	deportista04	Jose	Lalangui	jose25@yahoo.es	f	f	2018-04-14 19:07:01.567-05	1106782323	Masculino	0982344566		56	\N	f	t	f	f	Loja
18	Angelc1997	2018-04-15 16:48:17.687-05	f	deportista13	preinscripcion10	Ruiz	migwe0@gmail.com	t	t	2018-04-15 16:48:17.687-05	1111111188	Masculino	0982787093		2 	0725693243	\N	\N	\N	\N	Guayaquil
19	pbkdf2_sha256$36000$o2sauMeiMdZs$bKa+x4DUKIdI/0aukzsYUnSMV7HqLBjfg48e6Ib/2oU=	2018-04-17 16:57:25.309-05	f	deportiusta03	Jose	Cuenca	miguel12@yahoo.es	f	t	2018-04-15 20:06:51.928-05	\N	\N	\N	\N	\N	\N	\N	t	\N	\N	\N
13	Angelc1997	2018-04-14 14:56:23.163-05	f	deportista02	deportista02	Lalangui	miguel12@yahoo.es	f	t	2018-04-14 14:56:23.163-05	1111111199	Masculino	0982344566		8 	0725693243	f	t	f	f	Loja
38	Angelc1997	2018-04-17 10:02:46.547-05	f	instructorcarlos	Carlos Humberto	Jerves Galvan	carlosjervesg@yahoo.com	t	t	2018-04-17 10:02:46.547-05	1103559751	Masculino	0989654794		40	072546814 	f	f	t	f	Loja
39	Angelc1997	2018-04-17 10:08:48.467-05	f	DEPORTISTA34	Carlos	Jerves	miguel12@yahoo.es	t	t	2018-04-17 10:08:48.467-05	1109876588	Masculino	0982643564		1 	\N	f	t	f	f	Loja
20	Angelc1997	2018-04-16 00:44:32.097-05	f	secretaria44	secretaria3	Lievanas	secre25@yahoo.es	t	t	2018-04-16 00:44:32.097-05	1109876522	Femenino	0982787093		3 	0725693243	f	f	f	t	Loja - San Sebastian
21	Angelc1997	2018-04-16 00:55:41.188-05	f	usuario9	usuario22	Lievanas	miguelcapa25@yahoo.es	t	t	2018-04-16 00:55:41.188-05	1111111333	Masculino	0983625478		3 	0725693243	f	f	f	t	Loja
22	Angelc1997	2018-04-16 00:57:31.587-05	f	usuario10	usuario22	Lievanas	miguelcapa25@yahoo.es	t	t	2018-04-16 00:57:31.587-05	1111111332	Masculino	0983625478		3 	0725693243	f	f	f	t	Loja
1	pbkdf2_sha256$36000$JMszTktRBMlo$WI18we6SRqtSXvROeVK4TQdpsXkwhs1m154XkTS9Yt0=	2018-04-17 17:10:35.155-05	t	tesis001			miguelcapa20@gmail.com	t	t	2018-04-14 13:08:09.26-05	1298337483	Masculino	0983625478		2 	0725693243	\N	\N	\N	\N	Guayaquil
33	pbkdf2_sha256$36000$WY7r3tAYzDtB$TQmDFYV2iWSycsY8Yo/KIkHR09bJQyfV4e0wqOkrJKQ=	2018-04-16 11:26:07.511-05	f	Deportista2	Rodrigo	Lalangui	miguel12@yahoo.es	f	t	2018-04-16 11:13:12.598-05	\N	\N	\N	\N	\N	\N	\N	t	\N	\N	\N
37	pbkdf2_sha256$36000$4X5sP6OnbPOY$hqGyeok5cxyh7vnwIjs+Rn1iI7rdcxLIHGL4COuZCjM=	\N	f	Deportista3	Deportista2	Lievanas	miguel12@yahoo.es	f	t	2018-04-16 16:11:20.378-05	\N	\N	\N	\N	\N	\N	\N	t	\N	\N	\N
30	Angelc1997	2018-04-16 10:51:59.553-05	f	deportista100	Jose	Lievanas	miguelcapa20@gmail.com	t	t	2018-04-16 10:51:59.553-05	1111111455	Masculino	0982787093		2 	0725693243	\N	\N	\N	\N	PItas
31	Angelc1997	2018-04-16 11:07:21.413-05	f	deportista1001	Rodrigo	Ruiz	miguel12@yahoo.es	t	t	2018-04-16 11:07:21.413-05	1109873333	Masculino	0982643564		2 	0725693243	f	t	f	f	Loja
45	pbkdf2_sha256$36000$YKqIKZO3nWSN$HySmHIJY3c9Ce7Ruq6DHLqml/N6Qx83ecqmsPYge/c8=	\N	f	migueocampo1	Miguel	Ocampo	migueocampo@gmail.com	f	t	2018-04-23 19:02:00.546293-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
46	pbkdf2_sha256$36000$F8g7RDTDpGlu$k3lx8DezBgcIgDaT3QNsvbeP7lgtbyZ1iU0y3N7rl8Q=	\N	f	migueocampo2	Miguel	Ocampo	migueocampo@gmail.com	f	t	2018-04-23 19:04:42.630077-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
42	pbkdf2_sha256$36000$8x2T7rIUsUlI$L/66niIpTPlcLdA5mCUyq6QpHKGEO8Q/n+4bMHJcYeY=	\N	t	jonacastro				t	t	2018-04-19 12:04:13.619161-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
47	pbkdf2_sha256$36000$ZvqFp6xB82ZC$aqCE+LNpOa4T8g1unWFR9Jt37RLvNbgNAUr9U655Zac=	\N	f	migueocampo3	Miguel	Ocampo	migueocampo@gmail.com	f	t	2018-04-23 19:06:57.589907-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
48	pbkdf2_sha256$36000$5RhJcKjrdMbv$8PVM31mZ9hMrC+RlA/DOftI1AmCrMVCYGQyDkAgnY90=	\N	f	migueocampo4	Miguel	Ocampo	migueocampo@gmail.com	f	t	2018-04-23 19:07:57.248501-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
49	pbkdf2_sha256$36000$OXy4rEZ8E2ju$vWmEKAVVbHMwmMyJW1o+PPCwZ13DOrGIbEbs/macXZw=	\N	f	migueocampo5	Miguel	Ocampo	migueocampo@gmail.com	f	t	2018-04-23 19:09:04.942357-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
41	pbkdf2_sha256$36000$ZWEXOPIxURdl$Jo8tFR+3r6WOJuM7EC6x/kQCRxeIW6Mf/jfy0cSTRYk=	2018-04-20 06:19:17.623371-05	f	jacastro12	Jonathan	Castro	jonacastro@gmail.com	f	t	2018-04-19 11:33:32.169702-05	\N	\N	\N	\N	\N	\N	\N	t	\N	\N	\N
50	pbkdf2_sha256$36000$QKUiWDe8cGFg$qTsHe1wYgJOmO3GCPFrUAAHucxxgAsuzCcBJtHK6VkA=	\N	f	migueocampo6	Miguel	Ocampo	migueocampo@gmail.com	f	t	2018-04-23 19:10:08.514821-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
51	pbkdf2_sha256$36000$a9RgoOV6qNkp$2XmPVF5toW7XjJEKxeXTuK9sGvZYG/F34Gt4OtY+XDI=	\N	f	migueocampo7	Miguel	Ocampo	migueocampo@gmail.com	f	t	2018-04-23 19:10:54.439443-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
52	pbkdf2_sha256$36000$GJ42hLRghtAi$Nf1FxKX3G+6aQqcXVkAyvMwwGRDlPXG4S+hKxtLHj2g=	\N	f	migueocampo8	Miguel	Ocampo	migueocampo@gmail.com	f	t	2018-04-23 19:11:15.515133-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
43	pbkdf2_sha256$36000$ZGUuDH3uZECA$xnWT4xzL7sG0G3RO8aYi4Cg47TgNgpPXabYxKVFgV+k=	2018-04-24 15:26:46.173486-05	t	jacastro				t	t	2018-04-19 12:06:23.182177-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
44	pbkdf2_sha256$36000$4B14634TbFpZ$1f8e7graYCP1N/lOcSjv1e8brBxqy/BRvZ2xV+WrdWs=	\N	f	migueocampo	Miguel	Ocampo	migueocampo@gmail.com	f	t	2018-04-23 19:01:04.357101-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
53	pbkdf2_sha256$36000$shBXmeQctyQv$PBTHwPmTaz5HFERvqdIvAp5wOdO+8DFwLv9NzssFxCQ=	2018-04-23 19:15:45.059308-05	f	migueocampo9	Miguel	Ocampo	migueocampo@gmail.com	f	t	2018-04-23 19:15:44.788388-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
54	pbkdf2_sha256$36000$ByDd7W5SQXCW$WMGPjy6bnA/GwLF4vcDs+3AJh7720i08oBRAGeY4LAs=	2018-04-23 19:19:02.401756-05	f	jona	Jonathan	Castro	asdf@solnustec.com	f	t	2018-04-23 19:19:02.304024-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
55	pbkdf2_sha256$36000$JPVDkWTiV7Af$f2QGdNI9CvGQtXZQSYw0GbquIs2DF23F+N6tj9giRXA=	2018-04-23 19:19:37.93358-05	f	nelson	nelson	agurto	dfas@gmail.com	f	t	2018-04-23 19:19:37.773673-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
56	pbkdf2_sha256$36000$ignoc9FdtKwA$0qej2oJP8wh5fAxlZhqi0T9hqRuAnuuVDrr++ifHMBg=	2018-04-23 19:34:09.574331-05	f	nelson2	nelson2	nelson2	nelson2@gmail.com	f	t	2018-04-23 19:34:09.489747-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
57	pbkdf2_sha256$36000$djrPUTHzPpCn$G5peXnDSRIeoYJgaS4eN1Eboq/ijat0BwLoaP6dAq1I=	2018-04-23 19:35:51.274726-05	f	nelson3	nelson2	nelson2	nelson2@gmail.com	f	t	2018-04-23 19:35:51.16421-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
58	pbkdf2_sha256$36000$d0S6Kotip0QH$xLVWjdc8VszGfK0hHVDz7JL3oVg8oRpBDeFgRUGEBHE=	2018-04-23 19:36:19.889119-05	f	nelson4	nelson2	nelson2	nelson2@gmail.com	f	t	2018-04-23 19:36:19.733676-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
59	pbkdf2_sha256$36000$Vngks1hW4Bl0$vdD5J80Q/mej3kTOo0xiYLgM/ukreW7C4RCOEHBxpgE=	2018-04-24 12:46:49.384335-05	f	jonath	Jonathan	Castro	mail@gmail.com	f	t	2018-04-24 12:46:49.070471-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
61	pbkdf2_sha256$36000$WrA8pTuZ5YxG$LJ1tyNy8SXQbt/gPOZ3TUsYkr57vF6kW2inJCLLenoc=	\N	f	kjñl	Mario	Anibal	jk	f	t	2018-04-24 12:49:05.887311-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
62	pbkdf2_sha256$36000$jHiHulNXUWcq$NQOjKetzxtNIdpqdsSrbJGxOcwroM++s6BGWu4H39GQ=	\N	f	jsjsj	lololo	lololo	jssj	f	t	2018-04-24 12:50:44.901393-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
64	pbkdf2_sha256$36000$r7rA8OhqUCOV$raD3ZmPrTE8m+CkwoH7/KBW8uzq18/uSWON/iKr/fto=	\N	f	scasa	Juanito	Cazares	dfasd@gmail.coma	f	t	2018-04-24 12:56:20.499858-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
66	pbkdf2_sha256$36000$U7r9opbPGvCi$WhG044x9GIq+Znorp5OfYA6I4qhCauFCcT95rf4+r7U=	2018-04-24 12:57:26.97721-05	f	afasdf	juniaj	sadf	afsdf	f	t	2018-04-24 12:57:26.831572-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
67	pbkdf2_sha256$36000$gntyfgRUmLW8$+2ODQ5/rDNG6aNgfVyepQIWo3wfs6fbR3LrV1T1TWFA=	\N	f	enrique	Manuel	Enrique	mail@gmail.com	f	t	2018-04-24 13:02:45.760742-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
68	pbkdf2_sha256$36000$l2brRHLCcPQm$5dOZ91NoyN0oIZ5ELEwymRpROLlGluic2UNK91ak7bA=	\N	f	enrique2	Manuel	Enrique	mail@gmail.com	f	t	2018-04-24 13:03:28.286342-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
69	pbkdf2_sha256$36000$BtfnGxgbrJWK$Zt4s2BgvA4A2YYjMdZjd8MW6Ww5HsPtwewduG2Moo6s=	\N	f	manuel	Manuelito	Jimenez	manuel@gmail.com	f	t	2018-04-24 13:04:49.951741-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
70	pbkdf2_sha256$36000$k4vaaL8Fl4nS$YhmQcms9SpJSvzORQ0/PnuxXbuj12stelT6jEZoSPII=	\N	f	manuel1	Manuelito	Jimenez	manuel@gmail.com	f	t	2018-04-24 13:05:03.410079-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
72	pbkdf2_sha256$36000$rQM6eFdR6enI$su29ecduvoa7EMFWPcA4MMcoIULCRgYXPTFV5LtTvJA=	2018-04-24 13:11:41.682024-05	f	anibal	Anibal	Gonzalez	asdf@gmail.com	f	t	2018-04-24 13:11:41.51054-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
73	pbkdf2_sha256$36000$gXUoNDaU1hfD$sMp5s141Tgj20mBOtfLwaV5af3R6BS1c+SLxcZlqzzQ=	2018-04-24 13:15:46.832776-05	f	jfkajsdlkfj	dajdfasdj	aksdjflñk	lkjfalksjd	f	t	2018-04-24 13:15:46.68218-05	\N	\N	afkjsdflja	\N	10	afkjsdlfj 	\N	\N	\N	\N	aksdfjalk
74	pbkdf2_sha256$36000$Ww4BVx3CFIQb$V0brNWw3rdtFmeMmtc+jwC1xcZu5tzJN/IJUUv7pouY=	\N	f	anita	Anita	Mariana	asdfa@gmail.com	f	t	2018-04-24 13:18:06.955874-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
75	pbkdf2_sha256$36000$GLcYDbOLMUeD$hnV1X8FzMQmEgzqJxNM0CUhbL05/Qd2SBCfR+mW4SSA=	2018-04-24 13:19:50.584043-05	f	vero	Andrea	Vero	vero@gamil.com	f	t	2018-04-24 13:19:50.464867-05	2123200021	Masculino	0984512315	\N	15	20122556  	\N	\N	\N	\N	Loj
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_user_groups (id, user_id, group_id) FROM stdin;
\.


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_user_groups_id_seq', 1, false);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_user_id_seq', 75, true);


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_user_user_permissions_id_seq', 1, false);


--
-- Data for Name: contacto; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY contacto (id_contacto, id_gimnasio, titulo_contac, desc_contact, direccion_contact, phone_uno, phone_dos, phone_tres) FROM stdin;
1	1	Instalaciones	<p style="text-align:justify">En nuestras instalacines encontrar&aacute;s: Una sala de Fitness con aparatos de cardiovascular (cintas, bicis est&aacute;ticas y de spining, elipticas, remo...), peso libre (barras, mancuernas, discos...), maquinas de fuerza (press, multipower, prensa, extensiones...), zona de estiramientos y abdominales (esterillas, fitball, softball, kettlebell...) y el roc&oacute;dromo. Dos vestuarios﻿ (femenino y masculino) .</p>\r\n\r\n<p><img alt="" src="/static/media/uploads/2018/04/14/fondo_img.jpg" style="height:250px; width:500px" /></p>	Loja, Ramon Pinto entre Miguel Y Azuay	723444222	987654321	\N
\.


--
-- Name: contacto_id_contacto_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('contacto_id_contacto_seq', 1, true);


--
-- Data for Name: deportista; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY deportista (id_deportista, id) FROM stdin;
2	14
1	13
3	19
4	31
5	33
6	37
7	39
8	41
9	58
10	66
11	72
12	73
13	75
\.


--
-- Name: deportista_id_deportista_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('deportista_id_deportista_seq', 13, true);


--
-- Data for Name: disciplina; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY disciplina (id_disciplina, nombre, img_disciplina) FROM stdin;
1	FÍSICO CULTURISMO Y CROSFITT	disciplina/2018/04/14/crosfit.jpg
2	BAILOTERAPIA	disciplina/2018/04/14/crosfit_0urmxTu.jpg
5	TAEKWONDO	disciplina/2018/04/17/crosfit_bBmVW73.jpg
3	ARTES MARCIALES MIXTAS	disciplina/2018/04/17/crosfit_yFWN0lE.jpg
\.


--
-- Name: disciplina_id_disciplina_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('disciplina_id_disciplina_seq', 5, true);


--
-- Data for Name: disciplina_instructor; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY disciplina_instructor (id_disciplina_horario, id_disciplina, id_horario, id_instructor) FROM stdin;
8	3	10	4
9	2	11	4
10	5	5	4
11	5	6	4
\.


--
-- Name: disciplina_instructor_id_disciplina_horario_seq_1; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('disciplina_instructor_id_disciplina_horario_seq_1', 11, true);


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
1	2018-04-14 14:00:47.804-05	3	administrador1 Lalangui	1	[{"added": {}}]	27	1
2	2018-04-14 14:00:50.169-05	1	Administrador object	1	[{"added": {}}]	30	1
3	2018-04-14 15:17:38.592-05	7	deportista02 Lalangui - AERÓBICOS EN STEP - 19:00:00 21:00:00	1	[{"added": {}}]	36	1
4	2018-04-14 15:27:34.986-05	1	GYM DEOS	1	[{"added": {}}]	28	1
5	2018-04-14 15:53:29.2-05	2	ParallaxUno object	3		12	1
6	2018-04-15 20:05:38.698-05	13	deportista02 Lalangui	2	[{"changed": {"fields": ["edad", "direccion", "password"]}}]	27	1
7	2018-04-15 20:05:40.794-05	1	deportista02 Lalangui	2	[]	18	1
8	2018-04-20 16:28:41.56513-05	26	Jose Lalangui	3		36	43
\.


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('django_admin_log_id_seq', 8, true);


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY django_content_type (id, app_label, model) FROM stdin;
1	admin	logentry
2	auth	permission
3	auth	group
4	auth	user
5	contenttypes	contenttype
6	sessions	session
7	app_deos	authgroup
8	app_deos	email
9	app_deos	djangosession
10	app_deos	horario
11	app_deos	djangomigrations
12	app_deos	parallaxuno
13	app_deos	acercade
14	app_deos	disciplinainstructor
15	app_deos	authpermission
16	app_deos	preinscripcion
17	app_deos	secretaria
18	app_deos	deportista
19	app_deos	parallax
20	app_deos	authuseruserpermissions
21	app_deos	producto
22	app_deos	disciplinahorario
23	app_deos	disciplinapreinscripcion
24	app_deos	inicioslide
25	app_deos	contacto
26	app_deos	authgrouppermissions
27	app_deos	authuser
28	app_deos	gimnasio
29	app_deos	authusergroups
30	app_deos	administrador
31	app_deos	djangocontenttype
32	app_deos	parallaxdos
33	app_deos	disciplina
34	app_deos	instructor
35	app_deos	djangoadminlog
36	app_deos	matricula
37	app_deos	galeria
38	app_deos	matriculadisciplina
39	app_deos	mensualidad
40	app_deos	seguimiento
\.


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('django_content_type_id_seq', 40, true);


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2018-04-14 12:43:29.566-05
2	auth	0001_initial	2018-04-14 12:43:30.303-05
3	admin	0001_initial	2018-04-14 12:43:30.481-05
4	admin	0002_logentry_remove_auto_add	2018-04-14 12:43:30.5-05
5	app_deos	0001_initial	2018-04-14 12:43:30.603-05
6	app_deos	0002_disciplinahorario	2018-04-14 12:43:30.608-05
7	contenttypes	0002_remove_content_type_name	2018-04-14 12:43:30.661-05
8	auth	0002_alter_permission_name_max_length	2018-04-14 12:43:30.681-05
9	auth	0003_alter_user_email_max_length	2018-04-14 12:43:30.696-05
10	auth	0004_alter_user_username_opts	2018-04-14 12:43:30.714-05
11	auth	0005_alter_user_last_login_null	2018-04-14 12:43:30.731-05
12	auth	0006_require_contenttypes_0002	2018-04-14 12:43:30.734-05
13	auth	0007_alter_validators_add_error_messages	2018-04-14 12:43:30.748-05
14	auth	0008_alter_user_username_max_length	2018-04-14 12:43:30.822-05
15	sessions	0001_initial	2018-04-14 12:43:30.948-05
\.


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('django_migrations_id_seq', 15, true);


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY django_session (session_key, session_data, expire_date) FROM stdin;
lhygosa88os44q1bn6rsdcjpvgiyg7cp	MzkzZjBjZmE2Mjc2MTdkYTU4OGQyNzEyNzc0Yzk0NzYzZTEzNDUxYjp7Il9hdXRoX3VzZXJfaGFzaCI6IjBhNjYyNTAwNmY0ODIzNTRmMGZiNzdjMWU5NDcxNWQ3Y2JiZGEzNDkiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2018-05-01 17:10:35.161-05
7sx7sfxvnb3850u6twiy89dnkj7wuf04	NGFlYmM2YTIyMGZiYTgxMTdiZmYzNjk0NjU4Y2E4YzAyZjYwYTQ0YTp7Il9hdXRoX3VzZXJfaWQiOiI0MyIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOGMwMTQ0OTcyYmU5ZTJkYWViZTJlM2EwNWMwNDMxMjM5Mzc5NjIzOSJ9	2018-05-03 19:05:12.521933-05
jnx5oey7b0cx3mdi6ufg0rs4k4xktww5	NDBmOTJiZGM1NmVlZGQ0Mjg4ZGU3YjBjNDY0ZTFhMWFjMjhiMjg2ZDp7Il9hdXRoX3VzZXJfaWQiOiI0MSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNmRjMzU1NjIxNThhZTdkMTAwZDk4MTk4YWQ2NzVhMmU0ZGRhNTFlOCJ9	2018-05-04 06:19:17.626379-05
5zf15mzq73xjq2spi6z235fa7tejksy1	NGFlYmM2YTIyMGZiYTgxMTdiZmYzNjk0NjU4Y2E4YzAyZjYwYTQ0YTp7Il9hdXRoX3VzZXJfaWQiOiI0MyIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOGMwMTQ0OTcyYmU5ZTJkYWViZTJlM2EwNWMwNDMxMjM5Mzc5NjIzOSJ9	2018-05-04 06:40:04.355419-05
3lyx9y0u8hmktxy6q1jrlx0jkqnfpmv5	NGFlYmM2YTIyMGZiYTgxMTdiZmYzNjk0NjU4Y2E4YzAyZjYwYTQ0YTp7Il9hdXRoX3VzZXJfaWQiOiI0MyIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOGMwMTQ0OTcyYmU5ZTJkYWViZTJlM2EwNWMwNDMxMjM5Mzc5NjIzOSJ9	2018-05-04 13:31:57.944775-05
e2himcebqb88vitpnuypv3g0e9aa84qz	NGFlYmM2YTIyMGZiYTgxMTdiZmYzNjk0NjU4Y2E4YzAyZjYwYTQ0YTp7Il9hdXRoX3VzZXJfaWQiOiI0MyIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOGMwMTQ0OTcyYmU5ZTJkYWViZTJlM2EwNWMwNDMxMjM5Mzc5NjIzOSJ9	2018-05-04 15:57:11.995202-05
ao1zb9d0k7abduy4h9eey0tnpyvpqbga	NGFlYmM2YTIyMGZiYTgxMTdiZmYzNjk0NjU4Y2E4YzAyZjYwYTQ0YTp7Il9hdXRoX3VzZXJfaWQiOiI0MyIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOGMwMTQ0OTcyYmU5ZTJkYWViZTJlM2EwNWMwNDMxMjM5Mzc5NjIzOSJ9	2018-05-04 16:28:27.851784-05
gp4unxm6gpid3uceyf7lybr91mtol4pb	NDYwZjVlYTZkNzlhZGY4ZGEyMzRiODRjYWI3NmFjYTVkZTFjNzUyMDp7fQ==	2018-05-07 19:34:09.572322-05
2rs9b7xo7wvgx87olxx2qktxjhckbab5	NDYwZjVlYTZkNzlhZGY4ZGEyMzRiODRjYWI3NmFjYTVkZTFjNzUyMDp7fQ==	2018-05-07 19:35:51.27272-05
25ob3euagnlqm7bnk7ugj2gzkkgq17ha	NGFlYmM2YTIyMGZiYTgxMTdiZmYzNjk0NjU4Y2E4YzAyZjYwYTQ0YTp7Il9hdXRoX3VzZXJfaWQiOiI0MyIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOGMwMTQ0OTcyYmU5ZTJkYWViZTJlM2EwNWMwNDMxMjM5Mzc5NjIzOSJ9	2018-05-07 19:46:01.607768-05
jncl6j62a3iujyr256lyrw5a77dsnjtc	NDYwZjVlYTZkNzlhZGY4ZGEyMzRiODRjYWI3NmFjYTVkZTFjNzUyMDp7fQ==	2018-05-08 12:46:49.382004-05
hprd4i4o01dz0brepodi4jaabd7gr9db	NGFlYmM2YTIyMGZiYTgxMTdiZmYzNjk0NjU4Y2E4YzAyZjYwYTQ0YTp7Il9hdXRoX3VzZXJfaWQiOiI0MyIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOGMwMTQ0OTcyYmU5ZTJkYWViZTJlM2EwNWMwNDMxMjM5Mzc5NjIzOSJ9	2018-05-08 15:26:46.176638-05
\.


--
-- Data for Name: email; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY email (id_email, nombre, direccion, email, telefono, mensaje) FROM stdin;
\.


--
-- Name: email_id_email_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('email_id_email_seq', 1, false);


--
-- Data for Name: galeria; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY galeria (id_galeria, id_gimnasio, titulo_galeria, img_galeria, desc_galeria) FROM stdin;
1	1	FÍSICO CULTURISMO	galeria/2018/04/14/imagen6.jpg	Destacados en la disciplina de físico culturismo a nivel nacional
2	1	CROSFIT	galeria/2018/04/17/imagen6.jpg	Destacados en la disciplina de físico crosfit  nivel nacional
\.


--
-- Name: galeria_id_galeria_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('galeria_id_galeria_seq', 2, true);


--
-- Data for Name: gimnasio; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY gimnasio (id_gimnasio, nombre_gym) FROM stdin;
1	GYM DEOS
\.


--
-- Name: gimnasio_id_gimnasio_seq_5; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('gimnasio_id_gimnasio_seq_5', 1, true);


--
-- Data for Name: horario; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY horario (id_horario, hora_inicio, hora_fin, ubicacion, sesion, desc_horario, categoria, cupos) FROM stdin;
4	14:00:00	15:00:00	Sala 1	Tarde	\N	4 a 6 anios	40
7	15:00:00	16:00:00	Sala1	Tarde	\N	7 a 13 anios	10
8	16:00:00	17:00:00	Sala 2	Tarde	\N	9 a 17 anios	8
9	18:00:00	20:00:00	sala 3	Noche	\N	Avanzados	5
11	20:00:00	21:00:00	sala 3	Tarde	\N	Avanzados	0
10	17:00:00	18:30:00	Sala 1	Tarde	\N	Avanzados	0
5	08:30:00	10:00:00	Sala 1	Manana	\N	Avanzados	34
6	10:00:00	11:00:00	Sala1	Manana	\N	Avanzados	20
\.


--
-- Name: horario_id_horario_seq_1_1; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('horario_id_horario_seq_1_1', 11, true);


--
-- Data for Name: inicio_slide; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY inicio_slide (id_inicioslide, id_gimnasio, img_slide, imgbtn_slide, titulo_slide, subt_slide, nombre_enlace, direccion_enlace) FROM stdin;
1	1	inicio/2018/04/14/slide1.jpg	inicio/2018/04/14/slide_1_btn.jpg	CLUB DEPORTIVO DEOS	Nueva Temporada	\N	\N
2	1	inicio/2018/04/14/slide4_.jpg	inicio/2018/04/14/slide4_btn.jpg	El mejor gimnasio of the city	Nueva Temporada	\N	\N
\.


--
-- Name: inicio_slide_id_inicioslide_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('inicio_slide_id_inicioslide_seq', 5, true);


--
-- Data for Name: instructor; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY instructor (id_instructor, id, inst_profesion, inst_estado, inst_descripcion, inst_imagen) FROM stdin;
2	8	\N	\N	\N	\N
4	38	\N	\N	\N	\N
\.


--
-- Name: instructor_id_instructor_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('instructor_id_instructor_seq', 4, true);


--
-- Data for Name: matricula; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY matricula (id_matricula, id_deportista, id_secretaria, estado, fecha_matricula, costo) FROM stdin;
33	8	\N	f	2018-04-24 00:04:42.724894	15
34	8	\N	f	2018-04-24 00:06:57.687041	15
35	8	\N	f	2018-04-24 00:07:57.322698	10
36	8	\N	f	2018-04-24 00:09:05.004614	10
37	8	\N	f	2018-04-24 00:10:08.568851	10
38	8	\N	f	2018-04-24 00:10:54.49739	10
39	8	\N	f	2018-04-24 00:11:15.655905	10
40	8	\N	f	2018-04-24 00:15:44.831643	10
41	8	\N	f	2018-04-24 00:19:02.348141	5
42	8	\N	f	2018-04-24 00:19:37.855693	20
43	9	\N	f	2018-04-24 00:36:19.894894	10
44	9	\N	f	2018-04-24 00:36:46.442734	5
45	9	\N	f	2018-04-24 00:43:39.488621	5
46	9	\N	f	2018-04-24 00:43:57.811549	5
47	9	\N	f	2018-04-24 00:44:06.633361	5
48	9	\N	f	2018-04-24 05:44:21.311668	5
49	10	\N	f	2018-04-24 17:57:27.095972	10
50	11	\N	f	2018-04-24 18:11:41.688337	10
51	12	\N	f	2018-04-24 18:15:46.839292	10
52	13	\N	f	2018-04-24 18:19:50.59006	10
\.


--
-- Data for Name: matricula_disciplina; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY matricula_disciplina (id_matriculadisciplina, id_matricula, id_disciplina_horario) FROM stdin;
1	33	8
2	34	8
3	35	8
4	36	8
5	37	8
6	38	8
7	39	8
8	40	8
9	40	10
10	41	8
11	42	8
12	42	9
13	42	10
14	42	11
15	43	8
16	43	9
17	44	10
18	45	11
19	46	10
20	47	9
21	48	8
22	49	10
23	49	11
24	50	10
25	50	11
26	51	10
27	51	11
28	52	10
29	52	11
\.


--
-- Name: matricula_disciplina_id_matriculadisciplina_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('matricula_disciplina_id_matriculadisciplina_seq', 29, true);


--
-- Name: matricula_id_matricula_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('matricula_id_matricula_seq', 52, true);


--
-- Data for Name: mensualidad; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY mensualidad (id_mensualidad, id_matricula, concepto, total, pagado, fecha_mensualidad, fecha_pago) FROM stdin;
\.


--
-- Name: mensualidad_id_mensualidad_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('mensualidad_id_mensualidad_seq', 12, true);


--
-- Data for Name: parallax; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY parallax (id_parallax, id_gimnasio, img_parallax, title_parallax, desc_parallax, imginfe_parallax, enlace_video) FROM stdin;
1	1	parallax/2018/04/14/imagen.jpg	CLUB DEPORTIVO DEOS	Conoce acerca de nuestras disciplinas	parallax/2018/04/14/imagen_inferior.jpg	\N
\.


--
-- Data for Name: parallax_dos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY parallax_dos (id_parallaxdos, id_gimnasio, img_parallaxdos, costo_parallaxdos, dias_parallaxtres, desc_parallaxdos) FROM stdin;
1	1	parallaxdos/2018/04/14/fondo_img.jpg	49	Quince	Inscríbete a Gym Deos
2	1	parallaxdos/2018/04/14/fondo_img_YJ5xojr.jpg	25	Mensual	Gimnasio DEOS
\.


--
-- Name: parallax_dos_id_parallaxdos_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('parallax_dos_id_parallaxdos_seq', 2, true);


--
-- Name: parallax_id_parallax_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('parallax_id_parallax_seq', 1, true);


--
-- Data for Name: parallax_uno; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY parallax_uno (id_parallaxuno, id_gimnasio, back_parallaxuno, img_parallaxuno, title_parallaxuno, subt_parallaxuno) FROM stdin;
1	1	parallaxuno/2018/04/14/imagen_fondo.jpg	parallaxuno/2018/04/14/imagen1.jpg	Visitanos	LOja, Ramon entre Miguel y Azuay
\.


--
-- Name: parallax_uno_id_parallaxuno_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('parallax_uno_id_parallaxuno_seq', 2, true);


--
-- Data for Name: producto; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY producto (id_producto, id_gimnasio, nombre_prod, desc_prod, img_prod, precio_prod) FROM stdin;
1	1	SUPER MASS GAINER	Su principal función es la de brindarte los nutrientes necesarios para ganar peso muscular sobre todo cuando se trata de personas con una fisiología muy delgada, por lo cual este tipo de suplementos gym te aportaran ese extra para lograr ese gran objetivo	producto/2018/04/14/7.jpg	44.4500008
2	1	SUPER MASS GAINER	Nitro-Tech es una fórmula de desarrollo muscular de suero + aislado diseñada para todos los atletas que busquen más músculo, más fuerza y un mejor rendimiento. Nitro-Tech contiene proteínas obtenidas principalmente de péptidos de proteína de suero y aislado de suero, dos de las fuentes de proteína más limpias y puras disponibles para los atletas. Nitro-Tech también se ha mejorado con la forma más estudiada de creatina para una mejor ganancia muscular y de fuerza.	producto/2018/04/14/descarga.jpg	80.7799988
\.


--
-- Name: producto_id_producto_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('producto_id_producto_seq', 3, true);


--
-- Data for Name: secretaria; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY secretaria (id_secretaria, id, id_administrador) FROM stdin;
5	6	\N
6	32	\N
\.


--
-- Name: secretaria_id_secretaria_seq_1_1; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('secretaria_id_secretaria_seq_1_1', 8, true);


--
-- Data for Name: seguimiento; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY seguimiento (id_seguimiento, diametro_brazo, fecha, id_matricula) FROM stdin;
\.


--
-- Name: seguimiento_id_seguimiento_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('seguimiento_id_seguimiento_seq', 1, false);


--
-- Name: acerca_de_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY acerca_de
    ADD CONSTRAINT acerca_de_pk PRIMARY KEY (id_acerca);


--
-- Name: administrador_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY administrador
    ADD CONSTRAINT administrador_pk PRIMARY KEY (id_administrador);


--
-- Name: auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_user_id_group_id_94350c0c_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- Name: auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_user_id_permission_id_14a6b632_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- Name: auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: contacto_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY contacto
    ADD CONSTRAINT contacto_pk PRIMARY KEY (id_contacto);


--
-- Name: deportista_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY deportista
    ADD CONSTRAINT deportista_pk PRIMARY KEY (id_deportista);


--
-- Name: disciplina_instructor_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY disciplina_instructor
    ADD CONSTRAINT disciplina_instructor_pk PRIMARY KEY (id_disciplina_horario);


--
-- Name: disciplina_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY disciplina
    ADD CONSTRAINT disciplina_pk PRIMARY KEY (id_disciplina);


--
-- Name: django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: email_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY email
    ADD CONSTRAINT email_pk PRIMARY KEY (id_email);


--
-- Name: galeria_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY galeria
    ADD CONSTRAINT galeria_pk PRIMARY KEY (id_galeria);


--
-- Name: gimnasio_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY gimnasio
    ADD CONSTRAINT gimnasio_pk PRIMARY KEY (id_gimnasio);


--
-- Name: horario_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY horario
    ADD CONSTRAINT horario_pk PRIMARY KEY (id_horario);


--
-- Name: inicio_slide_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY inicio_slide
    ADD CONSTRAINT inicio_slide_pk PRIMARY KEY (id_inicioslide);


--
-- Name: instructor_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY instructor
    ADD CONSTRAINT instructor_pk PRIMARY KEY (id_instructor);


--
-- Name: matricula_disciplina_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY matricula_disciplina
    ADD CONSTRAINT matricula_disciplina_pk PRIMARY KEY (id_matriculadisciplina);


--
-- Name: matricula_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY matricula
    ADD CONSTRAINT matricula_pk PRIMARY KEY (id_matricula);


--
-- Name: mensualidad_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY mensualidad
    ADD CONSTRAINT mensualidad_pk PRIMARY KEY (id_mensualidad);


--
-- Name: parallax_dos_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY parallax_dos
    ADD CONSTRAINT parallax_dos_pk PRIMARY KEY (id_parallaxdos);


--
-- Name: parallax_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY parallax
    ADD CONSTRAINT parallax_pk PRIMARY KEY (id_parallax);


--
-- Name: parallax_uno_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY parallax_uno
    ADD CONSTRAINT parallax_uno_pk PRIMARY KEY (id_parallaxuno);


--
-- Name: producto_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY producto
    ADD CONSTRAINT producto_pk PRIMARY KEY (id_producto);


--
-- Name: secretaria_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY secretaria
    ADD CONSTRAINT secretaria_pk PRIMARY KEY (id_secretaria);


--
-- Name: seguimiento_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY seguimiento
    ADD CONSTRAINT seguimiento_pk PRIMARY KEY (id_seguimiento);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_name_a6ea08ec_like ON auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_group_id_97559544; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_groups_group_id_97559544 ON auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_user_id_6a12ed8b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_groups_user_id_6a12ed8b ON auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_permission_id_1fbb5f2c; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_user_id_a95ead1b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_username_6821ab7c_like ON auth_user USING btree (username varchar_pattern_ops);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_session_expire_date_a5c62663 ON django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_session_session_key_c0390e0f_like ON django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: administrador_secretaria_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY secretaria
    ADD CONSTRAINT administrador_secretaria_fk FOREIGN KEY (id_administrador) REFERENCES administrador(id_administrador);


--
-- Name: auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_administrador_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY administrador
    ADD CONSTRAINT auth_user_administrador_fk FOREIGN KEY (id) REFERENCES auth_user(id);


--
-- Name: auth_user_cliente_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY deportista
    ADD CONSTRAINT auth_user_cliente_fk FOREIGN KEY (id) REFERENCES auth_user(id);


--
-- Name: auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_secretaria_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY secretaria
    ADD CONSTRAINT auth_user_secretaria_fk FOREIGN KEY (id) REFERENCES auth_user(id);


--
-- Name: auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cliente_matricula_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY matricula
    ADD CONSTRAINT cliente_matricula_fk FOREIGN KEY (id_deportista) REFERENCES deportista(id_deportista);


--
-- Name: disciplina_disciplina_instructor_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY disciplina_instructor
    ADD CONSTRAINT disciplina_disciplina_instructor_fk FOREIGN KEY (id_disciplina) REFERENCES disciplina(id_disciplina);


--
-- Name: disciplina_instructor_matricula_disciplina_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY matricula_disciplina
    ADD CONSTRAINT disciplina_instructor_matricula_disciplina_fk FOREIGN KEY (id_disciplina_horario) REFERENCES disciplina_instructor(id_disciplina_horario);


--
-- Name: django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log_user_id_c564eba6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gimnasio_acerca_de_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY acerca_de
    ADD CONSTRAINT gimnasio_acerca_de_fk FOREIGN KEY (id_gimnasio) REFERENCES gimnasio(id_gimnasio);


--
-- Name: gimnasio_contacto_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY contacto
    ADD CONSTRAINT gimnasio_contacto_fk FOREIGN KEY (id_gimnasio) REFERENCES gimnasio(id_gimnasio);


--
-- Name: gimnasio_galeria_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY galeria
    ADD CONSTRAINT gimnasio_galeria_fk FOREIGN KEY (id_gimnasio) REFERENCES gimnasio(id_gimnasio);


--
-- Name: gimnasio_inicio_slide_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY inicio_slide
    ADD CONSTRAINT gimnasio_inicio_slide_fk FOREIGN KEY (id_gimnasio) REFERENCES gimnasio(id_gimnasio);


--
-- Name: gimnasio_parallax_dos_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY parallax_dos
    ADD CONSTRAINT gimnasio_parallax_dos_fk FOREIGN KEY (id_gimnasio) REFERENCES gimnasio(id_gimnasio);


--
-- Name: gimnasio_parallax_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY parallax
    ADD CONSTRAINT gimnasio_parallax_fk FOREIGN KEY (id_gimnasio) REFERENCES gimnasio(id_gimnasio);


--
-- Name: gimnasio_parallax_uno_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY parallax_uno
    ADD CONSTRAINT gimnasio_parallax_uno_fk FOREIGN KEY (id_gimnasio) REFERENCES gimnasio(id_gimnasio);


--
-- Name: gimnasio_producto_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY producto
    ADD CONSTRAINT gimnasio_producto_fk FOREIGN KEY (id_gimnasio) REFERENCES gimnasio(id_gimnasio);


--
-- Name: horario_disciplina_instructor_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY disciplina_instructor
    ADD CONSTRAINT horario_disciplina_instructor_fk FOREIGN KEY (id_horario) REFERENCES horario(id_horario);


--
-- Name: instructor_disciplina_instructor_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY disciplina_instructor
    ADD CONSTRAINT instructor_disciplina_instructor_fk FOREIGN KEY (id_instructor) REFERENCES instructor(id_instructor);


--
-- Name: matricula_matricula_disciplina_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY matricula_disciplina
    ADD CONSTRAINT matricula_matricula_disciplina_fk FOREIGN KEY (id_matricula) REFERENCES matricula(id_matricula);


--
-- Name: matricula_mensualidad_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY mensualidad
    ADD CONSTRAINT matricula_mensualidad_fk FOREIGN KEY (id_matricula) REFERENCES matricula(id_matricula);


--
-- Name: matricula_seguimiento_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY seguimiento
    ADD CONSTRAINT matricula_seguimiento_fk FOREIGN KEY (id_matricula) REFERENCES matricula(id_matricula);


--
-- Name: persona_instructor_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY instructor
    ADD CONSTRAINT persona_instructor_fk FOREIGN KEY (id) REFERENCES auth_user(id);


--
-- Name: secretaria_matricula_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY matricula
    ADD CONSTRAINT secretaria_matricula_fk FOREIGN KEY (id_secretaria) REFERENCES secretaria(id_secretaria);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

