--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.3
-- Dumped by pg_dump version 9.6.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: acerca_de; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE acerca_de (
    id_acerca integer NOT NULL,
    id_gimnasio integer NOT NULL,
    titulo_acerca character varying(50) NOT NULL,
    desc_acerca character varying(10000) NOT NULL,
    sidea_acerca character varying(300) NOT NULL,
    sidea1_acerca character varying(300) NOT NULL,
    sideb_acerca character varying(300) NOT NULL,
    sideb1_acerca character varying(300) NOT NULL,
    img_uno character varying(150),
    img_dos character varying(150)
);


ALTER TABLE acerca_de OWNER TO postgres;

--
-- Name: acerca_de_id_acerca_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE acerca_de_id_acerca_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE acerca_de_id_acerca_seq OWNER TO postgres;

--
-- Name: acerca_de_id_acerca_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE acerca_de_id_acerca_seq OWNED BY acerca_de.id_acerca;


--
-- Name: administrador; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE administrador (
    id_administrador integer NOT NULL,
    id integer NOT NULL
);


ALTER TABLE administrador OWNER TO postgres;

--
-- Name: administrador_id_administrador_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE administrador_id_administrador_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE administrador_id_administrador_seq OWNER TO postgres;

--
-- Name: administrador_id_administrador_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE administrador_id_administrador_seq OWNED BY administrador.id_administrador;


--
-- Name: app_deos_deportistadestacado; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE app_deos_deportistadestacado (
    id_deportista_des integer NOT NULL,
    nombre character varying(50) NOT NULL,
    apellidos character varying(50) NOT NULL,
    descripcion character varying(50) NOT NULL,
    nivel character varying(50),
    disciplina character varying(50) NOT NULL
);


ALTER TABLE app_deos_deportistadestacado OWNER TO postgres;

--
-- Name: app_deos_deportistadestacado_id_deportista_des_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE app_deos_deportistadestacado_id_deportista_des_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE app_deos_deportistadestacado_id_deportista_des_seq OWNER TO postgres;

--
-- Name: app_deos_deportistadestacado_id_deportista_des_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE app_deos_deportistadestacado_id_deportista_des_seq OWNED BY app_deos_deportistadestacado.id_deportista_des;


--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE auth_group OWNER TO postgres;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_group_id_seq OWNER TO postgres;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_group_id_seq OWNED BY auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE auth_group_permissions OWNER TO postgres;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_group_permissions_id_seq OWNER TO postgres;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_group_permissions_id_seq OWNED BY auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE auth_permission OWNER TO postgres;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_permission_id_seq OWNER TO postgres;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_permission_id_seq OWNED BY auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL,
    dni character(10),
    genero character varying(20),
    celular character(10),
    img_perfil character varying(8000),
    edad character(2),
    telefono character(10),
    es_administrador boolean,
    es_deportista boolean,
    es_instructor boolean,
    es_secretaria boolean,
    direccion character varying(300)
);


ALTER TABLE auth_user OWNER TO postgres;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE auth_user_groups OWNER TO postgres;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_groups_id_seq OWNER TO postgres;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_user_groups_id_seq OWNED BY auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_id_seq OWNER TO postgres;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_user_id_seq OWNED BY auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE auth_user_user_permissions OWNER TO postgres;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_user_permissions_id_seq OWNER TO postgres;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_user_user_permissions_id_seq OWNED BY auth_user_user_permissions.id;


--
-- Name: contacto; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE contacto (
    id_contacto integer NOT NULL,
    id_gimnasio integer NOT NULL,
    titulo_contac character varying(50) NOT NULL,
    desc_contact character varying(80000) NOT NULL,
    direccion_contact character varying(200) NOT NULL,
    phone_uno integer NOT NULL,
    phone_dos integer NOT NULL,
    phone_tres integer
);


ALTER TABLE contacto OWNER TO postgres;

--
-- Name: contacto_id_contacto_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE contacto_id_contacto_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE contacto_id_contacto_seq OWNER TO postgres;

--
-- Name: contacto_id_contacto_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE contacto_id_contacto_seq OWNED BY contacto.id_contacto;


--
-- Name: deportista; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE deportista (
    id_deportista integer NOT NULL,
    id integer NOT NULL
);


ALTER TABLE deportista OWNER TO postgres;

--
-- Name: deportista_destacado; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE deportista_destacado (
    id_deportista_des integer NOT NULL,
    nombre character varying(50) NOT NULL,
    apellidos character varying(50) NOT NULL,
    descripcion character varying(50),
    nivel character varying(12),
    disciplina character varying(20),
    imagen character varying(1000)
);


ALTER TABLE deportista_destacado OWNER TO postgres;

--
-- Name: deportista_destacado_id_deportista_des_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE deportista_destacado_id_deportista_des_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE deportista_destacado_id_deportista_des_seq OWNER TO postgres;

--
-- Name: deportista_destacado_id_deportista_des_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE deportista_destacado_id_deportista_des_seq OWNED BY deportista_destacado.id_deportista_des;


--
-- Name: deportista_id_deportista_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE deportista_id_deportista_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE deportista_id_deportista_seq OWNER TO postgres;

--
-- Name: deportista_id_deportista_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE deportista_id_deportista_seq OWNED BY deportista.id_deportista;


--
-- Name: disciplina; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE disciplina (
    id_disciplina integer NOT NULL,
    nombre character varying(50) NOT NULL,
    img_disciplina character varying(150) NOT NULL
);


ALTER TABLE disciplina OWNER TO postgres;

--
-- Name: disciplina_id_disciplina_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE disciplina_id_disciplina_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE disciplina_id_disciplina_seq OWNER TO postgres;

--
-- Name: disciplina_id_disciplina_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE disciplina_id_disciplina_seq OWNED BY disciplina.id_disciplina;


--
-- Name: disciplina_instructor; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE disciplina_instructor (
    id_disciplina_horario integer NOT NULL,
    id_disciplina integer NOT NULL,
    id_horario integer NOT NULL,
    id_instructor integer NOT NULL
);


ALTER TABLE disciplina_instructor OWNER TO postgres;

--
-- Name: disciplina_instructor_id_disciplina_horario_seq_1; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE disciplina_instructor_id_disciplina_horario_seq_1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE disciplina_instructor_id_disciplina_horario_seq_1 OWNER TO postgres;

--
-- Name: disciplina_instructor_id_disciplina_horario_seq_1; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE disciplina_instructor_id_disciplina_horario_seq_1 OWNED BY disciplina_instructor.id_disciplina_horario;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE django_admin_log OWNER TO postgres;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_admin_log_id_seq OWNER TO postgres;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE django_admin_log_id_seq OWNED BY django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE django_content_type OWNER TO postgres;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_content_type_id_seq OWNER TO postgres;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE django_content_type_id_seq OWNED BY django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE django_migrations OWNER TO postgres;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_migrations_id_seq OWNER TO postgres;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE django_migrations_id_seq OWNED BY django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE django_session OWNER TO postgres;

--
-- Name: email; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE email (
    id_email integer NOT NULL,
    nombre character varying(50) NOT NULL,
    direccion character varying(100),
    email character varying(80) NOT NULL,
    telefono character(10),
    mensaje character varying(100) NOT NULL
);


ALTER TABLE email OWNER TO postgres;

--
-- Name: email_id_email_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE email_id_email_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE email_id_email_seq OWNER TO postgres;

--
-- Name: email_id_email_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE email_id_email_seq OWNED BY email.id_email;


--
-- Name: galeria; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE galeria (
    id_galeria integer NOT NULL,
    id_gimnasio integer NOT NULL,
    titulo_galeria character varying(50) NOT NULL,
    img_galeria character varying(500) NOT NULL,
    desc_galeria character varying(500) NOT NULL
);


ALTER TABLE galeria OWNER TO postgres;

--
-- Name: galeria_id_galeria_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE galeria_id_galeria_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE galeria_id_galeria_seq OWNER TO postgres;

--
-- Name: galeria_id_galeria_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE galeria_id_galeria_seq OWNED BY galeria.id_galeria;


--
-- Name: gimnasio; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE gimnasio (
    id_gimnasio integer NOT NULL,
    nombre_gym character varying(100) NOT NULL
);


ALTER TABLE gimnasio OWNER TO postgres;

--
-- Name: gimnasio_id_gimnasio_seq_5; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE gimnasio_id_gimnasio_seq_5
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE gimnasio_id_gimnasio_seq_5 OWNER TO postgres;

--
-- Name: gimnasio_id_gimnasio_seq_5; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE gimnasio_id_gimnasio_seq_5 OWNED BY gimnasio.id_gimnasio;


--
-- Name: horario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE horario (
    id_horario integer NOT NULL,
    hora_inicio time without time zone NOT NULL,
    hora_fin time without time zone NOT NULL,
    ubicacion character varying(20),
    sesion character varying(10) NOT NULL,
    desc_horario character varying(500),
    categoria character varying(20),
    cupos integer
);


ALTER TABLE horario OWNER TO postgres;

--
-- Name: horario_id_horario_seq_1_1; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE horario_id_horario_seq_1_1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE horario_id_horario_seq_1_1 OWNER TO postgres;

--
-- Name: horario_id_horario_seq_1_1; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE horario_id_horario_seq_1_1 OWNED BY horario.id_horario;


--
-- Name: inicio_slide; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE inicio_slide (
    id_inicioslide integer NOT NULL,
    id_gimnasio integer NOT NULL,
    img_slide character varying(150) NOT NULL,
    imgbtn_slide character varying(150) NOT NULL,
    titulo_slide character varying(50) NOT NULL,
    subt_slide character varying(50) NOT NULL,
    nombre_enlace character varying(50),
    direccion_enlace character varying(300)
);


ALTER TABLE inicio_slide OWNER TO postgres;

--
-- Name: inicio_slide_id_inicioslide_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE inicio_slide_id_inicioslide_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE inicio_slide_id_inicioslide_seq OWNER TO postgres;

--
-- Name: inicio_slide_id_inicioslide_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE inicio_slide_id_inicioslide_seq OWNED BY inicio_slide.id_inicioslide;


--
-- Name: instructor; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE instructor (
    id_instructor integer NOT NULL,
    id integer NOT NULL,
    inst_profesion character varying(50),
    inst_estado boolean,
    inst_descripcion character varying(500),
    inst_imagen character varying(150)
);


ALTER TABLE instructor OWNER TO postgres;

--
-- Name: instructor_id_instructor_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE instructor_id_instructor_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE instructor_id_instructor_seq OWNER TO postgres;

--
-- Name: instructor_id_instructor_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE instructor_id_instructor_seq OWNED BY instructor.id_instructor;


--
-- Name: matricula; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE matricula (
    id_matricula integer NOT NULL,
    id_deportista integer NOT NULL,
    id_secretaria integer,
    estado boolean NOT NULL,
    fecha_matricula timestamp without time zone NOT NULL,
    costo real NOT NULL
);


ALTER TABLE matricula OWNER TO postgres;

--
-- Name: matricula_disciplina; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE matricula_disciplina (
    id_matriculadisciplina integer NOT NULL,
    id_matricula integer NOT NULL,
    id_disciplina_horario integer,
    estado boolean,
    costo real,
    fecha_matricula timestamp without time zone
);


ALTER TABLE matricula_disciplina OWNER TO postgres;

--
-- Name: matricula_disciplina_id_matriculadisciplina_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE matricula_disciplina_id_matriculadisciplina_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE matricula_disciplina_id_matriculadisciplina_seq OWNER TO postgres;

--
-- Name: matricula_disciplina_id_matriculadisciplina_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE matricula_disciplina_id_matriculadisciplina_seq OWNED BY matricula_disciplina.id_matriculadisciplina;


--
-- Name: matricula_id_matricula_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE matricula_id_matricula_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE matricula_id_matricula_seq OWNER TO postgres;

--
-- Name: matricula_id_matricula_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE matricula_id_matricula_seq OWNED BY matricula.id_matricula;


--
-- Name: mensualidad; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE mensualidad (
    id_mensualidad integer NOT NULL,
    concepto character varying(100) NOT NULL,
    total real NOT NULL,
    pagado boolean NOT NULL,
    fecha_mensualidad date NOT NULL,
    fecha_pago date NOT NULL,
    id_matriculadisciplina integer
);


ALTER TABLE mensualidad OWNER TO postgres;

--
-- Name: mensualidad_id_mensualidad_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE mensualidad_id_mensualidad_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE mensualidad_id_mensualidad_seq OWNER TO postgres;

--
-- Name: mensualidad_id_mensualidad_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE mensualidad_id_mensualidad_seq OWNED BY mensualidad.id_mensualidad;


--
-- Name: parallax; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE parallax (
    id_parallax integer NOT NULL,
    id_gimnasio integer NOT NULL,
    img_parallax character varying(150) NOT NULL,
    title_parallax character varying(50) NOT NULL,
    desc_parallax character varying(700) NOT NULL,
    imginfe_parallax character varying(150) NOT NULL,
    enlace_video character varying(300)
);


ALTER TABLE parallax OWNER TO postgres;

--
-- Name: parallax_dos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE parallax_dos (
    id_parallaxdos integer NOT NULL,
    id_gimnasio integer NOT NULL,
    img_parallaxdos character varying(150) NOT NULL,
    costo_parallaxdos real NOT NULL,
    dias_parallaxtres character varying(20) NOT NULL,
    desc_parallaxdos character varying(80) NOT NULL
);


ALTER TABLE parallax_dos OWNER TO postgres;

--
-- Name: parallax_dos_id_parallaxdos_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE parallax_dos_id_parallaxdos_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE parallax_dos_id_parallaxdos_seq OWNER TO postgres;

--
-- Name: parallax_dos_id_parallaxdos_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE parallax_dos_id_parallaxdos_seq OWNED BY parallax_dos.id_parallaxdos;


--
-- Name: parallax_id_parallax_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE parallax_id_parallax_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE parallax_id_parallax_seq OWNER TO postgres;

--
-- Name: parallax_id_parallax_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE parallax_id_parallax_seq OWNED BY parallax.id_parallax;


--
-- Name: parallax_uno; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE parallax_uno (
    id_parallaxuno integer NOT NULL,
    id_gimnasio integer NOT NULL,
    back_parallaxuno character varying(150) NOT NULL,
    img_parallaxuno character varying(150) NOT NULL,
    title_parallaxuno character varying(50) NOT NULL,
    subt_parallaxuno character varying(50) NOT NULL
);


ALTER TABLE parallax_uno OWNER TO postgres;

--
-- Name: parallax_uno_id_parallaxuno_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE parallax_uno_id_parallaxuno_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE parallax_uno_id_parallaxuno_seq OWNER TO postgres;

--
-- Name: parallax_uno_id_parallaxuno_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE parallax_uno_id_parallaxuno_seq OWNED BY parallax_uno.id_parallaxuno;


--
-- Name: producto; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE producto (
    id_producto integer NOT NULL,
    id_gimnasio integer NOT NULL,
    nombre_prod character varying(30) NOT NULL,
    desc_prod character varying(500) NOT NULL,
    img_prod character varying(500) NOT NULL,
    precio_prod real
);


ALTER TABLE producto OWNER TO postgres;

--
-- Name: producto_id_producto_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE producto_id_producto_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE producto_id_producto_seq OWNER TO postgres;

--
-- Name: producto_id_producto_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE producto_id_producto_seq OWNED BY producto.id_producto;


--
-- Name: secretaria; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE secretaria (
    id_secretaria integer NOT NULL,
    id integer NOT NULL,
    id_administrador integer
);


ALTER TABLE secretaria OWNER TO postgres;

--
-- Name: secretaria_id_secretaria_seq_1_1; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE secretaria_id_secretaria_seq_1_1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE secretaria_id_secretaria_seq_1_1 OWNER TO postgres;

--
-- Name: secretaria_id_secretaria_seq_1_1; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE secretaria_id_secretaria_seq_1_1 OWNED BY secretaria.id_secretaria;


--
-- Name: seguimiento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE seguimiento (
    id_seguimiento integer NOT NULL,
    diametro_brazo real,
    fecha date NOT NULL,
    id_matricula integer NOT NULL
);


ALTER TABLE seguimiento OWNER TO postgres;

--
-- Name: seguimiento_id_seguimiento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE seguimiento_id_seguimiento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE seguimiento_id_seguimiento_seq OWNER TO postgres;

--
-- Name: seguimiento_id_seguimiento_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE seguimiento_id_seguimiento_seq OWNED BY seguimiento.id_seguimiento;


--
-- Name: thumbnail_kvstore; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE thumbnail_kvstore (
    key character varying(200) NOT NULL,
    value text NOT NULL
);


ALTER TABLE thumbnail_kvstore OWNER TO postgres;

--
-- Name: acerca_de id_acerca; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY acerca_de ALTER COLUMN id_acerca SET DEFAULT nextval('acerca_de_id_acerca_seq'::regclass);


--
-- Name: administrador id_administrador; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY administrador ALTER COLUMN id_administrador SET DEFAULT nextval('administrador_id_administrador_seq'::regclass);


--
-- Name: app_deos_deportistadestacado id_deportista_des; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY app_deos_deportistadestacado ALTER COLUMN id_deportista_des SET DEFAULT nextval('app_deos_deportistadestacado_id_deportista_des_seq'::regclass);


--
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group ALTER COLUMN id SET DEFAULT nextval('auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_permission ALTER COLUMN id SET DEFAULT nextval('auth_permission_id_seq'::regclass);


--
-- Name: auth_user id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user ALTER COLUMN id SET DEFAULT nextval('auth_user_id_seq'::regclass);


--
-- Name: auth_user_groups id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_groups ALTER COLUMN id SET DEFAULT nextval('auth_user_groups_id_seq'::regclass);


--
-- Name: auth_user_user_permissions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('auth_user_user_permissions_id_seq'::regclass);


--
-- Name: contacto id_contacto; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY contacto ALTER COLUMN id_contacto SET DEFAULT nextval('contacto_id_contacto_seq'::regclass);


--
-- Name: deportista id_deportista; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY deportista ALTER COLUMN id_deportista SET DEFAULT nextval('deportista_id_deportista_seq'::regclass);


--
-- Name: deportista_destacado id_deportista_des; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY deportista_destacado ALTER COLUMN id_deportista_des SET DEFAULT nextval('deportista_destacado_id_deportista_des_seq'::regclass);


--
-- Name: disciplina id_disciplina; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY disciplina ALTER COLUMN id_disciplina SET DEFAULT nextval('disciplina_id_disciplina_seq'::regclass);


--
-- Name: disciplina_instructor id_disciplina_horario; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY disciplina_instructor ALTER COLUMN id_disciplina_horario SET DEFAULT nextval('disciplina_instructor_id_disciplina_horario_seq_1'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_admin_log ALTER COLUMN id SET DEFAULT nextval('django_admin_log_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_content_type ALTER COLUMN id SET DEFAULT nextval('django_content_type_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_migrations ALTER COLUMN id SET DEFAULT nextval('django_migrations_id_seq'::regclass);


--
-- Name: email id_email; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY email ALTER COLUMN id_email SET DEFAULT nextval('email_id_email_seq'::regclass);


--
-- Name: galeria id_galeria; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY galeria ALTER COLUMN id_galeria SET DEFAULT nextval('galeria_id_galeria_seq'::regclass);


--
-- Name: gimnasio id_gimnasio; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY gimnasio ALTER COLUMN id_gimnasio SET DEFAULT nextval('gimnasio_id_gimnasio_seq_5'::regclass);


--
-- Name: horario id_horario; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY horario ALTER COLUMN id_horario SET DEFAULT nextval('horario_id_horario_seq_1_1'::regclass);


--
-- Name: inicio_slide id_inicioslide; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY inicio_slide ALTER COLUMN id_inicioslide SET DEFAULT nextval('inicio_slide_id_inicioslide_seq'::regclass);


--
-- Name: instructor id_instructor; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY instructor ALTER COLUMN id_instructor SET DEFAULT nextval('instructor_id_instructor_seq'::regclass);


--
-- Name: matricula id_matricula; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY matricula ALTER COLUMN id_matricula SET DEFAULT nextval('matricula_id_matricula_seq'::regclass);


--
-- Name: matricula_disciplina id_matriculadisciplina; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY matricula_disciplina ALTER COLUMN id_matriculadisciplina SET DEFAULT nextval('matricula_disciplina_id_matriculadisciplina_seq'::regclass);


--
-- Name: mensualidad id_mensualidad; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY mensualidad ALTER COLUMN id_mensualidad SET DEFAULT nextval('mensualidad_id_mensualidad_seq'::regclass);


--
-- Name: parallax id_parallax; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY parallax ALTER COLUMN id_parallax SET DEFAULT nextval('parallax_id_parallax_seq'::regclass);


--
-- Name: parallax_dos id_parallaxdos; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY parallax_dos ALTER COLUMN id_parallaxdos SET DEFAULT nextval('parallax_dos_id_parallaxdos_seq'::regclass);


--
-- Name: parallax_uno id_parallaxuno; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY parallax_uno ALTER COLUMN id_parallaxuno SET DEFAULT nextval('parallax_uno_id_parallaxuno_seq'::regclass);


--
-- Name: producto id_producto; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY producto ALTER COLUMN id_producto SET DEFAULT nextval('producto_id_producto_seq'::regclass);


--
-- Name: secretaria id_secretaria; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY secretaria ALTER COLUMN id_secretaria SET DEFAULT nextval('secretaria_id_secretaria_seq_1_1'::regclass);


--
-- Name: seguimiento id_seguimiento; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY seguimiento ALTER COLUMN id_seguimiento SET DEFAULT nextval('seguimiento_id_seguimiento_seq'::regclass);


--
-- Data for Name: acerca_de; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY acerca_de (id_acerca, id_gimnasio, titulo_acerca, desc_acerca, sidea_acerca, sidea1_acerca, sideb_acerca, sideb1_acerca, img_uno, img_dos) FROM stdin;
1	1	ACERCA DE NUESTRO CLUB DEPORTIVO DEOS	<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><img alt="" src="/static/media/uploads/2018/04/18/iimagen.jpg" style="height:305px; width:728px" /></p>\r\n\r\n<p style="margin-left:0cm; margin-right:0cm; text-align:justify">&nbsp;</p>\r\n\r\n<p style="text-align:justify">El club deportivo &ldquo;DEOS&rdquo; se caracteriza por su gran desempe&ntilde;o institucional que ha tenido durante m&aacute;s de 15 a&ntilde;os de vida hist&oacute;rica, que con&nbsp;esfuerzo y capacidad administrativa ha logrado ser uno de los mejores clubes deportivos sobresalientes de la localidad e incluso llegar a participar a nivel local y nacional en diferentes disciplinas.</p>\r\n\r\n<p style="text-align:justify">En la actualidad se encuentra ubicado en las calles Lauro Guerrero entre Miguel Riofrio y Azuay contando con un amplio espacio para realizar sus diferentes actividades f&iacute;sicas en coordinaci&oacute;n con los entrenadores y el gerente propietario del club deportivo.&nbsp;Desde que se abri&oacute; las puertas se ha venido contratando y actualizando conocimientos con el personal que labora en el club deportivo, en base a esta eficiente capacidad de entrenamiento ha logrado t&iacute;tulos de entrenador internacional IFBB de f&iacute;sico culturismo, Instructor Nacional de Aer&oacute;bicos y Bailoterapia, entrenador nacional de TAE KWONDO adem&aacute;s de ser juez Nacional en F&iacute;sico Culturismo, y &aacute;rbitro nacional en TAE KWON DO.</p>\r\n\r\n<p style="text-align:justify">En &aacute;reas como TAE KWON DO y F&iacute;sico Culturismo en varias ocasiones y a nivel nacional ha venido trabajando con distintos clientes siendo as&iacute; uno de los mejores clubes deportivos logrando los siguientes resultados.</p>\r\n\r\n<p style="text-align:justify">Tae kwon Do:</p>\r\n\r\n<ul>\r\n\t<li style="text-align: justify;">Campeones nacionales oficiales de Tae Kwon Do.</li>\r\n\t<li style="text-align: justify;">Campeones juegos nacionales de Tae Kwon Do.</li>\r\n\t<li style="text-align: justify;">Campeones por equipos en varios torneos de car&aacute;cter abierto.</li>\r\n\t<li style="text-align: justify;">Medallistas provinciales y nacionales en las categor&iacute;as infantil, menores, pre juvenil, juvenil y adultos.</li>\r\n</ul>\r\n\r\n<p style="text-align:justify">F&iacute;sico Culturismo:</p>\r\n\r\n<ul>\r\n\t<li style="text-align: justify;">Campeones por equipos en la categor&iacute;a novatos y clasificados de f&iacute;sico culturismo por m&aacute;s de 4 ocasiones.</li>\r\n\t<li style="text-align: justify;">Seleccionados nacionales en f&iacute;sico culturismo.</li>\r\n\t<li style="text-align: justify;">Medallistas de plata en eventos internacionales oficiales como lo es el sudamericano de f&iacute;sico culturismo.</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>	Impulsar y formar deportistas con el más alto rendimiento, con la finalidad de promover en la sociedad la actividad física como parte de nuestras rutinas diarias, que se logra a través de un arduo entrenamiento, nutrición y actividad física que se imparte dentro de nuestro establecimiento.	Ser una de los mejores clubes deportivos a nivel local y nacional brindando servicios fitness posicionándonos como un club deportivo de marca que contribuye al desarrollo de  hábitos deportivos saludables.	<ul>\r\n\t<li><strong>Responsabilidad </strong></li>\r\n\t<li><strong>Disciplina </strong></li>\r\n\t<li><strong>Respeto</strong></li>\r\n\t<li><strong>Perseverancia</strong></li>\r\n</ul>	<ul>\r\n\t<li><strong>Quito&nbsp;</strong></li>\r\n\t<li><strong>&nbsp;Machala</strong></li>\r\n\t<li><strong>&nbsp;Cuenca</strong></li>\r\n\t<li><strong>&nbsp;Guayaquil</strong></li>\r\n</ul>		
\.


--
-- Name: acerca_de_id_acerca_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('acerca_de_id_acerca_seq', 1, true);


--
-- Data for Name: administrador; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY administrador (id_administrador, id) FROM stdin;
1	3
\.


--
-- Name: administrador_id_administrador_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('administrador_id_administrador_seq', 1, true);


--
-- Data for Name: app_deos_deportistadestacado; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY app_deos_deportistadestacado (id_deportista_des, nombre, apellidos, descripcion, nivel, disciplina) FROM stdin;
\.


--
-- Name: app_deos_deportistadestacado_id_deportista_des_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('app_deos_deportistadestacado_id_deportista_des_seq', 1, false);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_group (id, name) FROM stdin;
1	Administrador
2	Usuarios
4	SuperUsuarios
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_group_id_seq', 4, true);


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_group_permissions (id, group_id, permission_id) FROM stdin;
3	1	115
6	1	106
7	1	100
8	2	52
9	2	53
10	2	54
11	4	1
12	4	2
13	4	3
14	4	4
15	4	5
16	4	6
17	4	7
18	4	8
19	4	9
20	4	10
21	4	11
22	4	12
23	4	13
24	4	14
25	4	15
26	4	16
27	4	17
28	4	18
29	4	19
30	4	20
31	4	21
32	4	22
33	4	23
34	4	24
35	4	25
36	4	26
37	4	27
38	4	28
39	4	29
40	4	30
41	4	31
42	4	32
43	4	33
44	4	34
45	4	35
46	4	36
47	4	37
48	4	38
49	4	39
50	4	40
51	4	41
52	4	42
53	4	43
54	4	44
55	4	45
56	4	46
57	4	47
58	4	48
59	4	49
60	4	50
61	4	51
62	4	52
63	4	53
64	4	54
65	4	55
66	4	56
67	4	57
68	4	58
69	4	59
70	4	60
71	4	61
72	4	62
73	4	63
74	4	64
75	4	65
76	4	66
77	4	67
78	4	68
79	4	69
80	4	70
81	4	71
82	4	72
83	4	73
84	4	74
85	4	75
86	4	76
87	4	77
88	4	78
89	4	79
90	4	80
91	4	81
92	4	82
93	4	83
94	4	84
95	4	85
96	4	86
97	4	87
98	4	88
99	4	89
100	4	90
101	4	91
102	4	92
103	4	93
104	4	94
105	4	95
106	4	96
107	4	97
108	4	98
109	4	99
110	4	100
111	4	101
112	4	102
113	4	103
114	4	104
115	4	105
116	4	106
117	4	107
118	4	108
119	4	109
120	4	110
121	4	111
122	4	112
123	4	113
124	4	114
125	4	115
126	4	116
127	4	117
128	4	118
129	4	119
130	4	120
131	4	121
132	4	122
133	4	123
\.


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_group_permissions_id_seq', 133, true);


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can add permission	2	add_permission
5	Can change permission	2	change_permission
6	Can delete permission	2	delete_permission
7	Can add group	3	add_group
8	Can change group	3	change_group
9	Can delete group	3	delete_group
10	Can add user	4	add_user
11	Can change user	4	change_user
12	Can delete user	4	delete_user
13	Can add content type	5	add_contenttype
14	Can change content type	5	change_contenttype
15	Can delete content type	5	delete_contenttype
16	Can add session	6	add_session
17	Can change session	6	change_session
18	Can delete session	6	delete_session
19	Can add auth group	7	add_authgroup
20	Can change auth group	7	change_authgroup
21	Can delete auth group	7	delete_authgroup
22	Can add email	8	add_email
23	Can change email	8	change_email
24	Can delete email	8	delete_email
25	Can add django session	9	add_djangosession
26	Can change django session	9	change_djangosession
27	Can delete django session	9	delete_djangosession
28	Can add horario	10	add_horario
29	Can change horario	10	change_horario
30	Can delete horario	10	delete_horario
31	Can add django migrations	11	add_djangomigrations
32	Can change django migrations	11	change_djangomigrations
33	Can delete django migrations	11	delete_djangomigrations
34	Can add parallax uno	12	add_parallaxuno
35	Can change parallax uno	12	change_parallaxuno
36	Can delete parallax uno	12	delete_parallaxuno
37	Can add acerca de	13	add_acercade
38	Can change acerca de	13	change_acercade
39	Can delete acerca de	13	delete_acercade
40	Can add disciplina instructor	14	add_disciplinainstructor
41	Can change disciplina instructor	14	change_disciplinainstructor
42	Can delete disciplina instructor	14	delete_disciplinainstructor
43	Can add auth permission	15	add_authpermission
44	Can change auth permission	15	change_authpermission
45	Can delete auth permission	15	delete_authpermission
46	Can add preinscripcion	16	add_preinscripcion
47	Can change preinscripcion	16	change_preinscripcion
48	Can delete preinscripcion	16	delete_preinscripcion
49	Can add secretaria	17	add_secretaria
50	Can change secretaria	17	change_secretaria
51	Can delete secretaria	17	delete_secretaria
52	Can add deportista	18	add_deportista
53	Can change deportista	18	change_deportista
54	Can delete deportista	18	delete_deportista
55	Can add parallax	19	add_parallax
56	Can change parallax	19	change_parallax
57	Can delete parallax	19	delete_parallax
58	Can add auth user user permissions	20	add_authuseruserpermissions
59	Can change auth user user permissions	20	change_authuseruserpermissions
60	Can delete auth user user permissions	20	delete_authuseruserpermissions
61	Can add producto	21	add_producto
62	Can change producto	21	change_producto
63	Can delete producto	21	delete_producto
64	Can add disciplina horario	22	add_disciplinahorario
65	Can change disciplina horario	22	change_disciplinahorario
66	Can delete disciplina horario	22	delete_disciplinahorario
67	Can add disciplina preinscripcion	23	add_disciplinapreinscripcion
68	Can change disciplina preinscripcion	23	change_disciplinapreinscripcion
69	Can delete disciplina preinscripcion	23	delete_disciplinapreinscripcion
70	Can add inicio slide	24	add_inicioslide
71	Can change inicio slide	24	change_inicioslide
72	Can delete inicio slide	24	delete_inicioslide
73	Can add contacto	25	add_contacto
74	Can change contacto	25	change_contacto
75	Can delete contacto	25	delete_contacto
76	Can add auth group permissions	26	add_authgrouppermissions
77	Can change auth group permissions	26	change_authgrouppermissions
78	Can delete auth group permissions	26	delete_authgrouppermissions
79	Can add auth user	27	add_authuser
80	Can change auth user	27	change_authuser
81	Can delete auth user	27	delete_authuser
82	Can add gimnasio	28	add_gimnasio
83	Can change gimnasio	28	change_gimnasio
84	Can delete gimnasio	28	delete_gimnasio
85	Can add auth user groups	29	add_authusergroups
86	Can change auth user groups	29	change_authusergroups
87	Can delete auth user groups	29	delete_authusergroups
88	Can add administrador	30	add_administrador
89	Can change administrador	30	change_administrador
90	Can delete administrador	30	delete_administrador
91	Can add django content type	31	add_djangocontenttype
92	Can change django content type	31	change_djangocontenttype
93	Can delete django content type	31	delete_djangocontenttype
94	Can add parallax dos	32	add_parallaxdos
95	Can change parallax dos	32	change_parallaxdos
96	Can delete parallax dos	32	delete_parallaxdos
97	Can add disciplina	33	add_disciplina
98	Can change disciplina	33	change_disciplina
99	Can delete disciplina	33	delete_disciplina
100	Can add instructor	34	add_instructor
101	Can change instructor	34	change_instructor
102	Can delete instructor	34	delete_instructor
103	Can add django admin log	35	add_djangoadminlog
104	Can change django admin log	35	change_djangoadminlog
105	Can delete django admin log	35	delete_djangoadminlog
106	Can add matricula	36	add_matricula
107	Can change matricula	36	change_matricula
108	Can delete matricula	36	delete_matricula
109	Can add galeria	37	add_galeria
110	Can change galeria	37	change_galeria
111	Can delete galeria	37	delete_galeria
112	Can add matricula disciplina	38	add_matriculadisciplina
113	Can change matricula disciplina	38	change_matriculadisciplina
114	Can delete matricula disciplina	38	delete_matriculadisciplina
115	Can add mensualidad	39	add_mensualidad
116	Can change mensualidad	39	change_mensualidad
117	Can delete mensualidad	39	delete_mensualidad
118	Can add seguimiento	40	add_seguimiento
119	Can change seguimiento	40	change_seguimiento
120	Can delete seguimiento	40	delete_seguimiento
121	Can add kv store	41	add_kvstore
122	Can change kv store	41	change_kvstore
123	Can delete kv store	41	delete_kvstore
124	Can add deportista destacado	42	add_deportistadestacado
125	Can change deportista destacado	42	change_deportistadestacado
126	Can delete deportista destacado	42	delete_deportistadestacado
\.


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_permission_id_seq', 126, true);


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined, dni, genero, celular, img_perfil, edad, telefono, es_administrador, es_deportista, es_instructor, es_secretaria, direccion) FROM stdin;
2	Angelc1997	2018-04-14 13:44:51.472-05	f	secretaria1	secretaria1	Lalangui	lore25@yahoo.es	t	t	2018-04-14 13:44:51.472-05	1106782340	Femenino	0982787093		56	0725693243	f	f	f	t	\N
3	Angelc1997	2018-04-14 14:00:47.803-05	f	administrador1	administrador1	Lalangui	miguel12@yahoo.es	f	f	2018-04-14 14:00:47.803-05	1111111111	Masculino	0982787093		3 	\N	t	f	f	f	\N
4	Angelc1997	2018-04-14 14:04:48.405-05	f	secretaria2	secretaria2	Lalangui	lore25@yahoo.es	f	f	2018-04-14 14:04:48.406-05	1106782333	Femenino	0982787093		4 	\N	f	f	f	t	\N
5	wsddw	2018-04-14 14:05:37.502-05	f	secretaria3	secretaria2	Lalangui	lore25@yahoo.es	f	f	2018-04-14 14:05:37.502-05	1106782335	Femenino	0982787093		4 	\N	f	f	f	t	\N
23	Angelc1997	2018-04-16 08:54:52.057-05	f	rodrigo	Rodrigo	Lievanas	miguelcapa20@gmail.com	t	t	2018-04-16 08:54:52.057-05	1111111122	Masculino	0983625478		2 	0725693243	\N	\N	\N	\N	Quito
24		2018-04-16 08:54:52.124-05	f					t	t	2018-04-16 08:54:52.124-05	          		          		  	\N	\N	\N	\N	\N	
7	Angelc1997	2018-04-14 14:31:32.455-05	f	instructor1	instructor01	Lalangui	lore25@yahoo.es	f	t	2018-04-14 14:31:32.455-05	1106782454	Masculino	0982344566		2 	0725693243	f	f	t	f	\N
9	Angelc1997|	2018-04-14 14:48:30.251-05	f	instructor03	instructor03	Lalangui	miguel12@yahoo.es	f	t	2018-04-14 14:48:30.251-05	1109876899	Masculino	0983625478		2 	0725693243	f	f	t	f	\N
27	Angelc1997	2018-04-16 09:34:52.064-05	f	tesis	Miguel	Ruiz	miguel12@yahoo.es	t	t	2018-04-16 09:34:52.064-05	1106782343	Masculino	0987632523		34	0725693243	\N	\N	\N	\N	Loja - San Sebastian
29	Angelc1997	2018-04-16 09:45:09.839-05	f	deportista22	Rodrigo	Lalangui	miguel12@yahoo.es	t	t	2018-04-16 09:45:09.839-05	1109876542	Masculino	0982344566		13	0725693243	\N	\N	\N	\N	PItas
16	Angelc1997	2018-04-14 21:58:32.426-05	f	prematricula1	Prematricula1	Lalangui	miguelcapa20@gmail.com	t	t	2018-04-14 21:58:32.426-05	1106782366	Masculino	0982344566		11	0725693243	\N	\N	\N	\N	\N
17	Angelc1997	2018-04-14 21:59:35.062-05	f	prematricula2	Prematricula2	Lalangui	miguelcapa20@gmail.com	t	t	2018-04-14 21:59:35.062-05	1106782344	Masculino	0982344566		11	0725693243	\N	\N	\N	\N	\N
14	123456789	2018-04-14 19:07:01.566-05	f	deportista04	Jose	Lalangui	jose25@yahoo.es	f	f	2018-04-14 19:07:01.567-05	1106782323	Masculino	0982344566		56	\N	f	t	f	f	Loja
18	Angelc1997	2018-04-15 16:48:17.687-05	f	deportista13	preinscripcion10	Ruiz	migwe0@gmail.com	t	t	2018-04-15 16:48:17.687-05	1111111188	Masculino	0982787093		2 	0725693243	\N	\N	\N	\N	Guayaquil
19	pbkdf2_sha256$36000$o2sauMeiMdZs$bKa+x4DUKIdI/0aukzsYUnSMV7HqLBjfg48e6Ib/2oU=	2018-04-17 16:57:25.309-05	f	deportiusta03	Jose	Cuenca	miguel12@yahoo.es	f	t	2018-04-15 20:06:51.928-05	\N	\N	\N	\N	\N	\N	\N	t	\N	\N	\N
13	Angelc1997	2018-04-14 14:56:23.163-05	f	deportista02	deportista02	Lalangui	miguel12@yahoo.es	f	t	2018-04-14 14:56:23.163-05	1111111199	Masculino	0982344566		8 	0725693243	f	t	f	f	Loja
20	Angelc1997	2018-04-16 00:44:32.097-05	f	secretaria44	secretaria3	Lievanas	secre25@yahoo.es	t	t	2018-04-16 00:44:32.097-05	1109876522	Femenino	0982787093		3 	0725693243	f	f	f	t	Loja - San Sebastian
21	Angelc1997	2018-04-16 00:55:41.188-05	f	usuario9	usuario22	Lievanas	miguelcapa25@yahoo.es	t	t	2018-04-16 00:55:41.188-05	1111111333	Masculino	0983625478		3 	0725693243	f	f	f	t	Loja
22	Angelc1997	2018-04-16 00:57:31.587-05	f	usuario10	usuario22	Lievanas	miguelcapa25@yahoo.es	t	t	2018-04-16 00:57:31.587-05	1111111332	Masculino	0983625478		3 	0725693243	f	f	f	t	Loja
33	pbkdf2_sha256$36000$WY7r3tAYzDtB$TQmDFYV2iWSycsY8Yo/KIkHR09bJQyfV4e0wqOkrJKQ=	2018-04-16 11:26:07.511-05	f	Deportista2	Rodrigo	Lalangui	miguel12@yahoo.es	f	t	2018-04-16 11:13:12.598-05	\N	\N	\N	\N	\N	\N	\N	t	\N	\N	\N
37	pbkdf2_sha256$36000$4X5sP6OnbPOY$hqGyeok5cxyh7vnwIjs+Rn1iI7rdcxLIHGL4COuZCjM=	\N	f	Deportista3	Deportista2	Lievanas	miguel12@yahoo.es	f	t	2018-04-16 16:11:20.378-05	\N	\N	\N	\N	\N	\N	\N	t	\N	\N	\N
30	Angelc1997	2018-04-16 10:51:59.553-05	f	deportista100	Jose	Lievanas	miguelcapa20@gmail.com	t	t	2018-04-16 10:51:59.553-05	1111111455	Masculino	0982787093		2 	0725693243	\N	\N	\N	\N	PItas
31	Angelc1997	2018-04-16 11:07:21.413-05	f	deportista1001	Rodrigo	Ruiz	miguel12@yahoo.es	t	t	2018-04-16 11:07:21.413-05	1109873333	Masculino	0982643564		2 	0725693243	f	t	f	f	Loja
45	pbkdf2_sha256$36000$YKqIKZO3nWSN$HySmHIJY3c9Ce7Ruq6DHLqml/N6Qx83ecqmsPYge/c8=	\N	f	migueocampo1	Miguel	Ocampo	migueocampo@gmail.com	f	t	2018-04-23 19:02:00.546293-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
46	pbkdf2_sha256$36000$F8g7RDTDpGlu$k3lx8DezBgcIgDaT3QNsvbeP7lgtbyZ1iU0y3N7rl8Q=	\N	f	migueocampo2	Miguel	Ocampo	migueocampo@gmail.com	f	t	2018-04-23 19:04:42.630077-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
42	pbkdf2_sha256$36000$8x2T7rIUsUlI$L/66niIpTPlcLdA5mCUyq6QpHKGEO8Q/n+4bMHJcYeY=	\N	t	jonacastro				t	t	2018-04-19 12:04:13.619161-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
47	pbkdf2_sha256$36000$ZvqFp6xB82ZC$aqCE+LNpOa4T8g1unWFR9Jt37RLvNbgNAUr9U655Zac=	\N	f	migueocampo3	Miguel	Ocampo	migueocampo@gmail.com	f	t	2018-04-23 19:06:57.589907-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
48	pbkdf2_sha256$36000$5RhJcKjrdMbv$8PVM31mZ9hMrC+RlA/DOftI1AmCrMVCYGQyDkAgnY90=	\N	f	migueocampo4	Miguel	Ocampo	migueocampo@gmail.com	f	t	2018-04-23 19:07:57.248501-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
49	pbkdf2_sha256$36000$OXy4rEZ8E2ju$vWmEKAVVbHMwmMyJW1o+PPCwZ13DOrGIbEbs/macXZw=	\N	f	migueocampo5	Miguel	Ocampo	migueocampo@gmail.com	f	t	2018-04-23 19:09:04.942357-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
41	pbkdf2_sha256$36000$ZWEXOPIxURdl$Jo8tFR+3r6WOJuM7EC6x/kQCRxeIW6Mf/jfy0cSTRYk=	2018-04-20 06:19:17.623371-05	f	jacastro12	Jonathan	Castro	jonacastro@gmail.com	f	t	2018-04-19 11:33:32.169702-05	\N	\N	\N	\N	\N	\N	\N	t	\N	\N	\N
50	pbkdf2_sha256$36000$QKUiWDe8cGFg$qTsHe1wYgJOmO3GCPFrUAAHucxxgAsuzCcBJtHK6VkA=	\N	f	migueocampo6	Miguel	Ocampo	migueocampo@gmail.com	f	t	2018-04-23 19:10:08.514821-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
51	pbkdf2_sha256$36000$a9RgoOV6qNkp$2XmPVF5toW7XjJEKxeXTuK9sGvZYG/F34Gt4OtY+XDI=	\N	f	migueocampo7	Miguel	Ocampo	migueocampo@gmail.com	f	t	2018-04-23 19:10:54.439443-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
52	pbkdf2_sha256$36000$GJ42hLRghtAi$Nf1FxKX3G+6aQqcXVkAyvMwwGRDlPXG4S+hKxtLHj2g=	\N	f	migueocampo8	Miguel	Ocampo	migueocampo@gmail.com	f	t	2018-04-23 19:11:15.515133-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
44	pbkdf2_sha256$36000$4B14634TbFpZ$1f8e7graYCP1N/lOcSjv1e8brBxqy/BRvZ2xV+WrdWs=	\N	f	migueocampo	Miguel	Ocampo	migueocampo@gmail.com	f	t	2018-04-23 19:01:04.357101-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
53	pbkdf2_sha256$36000$shBXmeQctyQv$PBTHwPmTaz5HFERvqdIvAp5wOdO+8DFwLv9NzssFxCQ=	2018-04-23 19:15:45.059308-05	f	migueocampo9	Miguel	Ocampo	migueocampo@gmail.com	f	t	2018-04-23 19:15:44.788388-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
54	pbkdf2_sha256$36000$ByDd7W5SQXCW$WMGPjy6bnA/GwLF4vcDs+3AJh7720i08oBRAGeY4LAs=	2018-04-23 19:19:02.401756-05	f	jona	Jonathan	Castro	asdf@solnustec.com	f	t	2018-04-23 19:19:02.304024-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
55	pbkdf2_sha256$36000$JPVDkWTiV7Af$f2QGdNI9CvGQtXZQSYw0GbquIs2DF23F+N6tj9giRXA=	2018-04-23 19:19:37.93358-05	f	nelson	nelson	agurto	dfas@gmail.com	f	t	2018-04-23 19:19:37.773673-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
56	pbkdf2_sha256$36000$ignoc9FdtKwA$0qej2oJP8wh5fAxlZhqi0T9hqRuAnuuVDrr++ifHMBg=	2018-04-23 19:34:09.574331-05	f	nelson2	nelson2	nelson2	nelson2@gmail.com	f	t	2018-04-23 19:34:09.489747-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
43	pbkdf2_sha256$36000$ZGUuDH3uZECA$xnWT4xzL7sG0G3RO8aYi4Cg47TgNgpPXabYxKVFgV+k=	2018-05-04 18:59:54.51-05	t	jacastro				t	t	2018-04-19 12:06:23.182177-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
39	Angelc1997	2018-04-17 10:08:48.467-05	f	DEPORTISTA34	Carlos	Jerves	miguel12@yahoo.es	t	t	2018-04-17 10:08:48.467-05	1109876588	Masculino	0982643564		43	\N	f	t	f	f	Loja
8	123	2018-04-14 14:32:53.643-05	f	instructor02	instructor02	Lalangui	lore25@yahoo.es	f	t	2018-04-14 14:32:53.643-05	1106782457	Masculino	0982344566	usuario/2018/04/30/blogs-daily-details-01-personal-trainer-main.jpg	34	0725693243	f	f	t	f	Quito
32	Angelc1997	2018-04-16 11:09:55.454-05	f	secretaria6	Ale	Lievanas	miguel12@yahoo.es	t	t	2018-04-16 11:09:55.454-05	1106782322	Femenino	0983625478	usuario/2018/04/30/6197.jpg	34	\N	f	f	f	t	Guayaquil
6	Angelc1997	2018-04-14 14:09:16.298-05	f	secretaria4	secretaria2	Lalangui	lore25@yahoo.es	f	f	2018-04-14 14:09:16.299-05	1106782354	Femenino	0982787093	usuario/2018/04/30/454439505.jpg	23	\N	f	f	f	t	Loja
57	pbkdf2_sha256$36000$djrPUTHzPpCn$G5peXnDSRIeoYJgaS4eN1Eboq/ijat0BwLoaP6dAq1I=	2018-04-23 19:35:51.274726-05	f	nelson3	nelson2	nelson2	nelson2@gmail.com	f	t	2018-04-23 19:35:51.16421-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
58	pbkdf2_sha256$36000$d0S6Kotip0QH$xLVWjdc8VszGfK0hHVDz7JL3oVg8oRpBDeFgRUGEBHE=	2018-04-23 19:36:19.889119-05	f	nelson4	nelson2	nelson2	nelson2@gmail.com	f	t	2018-04-23 19:36:19.733676-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
59	pbkdf2_sha256$36000$Vngks1hW4Bl0$vdD5J80Q/mej3kTOo0xiYLgM/ukreW7C4RCOEHBxpgE=	2018-04-24 12:46:49.384335-05	f	jonath	Jonathan	Castro	mail@gmail.com	f	t	2018-04-24 12:46:49.070471-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
61	pbkdf2_sha256$36000$WrA8pTuZ5YxG$LJ1tyNy8SXQbt/gPOZ3TUsYkr57vF6kW2inJCLLenoc=	\N	f	kjñl	Mario	Anibal	jk	f	t	2018-04-24 12:49:05.887311-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
62	pbkdf2_sha256$36000$jHiHulNXUWcq$NQOjKetzxtNIdpqdsSrbJGxOcwroM++s6BGWu4H39GQ=	\N	f	jsjsj	lololo	lololo	jssj	f	t	2018-04-24 12:50:44.901393-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
64	pbkdf2_sha256$36000$r7rA8OhqUCOV$raD3ZmPrTE8m+CkwoH7/KBW8uzq18/uSWON/iKr/fto=	\N	f	scasa	Juanito	Cazares	dfasd@gmail.coma	f	t	2018-04-24 12:56:20.499858-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
66	pbkdf2_sha256$36000$U7r9opbPGvCi$WhG044x9GIq+Znorp5OfYA6I4qhCauFCcT95rf4+r7U=	2018-04-24 12:57:26.97721-05	f	afasdf	juniaj	sadf	afsdf	f	t	2018-04-24 12:57:26.831572-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
67	pbkdf2_sha256$36000$gntyfgRUmLW8$+2ODQ5/rDNG6aNgfVyepQIWo3wfs6fbR3LrV1T1TWFA=	\N	f	enrique	Manuel	Enrique	mail@gmail.com	f	t	2018-04-24 13:02:45.760742-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
68	pbkdf2_sha256$36000$l2brRHLCcPQm$5dOZ91NoyN0oIZ5ELEwymRpROLlGluic2UNK91ak7bA=	\N	f	enrique2	Manuel	Enrique	mail@gmail.com	f	t	2018-04-24 13:03:28.286342-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
69	pbkdf2_sha256$36000$BtfnGxgbrJWK$Zt4s2BgvA4A2YYjMdZjd8MW6Ww5HsPtwewduG2Moo6s=	\N	f	manuel	Manuelito	Jimenez	manuel@gmail.com	f	t	2018-04-24 13:04:49.951741-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
70	pbkdf2_sha256$36000$k4vaaL8Fl4nS$YhmQcms9SpJSvzORQ0/PnuxXbuj12stelT6jEZoSPII=	\N	f	manuel1	Manuelito	Jimenez	manuel@gmail.com	f	t	2018-04-24 13:05:03.410079-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
72	pbkdf2_sha256$36000$rQM6eFdR6enI$su29ecduvoa7EMFWPcA4MMcoIULCRgYXPTFV5LtTvJA=	2018-04-24 13:11:41.682024-05	f	anibal	Anibal	Gonzalez	asdf@gmail.com	f	t	2018-04-24 13:11:41.51054-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
74	pbkdf2_sha256$36000$Ww4BVx3CFIQb$V0brNWw3rdtFmeMmtc+jwC1xcZu5tzJN/IJUUv7pouY=	\N	f	anita	Anita	Mariana	asdfa@gmail.com	f	t	2018-04-24 13:18:06.955874-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
87	pbkdf2_sha256$36000$LAWIy9FDM58U$oWx5LpOzdkkVQBwSmmqwHQ7Q5S04bTGkq7jVjiOy5j8=	2018-04-28 15:47:43.703-05	f	jaui	Jaquelyn	Carrilo	miguel12@yahoo.es	f	t	2018-04-28 15:47:43.6-05	1106667543	Femenino	0982787093	\N	2 	0725693243	\N	\N	\N	\N	Quito
76	pbkdf2_sha256$36000$JA72c6OCGbqN$WlW4ZI0N1qAyf5bjaQWiqgL1w/WR6RT72GS3wSfkrJQ=	2018-04-26 18:53:47.751-05	f	david	Dvid Alexander	Robles	miguel12@yahoo.es	f	t	2018-04-26 18:53:47.627-05	1102223334	Masculino	0982344566	\N	2 	0725693243	\N	\N	\N	\N	Loja
84	pbkdf2_sha256$36000$3tjtQNl4Xz0Y$8+gZrm9hG4lfrVJXXEwZ8OU185tQZBdJAfOKcCfdvIs=	2018-04-28 15:40:52.228-05	f	joel	Joel	Arroyo	miguel12@yahoo.es	f	t	2018-04-28 15:40:52.134-05	1106667543	Masculino	0982344566	\N	2 	0|        	\N	\N	\N	\N	Loja
77	pbkdf2_sha256$36000$i6E5nqmJK8Kd$H1maWH4rTERIQZIyBHwGY7XQPRHm01E97eR3CbCpotA=	2018-04-26 19:05:12.195-05	f	maria	Maria	GHA	miguel12@yahoo.es	f	t	2018-04-26 19:04:36.003-05	1106667543	Femenino	0982344566	\N	34	0725693243	\N	\N	\N	\N	Loja
78	pbkdf2_sha256$36000$BKY2SDaKZLyT$qYS9QhJuElafQgODGHrtSObbdjdeZ9pAgEfDi/UgYFg=	2018-04-26 19:13:18.159-05	f	luis	Luis	Martinez	miguel12@yahoo.es	f	t	2018-04-26 19:13:18.063-05	1106667543	Masculino	0983625478	\N	14	0725693243	\N	\N	\N	\N	Loja
92	pbkdf2_sha256$36000$BkmOmeEm7khm$Ui17f8fCNYkMc/WmoFwGNipuo7cZNiPgLbsmsTpvcts=	2018-05-05 13:59:22.418-05	f	miguel				f	t	2018-04-30 15:38:47.897-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
82	pbkdf2_sha256$36000$6A6iX5l9naqy$wxiv5FHei8q97GisRG9wedZFnojVAc8If1+nfk53dHo=	2018-04-27 18:18:15.303-05	f	carlos	Carlos12	fdgdf	miguel12@yahoo.es	f	t	2018-04-27 18:18:15.199-05	1102223334	Masculino	0982344566	\N	34	0725693243	\N	\N	\N	\N	Loja
90	pbkdf2_sha256$36000$5x9GE8NBLaOt$e8eX6pVZg7PHObtT5epvpJ4zXhla76ngxB6+uJP+G0A=	2018-04-30 16:26:41.227-05	f	alberto	Alberto Joaquin	Maza	miguel12@yahoo.es	f	f	2018-04-29 15:24:36.348-05	1106667544	Masculino	0982344566	usuario/2018/04/30/post-blog-ventakas-aloe-vera-deportistas-1.png	23	\N	f	f	f	f	Loja - San Sebastian
85	pbkdf2_sha256$36000$xZhQZk8FIBkL$Ch1wJ1ggNQCyMZ+00MZk1joeT9wvjGKOwGZS/KCKSog=	2018-04-28 15:43:06.818-05	f	joel12	Joel 	Ayora	miguelcapa25@yahoo.es	f	t	2018-04-28 15:43:06.726-05	1105557222	Masculino	0982344566	\N	14	973243    	\N	\N	\N	\N	Loja
79	pbkdf2_sha256$36000$Zr34avGQbvlH$hH5nrxWs7tTmmzId55PCdhHOeHeul/FHnSyufOqY3dA=	2018-04-27 10:03:40.937-05	f	carlos1	Carlosdd	Rivas	miguel12@yahoo.es	f	t	2018-04-27 10:03:40.361-05	1106667543	Masculino	0982344566	\N	34	0725693243	\N	\N	\N	\N	Loja
88	pbkdf2_sha256$36000$4GYZNprdOZOA$LPG3F3bc4s16ZPc/Km5nopfn4C24FZN5g+DR6Wz5SEE=	\N	f	andrea	ANDREA	Calva	miguel12@yahoo.es	f	t	2018-04-28 22:37:57.277-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
86	pbkdf2_sha256$36000$3l3zI2ngSz0z$7BS8EiMArn1xGe67Jre9HYaoBPJmTfvj1giIGvw3a4M=	2018-04-28 15:45:35.993-05	f	ander	Anderson	Perez	miguel12@yahoo.es	f	t	2018-04-28 15:45:35.921-05	1123456789	Masculino	0982344566	\N	3 	0725693243	\N	\N	\N	\N	Loja
91	pbkdf2_sha256$36000$rPTI0I8K8aay$8sPszK58hGjVJ+oTGXuid2FYR0KtmRlXLycYdBzm0c4=	2018-04-29 15:30:27.541-05	f	jose124	Jose	Alvarado	miguel12@yahoo.es	f	t	2018-04-29 15:30:27.469-05	1102223334	Masculino	0982787093	\N	14	0725693243	\N	\N	\N	\N	Loja - San Sebastian
81	lennin	2018-04-27 17:57:06.539-05	f	lennin12	Lenin Manuel	Jimenez Celi	aikijudolenin@hotmail.com	t	t	2018-04-27 17:57:06.539-05	1104906597	Masculino	0979927074	usuario/2018/04/30/aid101070-v4-728px-Become-a-Personal-Trainer-Step-1_8BvPvvP.jpg	26	\N	f	f	t	f	Loja, Mercadillo y Bolivar
83	pbkdf2_sha256$36000$Dth1XVS0PiXx$Vp5QzW+EBdbvd2vtRQEvpzFIhwabq98UlvqUr15115w=	2018-04-28 15:39:39.213-05	f	jorge	Jorge	Vivanco	miguelcapa20@gmail.com	f	t	2018-04-28 15:16:53.075-05	1106667543	Masculino	0982344566	\N	1 	0725693243	\N	\N	\N	\N	Loja
89	pbkdf2_sha256$36000$LPUZRudDcB3e$mRKAtuSOpUJgMGnM3raNkpc21ifJywZa+/yLNfoE3Yc=	2018-04-28 22:40:02.006-05	f	ds	sdca	sc	cdc	f	t	2018-04-28 22:40:01.897-05	1102223334	Masculino	2343      	\N	12	31234     	\N	\N	\N	\N	wdc
80	Angelc1997	2018-04-27 15:29:39.521-05	f	alecis	Alexis	Rodriguez	miguel12@yahoo.es	f	t	2018-04-27 15:20:05.109-05	1102223321	Masculino	0982344566	usuario/2018/04/30/post-blog-ventakas-aloe-vera-deportistas-1_3198S1e.png	23	0725693243	f	f	f	f	Loja
93	pbkdf2_sha256$36000$OTXVA4DIodkc$WZxc8Nx6TXtcuwDqOvTXTDD19BA1KnFhIfcflFSwjTo=	\N	f	angell				f	t	2018-04-30 15:56:04-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
94	pbkdf2_sha256$36000$3wc1wtDBTaF6$Ne7M9tsS9Vt13YdQirtWUJRuQIeoDpWiQRkrs7i+cG8=	2018-04-30 16:46:00.675-05	f	jose	joseee	macas	miguel12@yahoo.es	f	t	2018-04-30 16:44:38.915-05	1102223334	Masculino	0982344566	\N	1 	0725693243	\N	\N	\N	\N	Guayaquil
95	pbkdf2_sha256$36000$bfYm5CXFZBQ0$k+mF6nW3czKvaWGHyeAgSC9D2HALMD1OPE5AvnlihGg=	\N	f	jaime23			lore25@yahoo.es	f	t	2018-04-30 17:17:52.106-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
97	pbkdf2_sha256$36000$CXiY9bQAer29$6x/7/55cTwRzMQ/uFCV8l11WfnVm3KmoIHVCUcOQek0=	\N	f	jaime2			lore25@yahoo.es	f	t	2018-04-30 17:18:12.28-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
75	pbkdf2_sha256$36000$GLcYDbOLMUeD$hnV1X8FzMQmEgzqJxNM0CUhbL05/Qd2SBCfR+mW4SSA=	2018-04-24 13:19:50.584043-05	f	vero	Andrea	Vero	vero@gamil.com	f	t	2018-04-24 13:19:50.464867-05	2123200021	Masculino	0984512315	usuario/2018/04/30/woman-athlete-drinking-water.jpg	15	\N	\N	\N	\N	\N	Loj
99	pbkdf2_sha256$36000$msLwXalapNKi$kNdDzJZhkNJue62nySYhxA6cmB7QRu2eGTzhBGWd/HE=	2018-04-30 17:56:21.389-05	f	jaime	Jaime	Lalangui	miguel12@yahoo.es	t	t	2018-04-30 17:55:30.117-05	1312432534	Masculino	0982787093		2 	0725693243	f	t	f	f	Quito
1	pbkdf2_sha256$36000$ctppDn4oJFXw$jwOHHr6SsMKtUC8a5frqQvyt+4khHKpcgPuIESiagpE=	2018-04-30 19:56:36.564-05	t	tesis001			miguelcapa20@gmail.com	t	t	2018-04-14 13:08:09-05	1298337483	Masculino	0983625478		2 	0725693243	\N	\N	\N	\N	Guayaquil
101	pbkdf2_sha256$36000$2HLyV4ojCYQI$yTxrQvzmY4cCK3RJjJiQ+eP2rsU3+r94M3EiMQoeN9A=	2018-05-05 13:57:49.853-05	f	raul	Raul	Padro	miguelcapa20@gmail.com	t	t	2018-04-30 19:33:27.415-05	1223345645	Masculino	0982344566	usuario/2018/04/30/17068260-26730710-01finished-0-1510496576-1510496592-2000-1-1510496592-650-04f9ebaa03-1510932288.jpg	23	0725693243	\N	\N	\N	t	Loja - San Sebastian
117	pbkdf2_sha256$36000$W7e6V5CX9Y0T$XqskFm9EY2/apT8jbsJFGMIVjOhTY8hpYjW3ijO7I5E=	2018-05-04 16:19:55.496-05	f	deos	Jose	Lievanas	miguelcapa25@yahoo.es	t	t	2018-05-04 16:19:55.496-05	2312343214	Femenino	0982787093		14	0725693243	\N	\N	\N	t	Guayaquil
100	pbkdf2_sha256$36000$8uKASgAD0ilR$xDUvRG7dpQ1kE0v94t9mEgfs2CoRFAWpGbszjmjem7Q=	2018-04-30 18:25:08.277-05	f	instructormiguel	Miguel	Capa	miguelcapa20@gmail.com	t	t	2018-04-30 18:07:12.992-05	1212332435	Masculino	0982787093		1 	0725693243	f	f	t	f	Quito
113	pbkdf2_sha256$36000$NGKkND80cOg4$YT9PRJpBjuUtEWz3xiZEoV9Ebma+TOFfuFwxBL4haYY=	2018-05-04 14:01:16.354-05	f	usuario	sad	sad	miguel12@yahoo.es	t	t	2018-05-04 14:01:16.354-05	124321    	Masculino	23123     	usuario/2018/05/04/17068260-26730710-01finished-0-1510496576-1510496592-2000-1-1510496592-650-04f9ebaa03-1510932288_XLuvtbP.jpg	23	213       	\N	\N	\N	t	loas
105	pbkdf2_sha256$36000$oham3gmb1Jw0$rwYFtcFBisdPtvij+8YQChiK3zy1Ybb896soipCkaBQ=	2018-04-30 20:12:40.133-05	f	monse	Moserrat	Gallardo|	miguel12@yahoo.es	t	t	2018-04-30 20:11:26.326-05	1239832482	Femenino	0982787093		2 	\N	\N	t	\N	\N	Loja - San Sebastian
102	pbkdf2_sha256$36000$7G7rndtDiMiN$IpIi0T/9pc9N8gx2VTLLMniTw93usRmdZ6ubZaECfDs=	2018-04-30 19:42:47.57-05	f	pedroo	Pedroo	Llanos	miguel12@yahoo.es	t	t	2018-04-30 19:40:36.048-05	1103934348	Masculino	0982344566		45	0725693243	\N	t	\N	\N	Quito
114	pbkdf2_sha256$36000$Fou8wrpwXQ5L$G0wK4Ld8QNqirBZIr8m2Kmh2LfpAZjOoR/oT6MoH8qA=	2018-05-04 14:03:02.267-05	f	ronald	ronald	cevallos	miguel12@yahoo.es	t	t	2018-05-04 14:03:02.267-05	2132432452	Masculino	0982344566	usuario/2018/05/04/17068260-26730710-01finished-0-1510496576-1510496592-2000-1-1510496592-650-04f9ebaa03-1510932288_5Kzd8W3.jpg	12	\N	\N	\N	\N	t	loja
110	pbkdf2_sha256$36000$ypO1ipW4A9ca$HrIyyKP6dx5ftJUSM//qYPc3JbCF1njKTgWYTUDNok4=	2018-05-03 23:45:55.85-05	f	royer	royer	Ruiz	miguelcapa25@yahoo.es	t	t	2018-05-03 23:45:08.597-05	1111111134	Masculino	0982344566	usuario/2018/05/03/aid101070-v4-728px-Become-a-Personal-Trainer-Step-1.jpg	23	0725693243	\N	\N	\N	t	Guayaquil
108	pbkdf2_sha256$36000$g8Fw9bslMqme$h2wXHtuh/1RunZb/2K/TfqriVI+NzAPmp60qk2Hoeww=	2018-05-01 00:41:00.01-05	f	karina	Karina	Velazco	miguel12@yahoo.es	f	t	2018-05-01 00:40:59.9-05	1106667521	Femenino	0982344566	usuario/2018/05/01/fitness-deportista-en-ropa-deportiva-de-moda-haciendo-ejercicio-de-fitness-de-yoga-y-sentado-en-el-suelo-en-el-gimnasio-ropa-deportiva-y-zapatos-estilo-urbano_2139-435.jpg	12	0725693243	\N	\N	\N	\N	Quito
115	pbkdf2_sha256$36000$PSF9hyBRFiWm$ciZZnnDHiPEvFYc4/QKOpv+Pfx3bQuCS7MTqOfoxZ9k=	2018-05-04 14:14:42.366-05	f	123	Miguel	Ruiz	miguelcapa25@yahoo.es	t	t	2018-05-04 14:14:42.366-05	1243242534	Masculino	0983625478		23	0725693243	\N	\N	\N	t	Guayaquil
111	pbkdf2_sha256$36000$OX9SOqNkRwbs$4cU5IjPtwBJvtotDaIGY8HIk1rf+4svQXWR9GB5X02Y=	2018-05-04 09:27:07.312-05	f	pedro	pedro	cueva	miguelcapa20@gmail.com	t	t	2018-05-04 09:27:07.312-05	1127263727	Masculino	0983625478	usuario/2018/05/04/17068260-26730710-01finished-0-1510496576-1510496592-2000-1-1510496592-650-04f9ebaa03-1510932288.jpg	23	0725693243	\N	\N	\N	t	Quito
38	123	2018-04-17 10:02:46.547-05	f	instructorcarlos	Carlos Humberto	Jerves Galvan	carlosjervesg@yahoo.com	t	t	2018-04-17 10:02:46.547-05	1103559751	Masculino	0989654794	usuario/2018/04/30/aid101070-v4-728px-Become-a-Personal-Trainer-Step-1.jpg	40	\N	f	f	t	f	Loja
103	pbkdf2_sha256$36000$scXpr2i4QsMh$k8IO8OkWAzVpTYgWiz1HVRbTyUo60ON2g8sujNyeh78=	2018-04-30 19:53:40.246-05	f	joselyn	Joselyn	Collaguazo	miguel12@yahoo.es	t	t	2018-04-30 19:51:37.216-05	1234345546	Femenino	0982344566	usuario/2018/05/01/happy-woman-trainer-microphone-clipboard-fitness-sport-people-concept-sports-over-green-natural-background-56471852.jpg	12	\N	f	f	t	f	Loja - San Sebastian
116	pbkdf2_sha256$36000$oTNgIvZmWx6n$Wrb4XfSQb/gz11cv8gWvPANsVFSTcYSWAss4T+dtUmQ=	2018-05-04 14:45:50.063-05	f	1234	Miguel	Ruiz	miguelcapa25@yahoo.es	t	t	2018-05-04 14:45:50.063-05	1243242537	Masculino	0983625478		23	0725693243	\N	\N	\N	t	Guayaquil
106	pbkdf2_sha256$36000$RPvqjtmzcVf2$JaUpHrvbmn/DmYwsfFfsCA1RpvV+7ruUDOMnriV+viM=	2018-04-30 22:01:23.119-05	f	milton	Milton	Guajala	miguel12@yahoo.es	t	t	2018-04-30 22:01:23.12-05	1105557215	Masculino	0982787093		56	0725693243	\N	t	\N	\N	Guayaquyil
118	pbkdf2_sha256$36000$XhicbCB1RjyX$g4ER75Wfanqur4FP6S9NVpsQnQ9v1RfsOfrfTUQJ6Y0=	2018-05-04 16:23:50.679-05	f	josecastro	Jose	Castro	miguel12@yahoo.es	t	t	2018-05-04 16:23:50.679-05	1109876556	Masculino	0983625478		23	0725693243	\N	\N	\N	t	PItas
112	pbkdf2_sha256$36000$rMd9pml4wioP$FEMj9szuU9nLBkOMWV3eY+S/tii1FZPER0nWmxluzeg=	2018-05-04 11:49:21.413-05	t	tesis003			miguelcapa20@gmail.com	t	t	2018-05-04 11:49:04.084-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
109	pbkdf2_sha256$36000$m8jT1Y8XdnHc$aagQioZFtZc95mad4sMYG0YEc8v1FRV/WH5bTp/Ak90=	2018-05-05 13:59:26.744-05	f	marco	Marco	Calva	miguel12@yahoo.es	t	t	2018-05-01 08:41:31.339-05	1323453454	Masculino	0982787093	usuario/2018/05/01/17068260-26730710-01finished-0-1510496576-1510496592-2000-1-1510496592-650-04f9ebaa03-1510932288.jpg	5 	0725693243	\N	\N	\N	t	Quito
104	pbkdf2_sha256$36000$cuQSaN7ys6jd$bpQ6l+OVOBWKu9dOsAVwjUYbntAcjiRzJTPplJf01Uo=	2018-05-06 13:54:21.192-05	t	tesisfinal			miguelcapa20@gmail.com	t	t	2018-04-30 20:06:47-05	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
119	pbkdf2_sha256$36000$B9sVeW0TYpH9$y0YECpOtpcVjse+p7Q12i7dSoyhvNeoOaV7aVTfwLzQ=	2018-05-04 16:25:40.64-05	f	deos23	Jose	Lievanas	miguelcapa25@yahoo.es	t	t	2018-05-04 16:25:40.64-05	2312343217	Femenino	0982787093		14	0725693243	\N	\N	\N	t	Guayaquil
120	pbkdf2_sha256$36000$AbsLgqElGjQl$nE9c04JX3c2EEhwRPib9hucxvhEjksE/mHBoc43Gb9E=	2018-05-04 16:30:58.482-05	f	deos45	Jose	Lievanas	miguelcapa25@yahoo.es	t	t	2018-05-04 16:30:58.482-05	2312943217	Femenino	0982787093		14	0725693243	\N	\N	\N	t	Guayaquil
121	pbkdf2_sha256$36000$8MXh4vcYeOSB$1LCxWebWfelEodAArl8yJCfCPYQsT7Ol+UYSVOizHQE=	2018-05-04 16:33:35.877-05	f	deos4	Jose	Lievanas	miguelcapa25@yahoo.es	t	t	2018-05-04 16:33:35.877-05	0312943217	Femenino	0982787093		14	0725693243	\N	\N	\N	t	Guayaquil
122	pbkdf2_sha256$36000$rgfB8MPP1HwY$NhO7SeowxVv3/qROIpGvOyUtgzEhHOVsduZS7pKkhdw=	2018-05-04 16:34:31.13-05	f	deos5	Jose	Lievanas	miguelcapa25@yahoo.es	t	t	2018-05-04 16:34:31.13-05	0312903217	Femenino	0982787093		14	0725693243	\N	\N	\N	t	Guayaquil
123	pbkdf2_sha256$36000$gsUKl37xG1BE$51LSITMcf8O5IRcZOE5JRSjJ2ys1YvVxhzSN4uJixQ0=	2018-05-04 16:35:30.731-05	f	deos7	Jose	Lievanas	miguelcapa25@yahoo.es	t	t	2018-05-04 16:35:30.732-05	0312903277	Femenino	0982787093		14	0725693243	\N	\N	\N	t	Guayaquil
124	pbkdf2_sha256$36000$SydWmYZ2GovB$KoZjEZkP92sp636Gg4pgymGMaJB8KjW5XjPWjE6iIp8=	2018-05-04 16:42:14.395-05	f	deos8	Jose	Lievanas	miguelcapa25@yahoo.es	t	t	2018-05-04 16:42:14.395-05	0312909277	Femenino	0982787093		14	0725693243	\N	\N	\N	t	Guayaquil
125	pbkdf2_sha256$36000$nkbqTAgbbBqy$iZGyj6+SkIvwVYuXtSCealNjWVcpNIP8Qi18YTM/EXw=	2018-05-04 16:43:32.848-05	f	deos10	Jose	Lievanas	miguelcapa25@yahoo.es	t	t	2018-05-04 16:43:32.848-05	0312909299	Femenino	0982787093		14	0725693243	\N	\N	\N	t	Guayaquil
126	pbkdf2_sha256$36000$2KuCJu5zhIOL$Nq6cPXEfbm4rFw8jLY8g5jUf//M7Fsz991BL9ZwZkYQ=	2018-05-04 16:45:18.976-05	f	deos11	Jose	Lievanas	miguelcapa25@yahoo.es	t	t	2018-05-04 16:45:18.976-05	0312903423	Femenino	0982787093		14	0725693243	\N	\N	\N	t	Guayaquil
127	pbkdf2_sha256$36000$SHu3M92gTYMp$8NPuReJlDAiHNKLYW3dEl07zH6ZLW4nfz35PgXpiBr8=	2018-05-04 20:38:41.799-05	f	jonathan	Jonathan	Castro	miguelcapa25@yahoo.es	f	t	2018-05-04 20:38:41.731-05	1105557217	Masculino	0982787093	\N	2 	0725693243	\N	\N	\N	\N	Guayaquil
128	pbkdf2_sha256$36000$HSoVRIK6BDVY$MbWp8kGmXnUARBuMfq8MNoBNJRe0oefEuCAwwAtB+ks=	2018-05-05 12:27:53.776-05	f	nicolas	Nicolas	Cuenca	miguel12@yahoo.es	t	t	2018-05-05 12:22:37.543-05	1109876543	Masculino	0987654321		23	\N	\N	\N	\N	t	San sebastian
129	pbkdf2_sha256$36000$DAjkAuLcR381$GzOwJZXqiVgnrYSo2GODg/yAeRU8KTH4R7+F+6F8l8k=	2018-05-05 13:53:46.072-05	f	nico	Nicolas	Preinscripcion	miguel12@yahoo.es	f	t	2018-05-05 12:27:31.391-05	1106667543	Masculino	0982787093	\N	34	0725693243	\N	\N	\N	\N	Loja - San Sebastian
130	pbkdf2_sha256$36000$Bg7Ma2MrI4OM$vMJcEKbQVFBI5sfTTQ0Ejgl+MwdithyT9ZDdPiklQyI=	2018-05-06 13:55:06.738-05	f	jessica	Jessica	Calderon	miguel12@yahoo.es	t	t	2018-05-06 13:53:17.201-05	1123476585	Femenino	0982344566	usuario/2018/05/06/454439505.jpg	12	0765432412	\N	\N	\N	t	Las pitas
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_user_groups (id, user_id, group_id) FROM stdin;
4	109	2
9	104	4
5	101	1
10	21	2
11	128	2
12	130	2
\.


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_user_groups_id_seq', 12, true);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_user_id_seq', 130, true);


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
1	93	115
2	93	116
3	93	117
\.


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_user_user_permissions_id_seq', 3, true);


--
-- Data for Name: contacto; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY contacto (id_contacto, id_gimnasio, titulo_contac, desc_contact, direccion_contact, phone_uno, phone_dos, phone_tres) FROM stdin;
1	1	Instalaciones	<p style="text-align:justify">En nuestras instalacines encontrar&aacute;s: Una sala de Fitness con herramientas cardiovascular (cintas, bicis est&aacute;ticas y de spining, elipticas, remo), peso libre (barras, mancuernas, discos), maquinas de fuerza (press, multipower, prensa, extensiones), zona de estiramientos y abdominales (esterillas, fitball), salas de bailoterapia y taekwondo.&nbsp;</p>\r\n\r\n<p><img alt="" src="/static/media/uploads/2018/04/18/imagen1.png" style="height:453px; width:806px" /></p>	Loja, Ramón Pinto entre Miguel Riofrío Y Azuay	987654321	987654321	\N
\.


--
-- Name: contacto_id_contacto_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('contacto_id_contacto_seq', 1, true);


--
-- Data for Name: deportista; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY deportista (id_deportista, id) FROM stdin;
2	14
1	13
3	19
4	31
5	33
6	37
7	39
8	41
9	58
10	66
11	72
13	75
14	76
15	77
16	78
17	79
18	80
19	82
20	83
21	85
22	87
23	89
24	90
25	91
26	94
27	99
28	102
29	105
30	106
32	108
33	127
34	129
\.


--
-- Data for Name: deportista_destacado; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY deportista_destacado (id_deportista_des, nombre, apellidos, descripcion, nivel, disciplina, imagen) FROM stdin;
1	Juan	Lopez	GANADOR DE 5 MEDALLAS DE ORO	MEDIO	TAEKWONDO	destacados/2018/05/04/blogs-daily-details-01-personal-trainer-main.jpg
\.


--
-- Name: deportista_destacado_id_deportista_des_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('deportista_destacado_id_deportista_des_seq', 1, true);


--
-- Name: deportista_id_deportista_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('deportista_id_deportista_seq', 34, true);


--
-- Data for Name: disciplina; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY disciplina (id_disciplina, nombre, img_disciplina) FROM stdin;
2	BAILOTERAPIA	disciplina/2018/04/27/bailoterapia.jpg
5	TAEKWONDO	disciplina/2018/04/27/tkwdo.jpg
3	ARTES MARCIALES MIXTAS	disciplina/2018/04/30/tkwdo.jpg
1	FÍSICO CULTURISMO	disciplina/2018/05/01/fisicoculturismo.png
\.


--
-- Name: disciplina_id_disciplina_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('disciplina_id_disciplina_seq', 5, true);


--
-- Data for Name: disciplina_instructor; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY disciplina_instructor (id_disciplina_horario, id_disciplina, id_horario, id_instructor) FROM stdin;
12	1	6	5
13	2	5	5
15	5	10	4
11	2	4	4
\.


--
-- Name: disciplina_instructor_id_disciplina_horario_seq_1; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('disciplina_instructor_id_disciplina_horario_seq_1', 16, true);


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
1	2018-04-14 14:00:47.804-05	3	administrador1 Lalangui	1	[{"added": {}}]	27	1
2	2018-04-14 14:00:50.169-05	1	Administrador object	1	[{"added": {}}]	30	1
3	2018-04-14 15:17:38.592-05	7	deportista02 Lalangui - AERÓBICOS EN STEP - 19:00:00 21:00:00	1	[{"added": {}}]	36	1
4	2018-04-14 15:27:34.986-05	1	GYM DEOS	1	[{"added": {}}]	28	1
5	2018-04-14 15:53:29.2-05	2	ParallaxUno object	3		12	1
6	2018-04-15 20:05:38.698-05	13	deportista02 Lalangui	2	[{"changed": {"fields": ["edad", "direccion", "password"]}}]	27	1
7	2018-04-15 20:05:40.794-05	1	deportista02 Lalangui	2	[]	18	1
8	2018-04-20 16:28:41.56513-05	26	Jose Lalangui	3		36	43
9	2018-04-26 23:26:25.751-05	1	Administrador	1	[{"added": {}}]	3	1
10	2018-04-30 15:35:46.945-05	1	AuthGroupPermissions object	1	[{"added": {}}]	26	1
11	2018-04-30 15:36:00.686-05	2	AuthGroupPermissions object	1	[{"added": {}}]	26	1
12	2018-04-30 15:36:50.79-05	1	Administrador	3		7	1
13	2018-04-30 15:37:24.495-05	2	AuthGroupPermissions object	3		26	1
14	2018-04-30 15:37:24.497-05	1	AuthGroupPermissions object	3		26	1
15	2018-04-30 15:38:31.295-05	3	AuthGroupPermissions object	1	[{"added": {}}]	26	1
16	2018-04-30 15:38:47.985-05	92	miguel	1	[{"added": {}}]	4	1
17	2018-04-30 15:41:58.927-05	4	AuthGroupPermissions object	1	[{"added": {}}]	26	1
18	2018-04-30 15:42:48.507-05	1	tesis001	2	[]	4	1
19	2018-04-30 15:45:31.661-05	1	tesis001	2	[]	4	1
20	2018-04-30 15:45:41.355-05	1	tesis001	2	[]	4	1
21	2018-04-30 15:47:30.292-05	1	tesis001	2	[]	4	1
22	2018-04-30 15:51:24.232-05	2	AuthUserGroups object	1	[{"added": {}}]	29	1
23	2018-04-30 15:52:33.584-05	2	AuthUserGroups object	3		29	1
24	2018-04-30 15:56:04.865-05	93	angell	1	[{"added": {}}]	4	1
25	2018-04-30 15:56:50.529-05	93	angell	2	[]	4	1
26	2018-04-30 16:15:25.522-05	1	tesis001	2	[]	4	1
27	2018-05-01 08:59:13.403-05	5	AuthGroupPermissions object	1	[{"added": {}}]	26	104
28	2018-05-01 09:00:37.346-05	5	AuthGroupPermissions object	3		26	104
29	2018-05-01 09:00:57.88-05	6	AuthGroupPermissions object	1	[{"added": {}}]	26	104
30	2018-05-01 09:04:10.899-05	4	AuthUserGroups object	2	[]	29	104
31	2018-05-01 09:05:06.189-05	3	AuthUserGroups object	3		29	104
32	2018-05-01 09:08:47.087-05	7	AuthGroupPermissions object	1	[{"added": {}}]	26	104
33	2018-05-01 09:08:57.498-05	4	AuthGroupPermissions object	3		26	104
34	2018-05-01 10:02:55.787-05	2	Usuarios	1	[{"added": {}}]	3	104
35	2018-05-01 16:07:36.287-05	3	Superuser	1	[{"added": {}}]	7	104
36	2018-05-04 11:25:40.357-05	4	SuperUsuarios	1	[{"added": {}}]	3	104
37	2018-05-04 11:25:54.349-05	3	Superuser	3		3	104
38	2018-05-04 11:26:27.583-05	104	tesisfinal	2	[]	4	104
\.


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('django_admin_log_id_seq', 38, true);


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY django_content_type (id, app_label, model) FROM stdin;
1	admin	logentry
2	auth	permission
3	auth	group
4	auth	user
5	contenttypes	contenttype
6	sessions	session
7	app_deos	authgroup
8	app_deos	email
9	app_deos	djangosession
10	app_deos	horario
11	app_deos	djangomigrations
12	app_deos	parallaxuno
13	app_deos	acercade
14	app_deos	disciplinainstructor
15	app_deos	authpermission
16	app_deos	preinscripcion
17	app_deos	secretaria
18	app_deos	deportista
19	app_deos	parallax
20	app_deos	authuseruserpermissions
21	app_deos	producto
22	app_deos	disciplinahorario
23	app_deos	disciplinapreinscripcion
24	app_deos	inicioslide
25	app_deos	contacto
26	app_deos	authgrouppermissions
27	app_deos	authuser
28	app_deos	gimnasio
29	app_deos	authusergroups
30	app_deos	administrador
31	app_deos	djangocontenttype
32	app_deos	parallaxdos
33	app_deos	disciplina
34	app_deos	instructor
35	app_deos	djangoadminlog
36	app_deos	matricula
37	app_deos	galeria
38	app_deos	matriculadisciplina
39	app_deos	mensualidad
40	app_deos	seguimiento
41	thumbnail	kvstore
42	app_deos	deportistadestacado
\.


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('django_content_type_id_seq', 42, true);


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2018-04-14 12:43:29.566-05
2	auth	0001_initial	2018-04-14 12:43:30.303-05
3	admin	0001_initial	2018-04-14 12:43:30.481-05
4	admin	0002_logentry_remove_auto_add	2018-04-14 12:43:30.5-05
5	app_deos	0001_initial	2018-04-14 12:43:30.603-05
6	app_deos	0002_disciplinahorario	2018-04-14 12:43:30.608-05
7	contenttypes	0002_remove_content_type_name	2018-04-14 12:43:30.661-05
8	auth	0002_alter_permission_name_max_length	2018-04-14 12:43:30.681-05
9	auth	0003_alter_user_email_max_length	2018-04-14 12:43:30.696-05
10	auth	0004_alter_user_username_opts	2018-04-14 12:43:30.714-05
11	auth	0005_alter_user_last_login_null	2018-04-14 12:43:30.731-05
12	auth	0006_require_contenttypes_0002	2018-04-14 12:43:30.734-05
13	auth	0007_alter_validators_add_error_messages	2018-04-14 12:43:30.748-05
14	auth	0008_alter_user_username_max_length	2018-04-14 12:43:30.822-05
15	sessions	0001_initial	2018-04-14 12:43:30.948-05
16	thumbnail	0001_initial	2018-05-04 01:15:19.399-05
17	app_deos	0003_deportistadestacado	2018-05-04 19:18:17.676-05
18	app_deos	0004_auto_20180504_1918	2018-05-04 20:19:25.35-05
19	app_deos	0005_auto_20180504_2019	2018-05-04 20:19:31.898-05
\.


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('django_migrations_id_seq', 19, true);


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY django_session (session_key, session_data, expire_date) FROM stdin;
lhygosa88os44q1bn6rsdcjpvgiyg7cp	MzkzZjBjZmE2Mjc2MTdkYTU4OGQyNzEyNzc0Yzk0NzYzZTEzNDUxYjp7Il9hdXRoX3VzZXJfaGFzaCI6IjBhNjYyNTAwNmY0ODIzNTRmMGZiNzdjMWU5NDcxNWQ3Y2JiZGEzNDkiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2018-05-01 17:10:35.161-05
7sx7sfxvnb3850u6twiy89dnkj7wuf04	NGFlYmM2YTIyMGZiYTgxMTdiZmYzNjk0NjU4Y2E4YzAyZjYwYTQ0YTp7Il9hdXRoX3VzZXJfaWQiOiI0MyIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOGMwMTQ0OTcyYmU5ZTJkYWViZTJlM2EwNWMwNDMxMjM5Mzc5NjIzOSJ9	2018-05-03 19:05:12.521933-05
jnx5oey7b0cx3mdi6ufg0rs4k4xktww5	NDBmOTJiZGM1NmVlZGQ0Mjg4ZGU3YjBjNDY0ZTFhMWFjMjhiMjg2ZDp7Il9hdXRoX3VzZXJfaWQiOiI0MSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNmRjMzU1NjIxNThhZTdkMTAwZDk4MTk4YWQ2NzVhMmU0ZGRhNTFlOCJ9	2018-05-04 06:19:17.626379-05
5zf15mzq73xjq2spi6z235fa7tejksy1	NGFlYmM2YTIyMGZiYTgxMTdiZmYzNjk0NjU4Y2E4YzAyZjYwYTQ0YTp7Il9hdXRoX3VzZXJfaWQiOiI0MyIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOGMwMTQ0OTcyYmU5ZTJkYWViZTJlM2EwNWMwNDMxMjM5Mzc5NjIzOSJ9	2018-05-04 06:40:04.355419-05
3lyx9y0u8hmktxy6q1jrlx0jkqnfpmv5	NGFlYmM2YTIyMGZiYTgxMTdiZmYzNjk0NjU4Y2E4YzAyZjYwYTQ0YTp7Il9hdXRoX3VzZXJfaWQiOiI0MyIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOGMwMTQ0OTcyYmU5ZTJkYWViZTJlM2EwNWMwNDMxMjM5Mzc5NjIzOSJ9	2018-05-04 13:31:57.944775-05
e2himcebqb88vitpnuypv3g0e9aa84qz	NGFlYmM2YTIyMGZiYTgxMTdiZmYzNjk0NjU4Y2E4YzAyZjYwYTQ0YTp7Il9hdXRoX3VzZXJfaWQiOiI0MyIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOGMwMTQ0OTcyYmU5ZTJkYWViZTJlM2EwNWMwNDMxMjM5Mzc5NjIzOSJ9	2018-05-04 15:57:11.995202-05
ao1zb9d0k7abduy4h9eey0tnpyvpqbga	NGFlYmM2YTIyMGZiYTgxMTdiZmYzNjk0NjU4Y2E4YzAyZjYwYTQ0YTp7Il9hdXRoX3VzZXJfaWQiOiI0MyIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOGMwMTQ0OTcyYmU5ZTJkYWViZTJlM2EwNWMwNDMxMjM5Mzc5NjIzOSJ9	2018-05-04 16:28:27.851784-05
gp4unxm6gpid3uceyf7lybr91mtol4pb	NDYwZjVlYTZkNzlhZGY4ZGEyMzRiODRjYWI3NmFjYTVkZTFjNzUyMDp7fQ==	2018-05-07 19:34:09.572322-05
2rs9b7xo7wvgx87olxx2qktxjhckbab5	NDYwZjVlYTZkNzlhZGY4ZGEyMzRiODRjYWI3NmFjYTVkZTFjNzUyMDp7fQ==	2018-05-07 19:35:51.27272-05
25ob3euagnlqm7bnk7ugj2gzkkgq17ha	NGFlYmM2YTIyMGZiYTgxMTdiZmYzNjk0NjU4Y2E4YzAyZjYwYTQ0YTp7Il9hdXRoX3VzZXJfaWQiOiI0MyIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOGMwMTQ0OTcyYmU5ZTJkYWViZTJlM2EwNWMwNDMxMjM5Mzc5NjIzOSJ9	2018-05-07 19:46:01.607768-05
jncl6j62a3iujyr256lyrw5a77dsnjtc	NDYwZjVlYTZkNzlhZGY4ZGEyMzRiODRjYWI3NmFjYTVkZTFjNzUyMDp7fQ==	2018-05-08 12:46:49.382004-05
hprd4i4o01dz0brepodi4jaabd7gr9db	NGFlYmM2YTIyMGZiYTgxMTdiZmYzNjk0NjU4Y2E4YzAyZjYwYTQ0YTp7Il9hdXRoX3VzZXJfaWQiOiI0MyIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOGMwMTQ0OTcyYmU5ZTJkYWViZTJlM2EwNWMwNDMxMjM5Mzc5NjIzOSJ9	2018-05-08 15:26:46.176638-05
0va336fe6cksws4bfqg7f2l196uotcet	MzkzZjBjZmE2Mjc2MTdkYTU4OGQyNzEyNzc0Yzk0NzYzZTEzNDUxYjp7Il9hdXRoX3VzZXJfaGFzaCI6IjBhNjYyNTAwNmY0ODIzNTRmMGZiNzdjMWU5NDcxNWQ3Y2JiZGEzNDkiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2018-05-10 19:32:24.379-05
1expaclp1pug49l7000xpiie58xd55bj	MzkzZjBjZmE2Mjc2MTdkYTU4OGQyNzEyNzc0Yzk0NzYzZTEzNDUxYjp7Il9hdXRoX3VzZXJfaGFzaCI6IjBhNjYyNTAwNmY0ODIzNTRmMGZiNzdjMWU5NDcxNWQ3Y2JiZGEzNDkiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2018-05-10 21:48:04.665-05
96btwryim1qnsv6gm52bgmos5v344bi1	MzkzZjBjZmE2Mjc2MTdkYTU4OGQyNzEyNzc0Yzk0NzYzZTEzNDUxYjp7Il9hdXRoX3VzZXJfaGFzaCI6IjBhNjYyNTAwNmY0ODIzNTRmMGZiNzdjMWU5NDcxNWQ3Y2JiZGEzNDkiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2018-05-10 21:55:30.486-05
x7tvaoq12amh3thgub7dhddo33i0mvlj	MzkzZjBjZmE2Mjc2MTdkYTU4OGQyNzEyNzc0Yzk0NzYzZTEzNDUxYjp7Il9hdXRoX3VzZXJfaGFzaCI6IjBhNjYyNTAwNmY0ODIzNTRmMGZiNzdjMWU5NDcxNWQ3Y2JiZGEzNDkiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2018-05-12 22:41:14.609-05
ypvin6ehltasp2jn3oft5nv0d84nnn8f	MzkzZjBjZmE2Mjc2MTdkYTU4OGQyNzEyNzc0Yzk0NzYzZTEzNDUxYjp7Il9hdXRoX3VzZXJfaGFzaCI6IjBhNjYyNTAwNmY0ODIzNTRmMGZiNzdjMWU5NDcxNWQ3Y2JiZGEzNDkiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2018-05-11 10:57:30.046-05
n19q6uozsytzf0dkdooo79t69pdzk94m	YmI3NjNhNzMwZDRlMmEwNTRmMzA5NGFkN2UxZTRkYmJkMDYwYzU1Mjp7Il9hdXRoX3VzZXJfaGFzaCI6IjFmYTJjNjIyMTM3NTY1MmFkMWVkZDY2NDQxNmNjZTdhYjFiODY4OWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxMzAifQ==	2018-05-20 13:55:06.759-05
dneqig3di75l5pl6sgfmtjavxum5fa94	M2EyZWI5NDA1M2E4YWVmYjgwYjI2ZmNiODMwMGQ0NWRjYmI5OTFjZTp7Il9hdXRoX3VzZXJfaGFzaCI6IjdiNDliZTYxZTk5OTdjOWFjZjBiODIzMjI0NTk3MjZmOGY5NzgzMWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiI5MCJ9	2018-05-13 15:47:56.229-05
6ib3sc1x7swpv9sr8g5cf93q7oi32mb3	M2EyZWI5NDA1M2E4YWVmYjgwYjI2ZmNiODMwMGQ0NWRjYmI5OTFjZTp7Il9hdXRoX3VzZXJfaGFzaCI6IjdiNDliZTYxZTk5OTdjOWFjZjBiODIzMjI0NTk3MjZmOGY5NzgzMWYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiI5MCJ9	2018-05-13 15:55:34.965-05
\.


--
-- Data for Name: email; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY email (id_email, nombre, direccion, email, telefono, mensaje) FROM stdin;
\.


--
-- Name: email_id_email_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('email_id_email_seq', 1, false);


--
-- Data for Name: galeria; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY galeria (id_galeria, id_gimnasio, titulo_galeria, img_galeria, desc_galeria) FROM stdin;
1	1	V OPEN MACHALA	galeria/2018/04/27/30713920_10209859935095527_8627065061015289856_n.jpg	Participamos en 2 eventos muy importantes de preparación para nuestro prestigioso Club Deportivo "DEOS" el V Open Machala con casi 500 deportistas en escena y La I Copa del Pacífico de igual manera con una cantidad muy alta de deportistas y de gran nivel competitivo, estamos nuevamente contentos porque los resultados fueron muy satisfactorios para nuestro club obteniendo medallas de Oro, Plata y Bronce.
2	1	Poomsae del Selectivo Provincial	galeria/2018/04/27/30411705_10209817680839197_3864514386420826112_n.jpg	Y los resultados de Poomsae del Selectivo Provincial realizado este fin de semana en la categoría NOVATOS 5 medallas de Oro 6 medallas de Plata 4 medallas de Bronce... Categoría AVANZADO 6 medallas de Oro 4 medallas de Plata 5 medallas de Bronce. Un TOTAL EN LA MODALIDAD POOMSAE de 11 medallas de Oro 10 medallas de Plata 9 medallas de Bronce.
3	1	CAMPEONATO PROVINCIAL PRE ESCOLAR Y ESCOLAR	galeria/2018/04/27/30442800_10209817259668668_3200551084279988224_n.jpg	Deportistas en acción participando en varias categorías en el campeonato provincial Pre escolar y escolar obteniendo medallas de Oro, plata y bronce; en la categoría Infantil A e Infantil B 13 de Oro, 7 de Plata y 6 de Bronce; Categoría Menores 12 de Oro, 12 de Plata y 9 de Bronce; Categoría Prejuvenil 12 de Oro, 6 de Plata, 4 de Bronce, haciendo de esto el mejor club de la ciudad de Loja.
\.


--
-- Name: galeria_id_galeria_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('galeria_id_galeria_seq', 3, true);


--
-- Data for Name: gimnasio; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY gimnasio (id_gimnasio, nombre_gym) FROM stdin;
1	GYM DEOS
\.


--
-- Name: gimnasio_id_gimnasio_seq_5; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('gimnasio_id_gimnasio_seq_5', 1, true);


--
-- Data for Name: horario; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY horario (id_horario, hora_inicio, hora_fin, ubicacion, sesion, desc_horario, categoria, cupos) FROM stdin;
7	15:00:00	16:00:00	Sala1	Tarde	\N	7 a 13 anios	10
8	16:00:00	17:00:00	Sala 2	Tarde	\N	9 a 17 anios	8
9	18:00:00	20:00:00	sala 3	Noche	\N	Avanzados	5
11	20:00:00	21:00:00	sala 3	Tarde	\N	Avanzados	0
12	15:00:00	16:00:00	SALA90	Noche	\N	7 a 13 anios	25
10	17:00:00	18:30:00	Sala 1	Tarde	\N	Avanzados	0
6	10:00:00	11:00:00	Sala1	Manana	\N	Avanzados	0
5	08:30:00	10:00:00	Sala 1	Manana	\N	Avanzados	17
4	14:00:00	15:00:00	Sala 1	Tarde	\N	4 a 6 anios	31
\.


--
-- Name: horario_id_horario_seq_1_1; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('horario_id_horario_seq_1_1', 12, true);


--
-- Data for Name: inicio_slide; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY inicio_slide (id_inicioslide, id_gimnasio, img_slide, imgbtn_slide, titulo_slide, subt_slide, nombre_enlace, direccion_enlace) FROM stdin;
2	1	inicio/2018/04/26/Imagen1_JyRA82s.png	inicio/2018/04/26/Imagen1_QIyPD8J.png	FITNESS DEOS	Entrena con los mejores	\N	\N
1	1	inicio/2018/04/28/Imagen1.png	inicio/2018/04/28/Imagen1_tUEBLga.png	CLUB DEPORTIVO DEOS	Inicia desde ahora en el mejor club de la ciudad	\N	\N
\.


--
-- Name: inicio_slide_id_inicioslide_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('inicio_slide_id_inicioslide_seq', 5, true);


--
-- Data for Name: instructor; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY instructor (id_instructor, id, inst_profesion, inst_estado, inst_descripcion, inst_imagen) FROM stdin;
2	8	\N	\N	\N	\N
4	38	\N	\N	\N	\N
5	81	\N	\N	\N	\N
7	103	\N	\N	\N	\N
\.


--
-- Name: instructor_id_instructor_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('instructor_id_instructor_seq', 7, true);


--
-- Data for Name: matricula; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY matricula (id_matricula, id_deportista, id_secretaria, estado, fecha_matricula, costo) FROM stdin;
34	8	\N	f	2018-04-24 00:06:57.687041	15
43	9	\N	f	2018-04-24 00:36:19.894894	10
44	9	\N	f	2018-04-24 00:36:46.442734	5
45	9	\N	f	2018-04-24 00:43:39.488621	5
46	9	\N	f	2018-04-24 00:43:57.811549	5
47	9	\N	f	2018-04-24 00:44:06.633361	5
48	9	\N	f	2018-04-24 05:44:21.311668	5
50	11	\N	f	2018-04-24 18:11:41.688337	10
53	14	\N	f	2018-04-26 23:53:47.76	5
55	16	\N	f	2018-04-27 00:13:18.163	5
33	4	\N	f	2018-04-24 10:04:42.724894	15
35	7	\N	f	2018-04-24 05:07:57.322698	10
36	3	\N	f	2018-04-24 05:09:05.004614	10
37	3	\N	f	2018-04-24 05:10:08.568851	10
38	11	\N	f	2018-04-24 05:10:54.49739	10
39	13	\N	f	2018-04-24 05:11:15.655905	10
40	14	\N	f	2018-04-24 05:15:44.831643	10
41	2	\N	f	2018-04-24 05:19:02.348141	5
56	17	\N	f	2018-04-27 15:03:41.001	5
58	19	\N	t	2018-04-28 04:18:15.311	5
59	20	\N	f	2018-04-28 20:16:53.458	5
61	22	\N	f	2018-04-28 20:47:43.708	5
62	22	\N	f	2018-04-28 20:49:02.098	5
65	2	\N	f	2018-04-28 22:44:32.385	5
66	2	\N	f	2018-04-29 01:46:05.618	5
67	13	\N	t	2018-04-29 08:08:50.734	5
68	23	\N	f	2018-04-29 03:40:02.012	5
69	15	\N	f	2018-04-30 01:12:44.191	5
70	8	\N	t	2018-04-29 21:33:51.155	5
71	13	\N	f	2018-04-29 20:22:41.792	5
72	13	\N	f	2018-04-29 20:22:41.816	10
74	25	\N	f	2018-04-29 20:30:27.545	5
75	26	\N	f	2018-04-30 21:44:39.038	5
42	8	\N	t	2018-04-25 06:19:37.855693	20
79	4	\N	t	2018-05-01 08:57:09.76	5
82	32	\N	t	2018-05-01 10:41:00.031	5
83	3	\N	f	2018-05-04 21:00:36.648	5
84	2	\N	t	2018-05-05 02:00:53.195	5
80	18	\N	t	2018-05-01 09:24:17.612	5
81	18	\N	t	2018-05-01 09:24:59.918	5
85	3	\N	f	2018-05-05 01:00:19.771	5
86	3	\N	f	2018-05-05 01:08:43.599	5
87	7	\N	f	2018-05-05 01:17:05.381	5
88	11	\N	f	2018-05-05 01:19:53.716	5
89	33	\N	f	2018-05-05 01:38:41.803	5
90	6	\N	f	2018-05-05 02:23:22.329	5
92	3	\N	f	2018-05-05 02:33:09.673	5
93	3	\N	f	2018-05-05 02:33:09.684	10
94	2	\N	f	2018-05-05 02:39:40.443	5
95	2	\N	f	2018-05-05 02:39:40.453	10
96	2	\N	f	2018-05-05 02:40:38.055	5
97	2	\N	f	2018-05-05 02:40:38.058	10
52	13	\N	t	2018-04-25 04:19:50.59006	10
98	1	\N	f	2018-05-05 16:42:35.6	5
91	4	\N	t	2018-05-05 12:23:33.521	5
99	14	\N	t	2018-05-05 21:51:10.001	5
100	34	\N	t	2018-05-05 22:27:31.488	5
60	21	\N	t	2018-04-29 01:43:06.823	5
57	18	\N	t	2018-04-29 22:20:05.253	5
101	29	\N	t	2018-05-07 00:32:32.486	5
78	29	\N	f	2018-05-01 13:37:32.596	5
\.


--
-- Data for Name: matricula_disciplina; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY matricula_disciplina (id_matriculadisciplina, id_matricula, id_disciplina_horario, estado, costo, fecha_matricula) FROM stdin;
14	42	11	\N	\N	\N
18	45	11	\N	\N	\N
25	50	11	\N	\N	\N
29	52	11	\N	\N	\N
34	57	11	\N	\N	\N
37	60	11	\N	\N	\N
38	61	11	\N	\N	\N
40	65	11	\N	\N	\N
41	66	11	\N	\N	\N
42	67	12	\N	\N	\N
43	68	11	\N	\N	\N
44	69	12	\N	\N	\N
45	70	13	\N	\N	\N
46	72	13	\N	\N	\N
47	72	11	\N	\N	\N
50	74	12	\N	\N	\N
51	75	11	\N	\N	\N
54	78	12	\N	\N	\N
55	79	11	\N	\N	\N
56	79	12	\N	\N	\N
57	80	12	\N	\N	\N
58	81	11	\N	\N	\N
59	81	12	\N	\N	\N
60	82	13	\N	\N	\N
61	83	12	\N	\N	\N
62	84	12	\N	\N	\N
63	85	12	f	\N	\N
64	86	12	f	\N	\N
65	87	13	f	\N	\N
66	88	13	f	\N	2018-05-05 01:19:53.724
67	89	13	f	\N	2018-05-05 01:38:41.807
69	90	13	f	\N	2018-05-05 02:23:22.339
70	91	13	f	\N	2018-05-05 02:23:33.573
71	93	13	f	0	2018-05-05 02:33:09.687
72	93	11	f	0	2018-05-05 02:33:09.698
73	95	13	f	0	2018-05-05 02:39:40.456
74	97	13	f	0	2018-05-05 02:40:38.062
75	97	11	f	0	2018-05-05 02:40:38.071
76	98	13	f	0	2018-05-05 16:42:35.73
77	99	11	f	0	2018-05-05 16:51:10.174
78	100	13	f	0	2018-05-05 17:27:31.492
79	101	11	f	0	2018-05-06 19:32:32.602
\.


--
-- Name: matricula_disciplina_id_matriculadisciplina_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('matricula_disciplina_id_matriculadisciplina_seq', 79, true);


--
-- Name: matricula_id_matricula_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('matricula_id_matricula_seq', 101, true);


--
-- Data for Name: mensualidad; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY mensualidad (id_mensualidad, concepto, total, pagado, fecha_mensualidad, fecha_pago, id_matriculadisciplina) FROM stdin;
13	Mensualidad enero	25	t	2018-04-11	2018-05-11	\N
14	Mnesualiad febrero	25	f	2018-04-02	2018-05-02	\N
16	Mensualidad abril	45	t	2018-04-07	2018-05-07	\N
17	Mnesualiad febrero	45	f	2018-04-13	2018-05-13	\N
18	Mensualidad enero	25	t	2018-04-30	2018-05-30	\N
19	PAGO MENSUAL	25	f	2018-05-04	2018-05-04	62
20	PAGO MENSUAL	25	f	2018-05-04	2018-05-04	62
22	PAGO MENSUAL	25	f	2018-05-04	2018-05-04	67
23	PAGO MENSUAL	25	f	2018-05-04	2018-05-04	71
24	PAGO MENSUAL	25	f	2018-05-04	2018-05-04	72
21	PAGO MENSUAL	25	t	2018-05-04	2018-05-04	63
25	PAGO MENSUAL	25	f	2018-05-04	2018-05-04	74
26	PAGO MENSUAL	25	f	2018-05-04	2018-05-04	75
28	PAGO MENSUAL	25	f	2018-05-04	2018-05-04	29
31	PAGO MENSUAL	25	t	2018-06-05	2018-05-05	70
33	PAGO MENSUAL	25	f	2018-05-05	2018-05-05	77
32	PAGO MENSUAL	25	t	2018-06-05	2018-05-05	77
34	PAGO MENSUAL	25	t	2018-06-05	2018-05-05	78
35	PAGO MENSUAL	25	f	2018-05-06	2018-05-06	37
36	PAGO MENSUAL	25	f	2018-05-06	2018-05-06	34
37	PAGO MENSUAL	25	f	2018-05-06	2018-05-06	34
38	PAGO MENSUAL	25	f	2018-05-06	2018-05-06	79
39	PAGO MENSUAL	25	f	2018-05-06	2018-05-06	79
40	PAGO MENSUAL	25	f	2018-05-06	2018-05-06	54
41	PAGO MENSUAL	25	f	2018-06-06	2018-05-06	54
\.


--
-- Name: mensualidad_id_mensualidad_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('mensualidad_id_mensualidad_seq', 41, true);


--
-- Data for Name: parallax; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY parallax (id_parallax, id_gimnasio, img_parallax, title_parallax, desc_parallax, imginfe_parallax, enlace_video) FROM stdin;
1	1	parallax/2018/04/27/20424059_643165922544765_7517435827115188380_o.jpg	CLUB DEPORTIVO FORMATIVO DEOS GYM -	Todas las disciplinas ofertadas con las mejores rutinas de entrenamiento para mejorar tu salud física, disciplinas vinculadas a artes marciales, relacionadas con el baile y muchas más actividades que ayudarán a fortalecer tu físico.	parallax/2018/04/27/20368848_643165535878137_1360297925046312613_o.jpg	\N
\.


--
-- Data for Name: parallax_dos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY parallax_dos (id_parallaxdos, id_gimnasio, img_parallaxdos, costo_parallaxdos, dias_parallaxtres, desc_parallaxdos) FROM stdin;
1	1	parallaxdos/2018/04/27/20280478_642840955910595_7715594033522695030_o.jpg	4	Inscripción	Comienza desde ahora, inscríbete a Gym Deos.
2	1	parallaxdos/2018/04/27/20280644_643166022544755_4485812296969053589_o.jpg	24	Mensualidad	Inicias tu mensualidad desde la fecha de tu matrícula
\.


--
-- Name: parallax_dos_id_parallaxdos_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('parallax_dos_id_parallaxdos_seq', 2, true);


--
-- Name: parallax_id_parallax_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('parallax_id_parallax_seq', 1, true);


--
-- Data for Name: parallax_uno; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY parallax_uno (id_parallaxuno, id_gimnasio, back_parallaxuno, img_parallaxuno, title_parallaxuno, subt_parallaxuno) FROM stdin;
1	1	parallaxuno/2018/04/27/21125763_656145621246795_4566850975548237778_o.jpg	parallaxuno/2018/04/27/instalacion.png	Puedes encontrarnos en la siguiente dirección.	Loja, Ramón Pinto entre Miguel Riofrio y Azuay
\.


--
-- Name: parallax_uno_id_parallaxuno_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('parallax_uno_id_parallaxuno_seq', 2, true);


--
-- Data for Name: producto; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY producto (id_producto, id_gimnasio, nombre_prod, desc_prod, img_prod, precio_prod) FROM stdin;
5	1	CASCOS	Su principal función es la de brindarte los nutrientes necesarios para ganar peso muscular sobre todo cuando se trata de personas con una fisiología muy delgada, por lo cual este tipo de suplementos gym te aportaran ese extra para lograr ese gran objetivo	producto/2018/05/02/20157911_639437986250892_1069580999285688662_o.jpg	\N
4	1	DOBOK	Su principal función es la de brindarte los nutrientes necesarios para ganar peso muscular sobre todo cuando se trata de personas con una fisiología muy delgada, por lo cual este tipo de suplementos gym te aportaran ese extra para lograr ese gran objetivo	producto/2018/05/02/20157911_639437986250892_1069580999285688662_o_eieUTym.jpg	\N
\.


--
-- Name: producto_id_producto_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('producto_id_producto_seq', 5, true);


--
-- Data for Name: secretaria; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY secretaria (id_secretaria, id, id_administrador) FROM stdin;
5	6	\N
6	32	\N
9	101	\N
10	109	\N
11	110	\N
12	111	\N
13	113	\N
14	119	\N
15	120	\N
16	121	\N
17	128	\N
18	130	\N
\.


--
-- Name: secretaria_id_secretaria_seq_1_1; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('secretaria_id_secretaria_seq_1_1', 18, true);


--
-- Data for Name: seguimiento; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY seguimiento (id_seguimiento, diametro_brazo, fecha, id_matricula) FROM stdin;
\.


--
-- Name: seguimiento_id_seguimiento_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('seguimiento_id_seguimiento_seq', 1, false);


--
-- Data for Name: thumbnail_kvstore; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY thumbnail_kvstore (key, value) FROM stdin;
\.


--
-- Name: acerca_de acerca_de_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY acerca_de
    ADD CONSTRAINT acerca_de_pk PRIMARY KEY (id_acerca);


--
-- Name: administrador administrador_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY administrador
    ADD CONSTRAINT administrador_pk PRIMARY KEY (id_administrador);


--
-- Name: app_deos_deportistadestacado app_deos_deportistadestacado_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY app_deos_deportistadestacado
    ADD CONSTRAINT app_deos_deportistadestacado_pkey PRIMARY KEY (id_deportista_des);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_user_id_group_id_94350c0c_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- Name: auth_user auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_permission_id_14a6b632_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- Name: auth_user auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: contacto contacto_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY contacto
    ADD CONSTRAINT contacto_pk PRIMARY KEY (id_contacto);


--
-- Name: deportista_destacado deportista_destacado_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY deportista_destacado
    ADD CONSTRAINT deportista_destacado_pk PRIMARY KEY (id_deportista_des);


--
-- Name: deportista deportista_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY deportista
    ADD CONSTRAINT deportista_pk PRIMARY KEY (id_deportista);


--
-- Name: disciplina_instructor disciplina_instructor_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY disciplina_instructor
    ADD CONSTRAINT disciplina_instructor_pk PRIMARY KEY (id_disciplina_horario);


--
-- Name: disciplina disciplina_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY disciplina
    ADD CONSTRAINT disciplina_pk PRIMARY KEY (id_disciplina);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: email email_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY email
    ADD CONSTRAINT email_pk PRIMARY KEY (id_email);


--
-- Name: galeria galeria_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY galeria
    ADD CONSTRAINT galeria_pk PRIMARY KEY (id_galeria);


--
-- Name: gimnasio gimnasio_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY gimnasio
    ADD CONSTRAINT gimnasio_pk PRIMARY KEY (id_gimnasio);


--
-- Name: horario horario_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY horario
    ADD CONSTRAINT horario_pk PRIMARY KEY (id_horario);


--
-- Name: inicio_slide inicio_slide_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY inicio_slide
    ADD CONSTRAINT inicio_slide_pk PRIMARY KEY (id_inicioslide);


--
-- Name: instructor instructor_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY instructor
    ADD CONSTRAINT instructor_pk PRIMARY KEY (id_instructor);


--
-- Name: matricula_disciplina matricula_disciplina_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY matricula_disciplina
    ADD CONSTRAINT matricula_disciplina_pk PRIMARY KEY (id_matriculadisciplina);


--
-- Name: matricula matricula_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY matricula
    ADD CONSTRAINT matricula_pk PRIMARY KEY (id_matricula);


--
-- Name: mensualidad mensualidad_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY mensualidad
    ADD CONSTRAINT mensualidad_pk PRIMARY KEY (id_mensualidad);


--
-- Name: parallax_dos parallax_dos_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY parallax_dos
    ADD CONSTRAINT parallax_dos_pk PRIMARY KEY (id_parallaxdos);


--
-- Name: parallax parallax_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY parallax
    ADD CONSTRAINT parallax_pk PRIMARY KEY (id_parallax);


--
-- Name: parallax_uno parallax_uno_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY parallax_uno
    ADD CONSTRAINT parallax_uno_pk PRIMARY KEY (id_parallaxuno);


--
-- Name: producto producto_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY producto
    ADD CONSTRAINT producto_pk PRIMARY KEY (id_producto);


--
-- Name: secretaria secretaria_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY secretaria
    ADD CONSTRAINT secretaria_pk PRIMARY KEY (id_secretaria);


--
-- Name: seguimiento seguimiento_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY seguimiento
    ADD CONSTRAINT seguimiento_pk PRIMARY KEY (id_seguimiento);


--
-- Name: thumbnail_kvstore thumbnail_kvstore_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY thumbnail_kvstore
    ADD CONSTRAINT thumbnail_kvstore_pkey PRIMARY KEY (key);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_name_a6ea08ec_like ON auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_group_id_97559544; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_groups_group_id_97559544 ON auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_user_id_6a12ed8b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_groups_user_id_6a12ed8b ON auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_permission_id_1fbb5f2c; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_user_id_a95ead1b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_username_6821ab7c_like ON auth_user USING btree (username varchar_pattern_ops);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_session_expire_date_a5c62663 ON django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_session_session_key_c0390e0f_like ON django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: thumbnail_kvstore_key_3f850178_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX thumbnail_kvstore_key_3f850178_like ON thumbnail_kvstore USING btree (key varchar_pattern_ops);


--
-- Name: secretaria administrador_secretaria_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY secretaria
    ADD CONSTRAINT administrador_secretaria_fk FOREIGN KEY (id_administrador) REFERENCES administrador(id_administrador);


--
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: administrador auth_user_administrador_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY administrador
    ADD CONSTRAINT auth_user_administrador_fk FOREIGN KEY (id) REFERENCES auth_user(id);


--
-- Name: deportista auth_user_cliente_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY deportista
    ADD CONSTRAINT auth_user_cliente_fk FOREIGN KEY (id) REFERENCES auth_user(id);


--
-- Name: auth_user_groups auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: secretaria auth_user_secretaria_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY secretaria
    ADD CONSTRAINT auth_user_secretaria_fk FOREIGN KEY (id) REFERENCES auth_user(id);


--
-- Name: auth_user_user_permissions auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: matricula cliente_matricula_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY matricula
    ADD CONSTRAINT cliente_matricula_fk FOREIGN KEY (id_deportista) REFERENCES deportista(id_deportista);


--
-- Name: disciplina_instructor disciplina_disciplina_instructor_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY disciplina_instructor
    ADD CONSTRAINT disciplina_disciplina_instructor_fk FOREIGN KEY (id_disciplina) REFERENCES disciplina(id_disciplina);


--
-- Name: matricula_disciplina disciplina_instructor_matricula_disciplina_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY matricula_disciplina
    ADD CONSTRAINT disciplina_instructor_matricula_disciplina_fk FOREIGN KEY (id_disciplina_horario) REFERENCES disciplina_instructor(id_disciplina_horario);


--
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: acerca_de gimnasio_acerca_de_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY acerca_de
    ADD CONSTRAINT gimnasio_acerca_de_fk FOREIGN KEY (id_gimnasio) REFERENCES gimnasio(id_gimnasio);


--
-- Name: contacto gimnasio_contacto_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY contacto
    ADD CONSTRAINT gimnasio_contacto_fk FOREIGN KEY (id_gimnasio) REFERENCES gimnasio(id_gimnasio);


--
-- Name: galeria gimnasio_galeria_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY galeria
    ADD CONSTRAINT gimnasio_galeria_fk FOREIGN KEY (id_gimnasio) REFERENCES gimnasio(id_gimnasio);


--
-- Name: inicio_slide gimnasio_inicio_slide_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY inicio_slide
    ADD CONSTRAINT gimnasio_inicio_slide_fk FOREIGN KEY (id_gimnasio) REFERENCES gimnasio(id_gimnasio);


--
-- Name: parallax_dos gimnasio_parallax_dos_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY parallax_dos
    ADD CONSTRAINT gimnasio_parallax_dos_fk FOREIGN KEY (id_gimnasio) REFERENCES gimnasio(id_gimnasio);


--
-- Name: parallax gimnasio_parallax_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY parallax
    ADD CONSTRAINT gimnasio_parallax_fk FOREIGN KEY (id_gimnasio) REFERENCES gimnasio(id_gimnasio);


--
-- Name: parallax_uno gimnasio_parallax_uno_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY parallax_uno
    ADD CONSTRAINT gimnasio_parallax_uno_fk FOREIGN KEY (id_gimnasio) REFERENCES gimnasio(id_gimnasio);


--
-- Name: producto gimnasio_producto_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY producto
    ADD CONSTRAINT gimnasio_producto_fk FOREIGN KEY (id_gimnasio) REFERENCES gimnasio(id_gimnasio);


--
-- Name: disciplina_instructor horario_disciplina_instructor_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY disciplina_instructor
    ADD CONSTRAINT horario_disciplina_instructor_fk FOREIGN KEY (id_horario) REFERENCES horario(id_horario);


--
-- Name: disciplina_instructor instructor_disciplina_instructor_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY disciplina_instructor
    ADD CONSTRAINT instructor_disciplina_instructor_fk FOREIGN KEY (id_instructor) REFERENCES instructor(id_instructor);


--
-- Name: mensualidad matricula_disciplina_mensualidad_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY mensualidad
    ADD CONSTRAINT matricula_disciplina_mensualidad_fk FOREIGN KEY (id_matriculadisciplina) REFERENCES matricula_disciplina(id_matriculadisciplina);


--
-- Name: matricula_disciplina matricula_matricula_disciplina_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY matricula_disciplina
    ADD CONSTRAINT matricula_matricula_disciplina_fk FOREIGN KEY (id_matricula) REFERENCES matricula(id_matricula);


--
-- Name: seguimiento matricula_seguimiento_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY seguimiento
    ADD CONSTRAINT matricula_seguimiento_fk FOREIGN KEY (id_matricula) REFERENCES matricula(id_matricula);


--
-- Name: instructor persona_instructor_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY instructor
    ADD CONSTRAINT persona_instructor_fk FOREIGN KEY (id) REFERENCES auth_user(id);


--
-- Name: matricula secretaria_matricula_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY matricula
    ADD CONSTRAINT secretaria_matricula_fk FOREIGN KEY (id_secretaria) REFERENCES secretaria(id_secretaria);


--
-- PostgreSQL database dump complete
--

